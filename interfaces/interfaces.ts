import { CSSProperties, SyntheticEvent, ReactNode } from 'react';

import { ButtonVariantType, IconIdType } from '../types';

export interface IAdditionalClassNames {
  /**
   * Specify additional class names that apply to the parent node
   */
  additionalClassNames?: string;
}

export interface IBackgroundColor {
  /**
   * Background colour for the item - is applied via inline style
   */
  backgroundColor?: CSSProperties['backgroundColor'];
}

export interface IBorderColor {
  /**
   * Border colour for the item - is applied via inline style
   */
  borderColor?: CSSProperties['borderColor'];
}

export interface IButtonVariant {
  /**
   * Specify the variant of button you would like to display
   */
  variant?: ButtonVariantType;
}

export interface ICoverLink {
  /**
   * Provide a link that will cover the element
   */
  coverLink: ReactNode;
}

export interface IErrorMessage {
  /**
   * The error message to display
   */
  errorMessage?: string;
}

export interface IForId {
  /**
   * The descriptive id to match the input for accessibility
   */
  forId: string;
}

export interface IIconId {
  /**
   * The id of the icon to be mapped to the SVG icon
   */
  iconId: IconIdType;
}

export interface IIdentifier {
  /**
   * Unique id of an item
   */
  id: string;
}

export interface IImage {
  /**
   * Specify alt text so that screen readers and search engines can identify the image
   */
  altText: string;
  /**
   * Specify the source of the image that will be displayed
   */
  src: string;
}

export interface IIsDisabled {
  /**
   * Specify if the input element is disabled
   */
  isDisabled?: boolean;
}

export interface IIsLabelHidden {
  /**
   * Visually hides the label but keeps it for screen readers
   */
  isLabelHidden?: boolean;
}

export interface IIsReadOnly {
  /**
   * The boolean value deciding whether the textarea is read only
   */
  isReadOnly?: boolean;
}

export interface IIsRequired {
  /**
   * Specify if the input element is required
   */
  isRequired?: boolean;
}

export interface ILabelText {
  /**
   * The label text
   */
  labelText: string;
}

export interface IName {
  /**
   * The name of the HTML element
   */
  name: string;
}

export interface IOnClickButton {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => void;
}

export interface IPlaceholder {
  /**
   * The placeholder of an input
   */
  placeholder?: string;
}

export interface ISubTitle {
  /**
   * Specify a secondary or explanatory title that will be displayed on the page
   */
  subTitle?: string;
}

export interface ISupportiveText {
  /**
   * Specify supportive text for screen readers
   */
  supportiveText: string;
}

export interface ITitle {
  /**
   * Specify a title that will be displayed on the page
   */
  title?: string;
}
