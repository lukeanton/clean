# Netfront UI

This is the base UI library for all Netfront web projects.

Any component being created in a project should be considered a candidate for inclusion in this library and should be discussed with the other devs.

## Technologies

- [React](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [PostCSS](https://postcss.org/)

## Prerequisites

- [Node](https://nodejs.org/en/) _(see [`.npmrc`](.npmrc) for version no)_
- [Yarn](https://yarnpkg.com/en/)

### Installing

Open a command line of your choice and do the following:

1. Clone the repository and `cd` into the project:

   ```bash
   git clone git@bitbucket.org:netfront/netfront-ui.git && cd netfront-ui
   ```

2. Install the dependencies:

   ```bash
   yarn
   ```

**✋ Powershell users must replace `&&` with `;`.**

### Running

Components should be built in isolation using [Storybook](https://storybook.js.org/)

```bash
yarn storybook
```

Open [http://localhost:6006](http://localhost:6006) to view the app if it doesn't open automatically.

### Testing

We should be aiming to write unit tests for all components [`Jest`](https://github.com/facebook/jest)

#### Coverage

We're starting out looking for 90% test coverage. It is a high bar, but we have a way higher chance of achieving it if we start out with high coverage rather than trying to patch tests back in later.

### Styling approach

Because we will have a shared library at a level above the Ekardo projects and also, small mini-apps within the projects that require shared styles / components, we have opted to use a vanilla CSS approach and take advantage of the global nature of CSS. Contraversial these days I know.

This means we have to be more mindful of naming conventions and the patterns we use in our CSS architecture.

We are using [Backpack.css](https://github.com/chris-pearce/backpack.css/) as our light-weight reset, and have a few PostCSS plugins that run at build time to handle auto-prefixing, code splitting and enabling compile-time (scss style) variables. This is set as a peer-dependency and does not ship with this library - it is required that you include it in your project directly as a dependency.

To keep our code modular we will be using the [BEM](https://en.bem.info/methodology/quick-start/) naming convention alongside suggestions made in [this article](https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/) for consistency.

### Code quality

We have a number of npm scripts to help ensure we have a consistent codebase as outlined below:

#### `yarn precommit`

Performs a series of tasks using [`lint-staged`](https://github.com/okonet/lint-staged) and [`husky`](https://github.com/typicode/husky) that are automatically triggered when performing a git commit.

The following tasks are performed on git staged files that match the configuration in `package.json`s `lint-staged` property:

1. Runs [Prettier](https://prettier.io/) over all processed files rewriting any issues.
2. Lints all JavaScript and CSS files rewriting any issues if possible (see [`yarn lint`](#markdown-header-yarn-lint)). Anything that can't be autofixed will have to be fixed before you can commit.

#### `yarn lint`

Runs [`yarn lint:js`](#markdown-header-yarn-lintjs) and [`yarn lint:css`](#markdown-header-yarn-lintcss).

**✋ Runs automatically as part of the [precommit hook](#markdown-header-yarn-precommit).**

#### `yarn lint:js`

Lints all JavaScript and TypeScript files (`*.js, *.tsc`) in the project using [ESLint](https://eslint.org/).

See [.eslintignore](.eslintignore) for the files ESLint ignores.

#### `yarn lint:css`

Lints all CSS files (`*.css`) in the project using [stylelint](https://stylelint.io/).

See [.stylelintignore](.stylelintignore) for the files stylelint ignores.

#### `yarn prettier:diff`

Reports all the files that don't adhere to Prettier's configuration.

### Editors

#### Visual Studio Code

##### Settings

If you're using [Visual Studio Code](https://code.visualstudio.com/) (VSC), like most of us are, then the most crucial settings will be provided via [`.vscode/settings.json`](.vscode/settings.json). On top of our lint tooling ([ESLint](https://eslint.org/), [stylelint](https://stylelint.io/), [Prettier](https://prettier.io/), etc) this is an extra layer to ensure we're all writing highly consistent code and keeping our code reviews highly focused.

##### Extensions

In addition to [**Settings**](#markdown-header-settings), there are also suggestions for extensions that further bolster code consistency and ensure our aforemention lint tooling can be as effecient as possible.

#### Others

Here are some integration plugins/extensions for other editors:

- [ESLint](https://eslint.org/docs/user-guide/integrations#editors)
- [stylelint](https://stylelint.io/user-guide/complementary-tools/#editor-plugins)
- [Prettier](https://prettier.io/docs/en/editors.html)
