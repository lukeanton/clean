export type AlignmentType = 'center' | 'left' | 'right';

export type AppRootIdType = 'root' | '__next';

export type AssetType = 'audio' | 'document' | 'image' | 'video';

export type ButtonSizeType = 'small' | 'medium' | 'large';

export type ButtonType = 'button' | 'submit' | 'reset';

export type ButtonVariantType =
  | 'cta'
  | 'cta--tertiary'
  | 'danger'
  | 'danger--tertiary'
  | 'dark'
  | 'primary'
  | 'secondary'
  | 'tertiary'
  | 'primary-inverse';

export type DialogType = 'error' | 'info' | 'success' | 'warning';

export type FitBoundsExternalPaddingType = 'small' | 'medium' | 'large';

export type IconIdType =
  | 'id_add_circle_icon'
  | 'id_add_rectangle_icon'
  | 'id_arrow_icon'
  | 'id_audio_asset_icon'
  | 'id_audio_icon'
  | 'id_audio_tab_icon'
  | 'id_bin_icon'
  | 'id_bookmark_icon'
  | 'id_caret_down_icon'
  | 'id_caret_icon'
  | 'id_caret_up_icon'
  | 'id_close_icon'
  | 'id_close_quote_icon'
  | 'id_contacts_tab_icon'
  | 'id_copy_icon'
  | 'id_docx_icon'
  | 'id_download_icon'
  | 'id_ekardo_icon'
  | 'id_email_bounced'
  | 'id_email_clicked'
  | 'id_email_complaint'
  | 'id_email_delivered'
  | 'id_email_none'
  | 'id_email_not_clicked'
  | 'id_email_opened'
  | 'id_email_rejected'
  | 'id_email_sent'
  | 'id_enter_icon'
  | 'id_error_icon'
  | 'id_eye_hide_icon'
  | 'id_eye_show_icon'
  | 'id_file_asset_icon'
  | 'id_four_icon'
  | 'id_four_mirrored_icon'
  | 'id_general_tab_icon'
  | 'id_group_icon'
  | 'id_groups_tab_icon'
  | 'id_image_asset_icon'
  | 'id_info_icon'
  | 'id_information_box_icon'
  | 'id_invalid_icon'
  | 'id_key_tab_icon'
  | 'id_minus_icon'
  | 'id_navigation_tab_icon'
  | 'id_new_window_icon'
  | 'id_notes_tab_icon'
  | 'id_open_quote_icon'
  | 'id_pause_icon'
  | 'id_pdf_icon'
  | 'id_play_icon'
  | 'id_plus_icon'
  | 'id_preloader_icon'
  | 'id_preloader_secondary_icon'
  | 'id_print_icon'
  | 'id_related_tab_icon'
  | 'id_search_icon'
  | 'id_seo_tab_icon'
  | 'id_star_icon'
  | 'id_style_tab_icon'
  | 'id_success_icon'
  | 'id_three_dots_icon'
  | 'id_tick_icon'
  | 'id_tooltip_icon'
  | 'id_tooltip_secondary_icon'
  | 'id_transcript_tab_icon'
  | 'id_units_tab_icon'
  | 'id_video_asset_icon'
  | 'id_warning_icon'
  | 'id_xd_icon'
  | 'id_xlsx_icon';

export type LayoutIdType = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export type NavigationButtonDirectionType = 'previous' | 'next';

export type RotationCssSuffixType = '0' | '90' | '180' | '270';

export type SpacingSizeType =
  | '6x-large'
  | '5x-large'
  | '4x-large'
  | '3x-large'
  | '2x-large'
  | 'x-large'
  | 'large'
  | 'small'
  | 'x-small'
  | '2x-small'
  | '3x-small'
  | 'none';

export type TargetType = '_blank' | '_self' | '_parent' | 'top';

export type TabThemeType = 'left-align' | 'switch' | 'underline';

export type TooltipPlacementType = 'bottom' | 'left' | 'right' | 'top';
