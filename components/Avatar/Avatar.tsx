import React, { useState } from 'react';

import cx from 'classnames';

import { getRandomColor } from './Avatar.helpers';
import { AvatarProps } from './Avatar.types';
import './Avatar.css';

const Avatar: React.FC<AvatarProps> = ({ additionalClassNames, image, roleIndicator, size = 'medium', title }) => {
  const [avatarBackgroundColor] = useState(getRandomColor);

  const avatarClassNames = cx(`c-avatar c-avatar--${size}`, additionalClassNames, {
    'c-avatar__title': Boolean(title),
  });

  const roleIndicatorComponent = roleIndicator ? (
    <div className={cx('c-role-indicator', `c-role-indicator--${roleIndicator.position || 'top-right'}`)}>{roleIndicator.component}</div>
  ) : null;

  const avatarText =
    !image && title
      ? title
          .split(' ')
          .slice(0, 2)
          .map((item) => item.charAt(0).toUpperCase())
          .join('')
      : '';

  return (
    <React.Fragment>
      {roleIndicatorComponent}
      <span
        className={avatarClassNames}
        style={{
          backgroundColor: avatarBackgroundColor,
        }}
      >
        {image ?? (
          <abbr className="c-avatar__initials" title={avatarText}>
            {avatarText}
          </abbr>
        )}
      </span>
    </React.Fragment>
  );
};

export { Avatar };
