import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../interfaces';

export interface IRoleIndicator {
  /*
   * Provide a role indicator component that will be displayed on the users avatar
   */
  component: ReactNode;
  /*
   * Specify the positioning of the role indicator
   */
  position?: 'bottom-right' | 'top-right';
}

export interface AvatarProps extends IAdditionalClassNames {
  /*
   * Provide an image that will be displayed as the users avatar
   */
  image?: ReactNode;
  /*
   * Specify a user role indicator and optional positioning to provide more information about the user
   */
  roleIndicator?: IRoleIndicator;
  /*
   * Specify a size of the avatar
   */
  size?: 'small' | 'medium';
  /*
   * Specify a meaningful title for the users avatar
   */
  title?: string;
}
