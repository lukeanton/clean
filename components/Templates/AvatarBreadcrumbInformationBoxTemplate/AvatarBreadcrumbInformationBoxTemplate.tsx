import React from 'react';

import cx from 'classnames';

import { AvatarBreadcrumbSectionTemplate } from '../AvatarBreadcrumbSectionTemplate';

import { AvatarBreadcrumbInformationBoxTemplateProps } from './AvatarBreadcrumbInformationBoxTemplate.types';

import { InformationBox } from '../../InformationBox';

import './AvatarBreadcrumbInformationBoxTemplate.css';

const AvatarBreadcrumbInformationBoxTemplate: React.FC<AvatarBreadcrumbInformationBoxTemplateProps> = ({
  additionalClassNames,
  breadcrumbItems,
  message,
  title = 'Netfront',
}) => {
  const cardListClassNames = cx('c-menu', additionalClassNames);

  return (
    <div className={cardListClassNames}>
      <AvatarBreadcrumbSectionTemplate breadcrumbItems={breadcrumbItems} title={title} />
      <InformationBox iconId="id_info_icon" message={message} />
    </div>
  );
};

export { AvatarBreadcrumbInformationBoxTemplate };
