import { AvatarBreadcrumbSectionTemplateProps } from '../AvatarBreadcrumbSectionTemplate';

import { InformationBoxProps } from '../../InformationBox';

export interface AvatarBreadcrumbInformationBoxTemplateProps extends AvatarBreadcrumbSectionTemplateProps, InformationBoxProps {}
