import { IAdditionalClassNames } from '../../../interfaces';
import { IBreadcrumbItem } from '../../Breadcrumbs';

export interface AvatarBreadcrumbCtaSectionTemplateProps extends IAdditionalClassNames {
  /**
   * Items in the breadcrumb trail
   */
  ctaBreadcrumbItems: IBreadcrumbItem[];
  /*
   * The text value displayed in the component
   */
  ctaButtonText: string;
  /*
   * The text value displayed in the component
   */
  sectionTitle: string;
}
