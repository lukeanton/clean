import React from 'react';

import cx from 'classnames';

import { AvatarBreadcrumbSectionTemplate } from '../AvatarBreadcrumbSectionTemplate';

import { AvatarBreadcrumbCtaSectionTemplateProps } from './AvatarBreadcrumbCtaSectionTemplate.types';

import { Button } from '../../Buttons';
import { Toolbar } from '../../Toolbar';

import './AvatarBreadcrumbSectionTemplate.css';

const AvatarBreadcrumbCtaSectionTemplate: React.FC<AvatarBreadcrumbCtaSectionTemplateProps> = ({
  additionalClassNames,
  ctaBreadcrumbItems,
  ctaButtonText,
  children,
  sectionTitle,
}) => {
  return (
    <div className={cx('c-breadcrumb-with-avatar-and-cta', additionalClassNames)}>
      <Toolbar
        childrenEnd={
          <React.Fragment>
            {children}
            <Button text={ctaButtonText} variant="cta" onClick={() => {}} />
          </React.Fragment>
        }
        childrenStart={<AvatarBreadcrumbSectionTemplate breadcrumbItems={ctaBreadcrumbItems} title={sectionTitle} />}
      />
    </div>
  );
};

export { AvatarBreadcrumbCtaSectionTemplate };
