import { IAdditionalClassNames } from '../../../interfaces';
import { IBreadcrumbItem } from '../../Breadcrumbs';

export interface AvatarBreadcrumbSectionTemplateProps extends IAdditionalClassNames {
  /**
   * Items in the breadcrumb trail
   */
  breadcrumbItems: IBreadcrumbItem[];
  /*
   * The text value displayed in the component
   */
  title: string;
}
