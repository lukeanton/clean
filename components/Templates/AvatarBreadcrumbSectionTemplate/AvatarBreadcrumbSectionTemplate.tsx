import React from 'react';

import cx from 'classnames';

import { AvatarBreadcrumbSectionTemplateProps } from './AvatarBreadcrumbSectionTemplate.types';

import { Avatar } from '../../Avatar';
import { Breadcrumbs } from '../../Breadcrumbs';

import './AvatarBreadcrumbSectionTemplate.css';

const AvatarBreadcrumbSectionTemplate: React.FC<AvatarBreadcrumbSectionTemplateProps> = ({
  additionalClassNames,
  breadcrumbItems,
  title,
}) => {
  const AvatarBreadcrumbSectionTemplateClassNames = cx('c-breadcrumb-with-avatar', additionalClassNames);

  return (
    <div className={AvatarBreadcrumbSectionTemplateClassNames}>
      <Avatar title={title} />
      <div className="c-breadcrumb-with-avatar__text-content">
        <Breadcrumbs items={breadcrumbItems} />
        <h1 className="c-breadcrumb-with-avatar__title">{title}</h1>
      </div>
    </div>
  );
};

export { AvatarBreadcrumbSectionTemplate };
