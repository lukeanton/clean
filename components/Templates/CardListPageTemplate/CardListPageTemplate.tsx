import React from 'react';

import cx from 'classnames';

import { AvatarBreadcrumbSectionTemplate } from '../AvatarBreadcrumbSectionTemplate';

import { CardListPageTemplateProps } from './CardListPageTemplate.types';

import { CardList } from '../../CardList';
import { InformationBox } from '../../InformationBox';

import './CardListPageTemplate.css';

const CardListPageTemplate: React.FC<CardListPageTemplateProps> = ({
  addNewButtonCard,
  additionalClassNames,
  breadcrumbItems,
  informationBoxMessage,
  items,
  pageTitle = 'Netfront',
}) => {
  const cardListClassNames = cx('c-menu', additionalClassNames);

  return (
    <div className={cardListClassNames}>
      <AvatarBreadcrumbSectionTemplate breadcrumbItems={breadcrumbItems} title={pageTitle} />
      <InformationBox iconId="id_info_icon" message={informationBoxMessage} />
      <CardList addNewButtonCard={addNewButtonCard} items={items} />
    </div>
  );
};

export { CardListPageTemplate };
