import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../../interfaces';
import { IBreadcrumbItem } from '../../Breadcrumbs';

export interface CardListPageTemplateProps extends IAdditionalClassNames {
  /*
   * Add new button card
   */
  addNewButtonCard?: ReactNode;
  /**
   * Items in the breadcrumb trail
   */
  breadcrumbItems: IBreadcrumbItem[];
  /**
   * The message displayed in the information box
   */
  informationBoxMessage: string;
  /*
   * The list of card components
   */
  items: ReactNode[];
  /*
   * The page title
   */
  pageTitle: string;
}
