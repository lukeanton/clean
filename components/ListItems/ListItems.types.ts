import { ImageCardProps } from '../Card';

import { IAdditionalClassNames } from '../../interfaces';

export interface ListItemsProps extends IAdditionalClassNames {
  items: ImageCardProps[];
}
