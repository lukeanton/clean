import React from 'react';

import cx from 'classnames';

import { Spacing } from '../Spacing';

import { ListItemsProps } from './ListItems.types';

import './ListItems.css';

const ListItems: React.FC<ListItemsProps> = ({ additionalClassNames, items }) => {
  return (
    <ul aria-label="A list of assets" className={cx('c-list-items', additionalClassNames)}>
      {items.map(
        ({ additionalClassNames: additionalListItemClassNames, image: { altText, src }, settingsButton, supportiveText, title }) => {
          const listItemsClassNames = cx('c-list-item', additionalListItemClassNames);
          const listItemsInnerClassNames = cx({
            'c-list-item__header': title,
          });

          return (
            <Spacing>
              <li className={listItemsClassNames}>
                <div className="c-list-item__title-section">
                  <img alt={altText} className="c-list-item__title-section-image" src={src} />
                  <div className={listItemsInnerClassNames}>
                    <span className="c-list-item__title-section-text">{title}</span>
                  </div>
                </div>
                <div className="c-list-item__settings-button">{settingsButton}</div>
                <span className="h-hide-visually">{supportiveText}</span>
              </li>
            </Spacing>
          );
        }
      )}
    </ul>
  );
};

export { ListItems };
