import { PartialPick } from '@netfront/common-library';

import { IIconId, IIdentifier, IIsLabelHidden, IIsRequired } from '../../interfaces';

export interface IMessage {
  error: string;
  success: string;
}

export interface InputFieldWrapperProps extends PartialPick<IIconId, 'iconId'>, IIdentifier, IIsLabelHidden, IIsRequired {
  label?: string;
  message?: IMessage;
}
