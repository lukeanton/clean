import { SpacingSizeType } from '../../types';

export interface SpacingProps {
  /**
   * The amount spacing beneath the wrapped components
   */
  size?: SpacingSizeType;
}
