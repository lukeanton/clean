import React, { useRef } from 'react';

import cx from 'classnames';
import Dropzone, { DropzoneRef } from 'react-dropzone';

import { Label } from '../Label';

import { DropzoneFileUploadProps } from './DropzoneFileUpload.types';

import './DropzoneFileUpload.css';

const DropzoneFileUpload: React.FC<DropzoneFileUploadProps> = ({
  additionalClassNames,
  children,
  file,
  isAcceptMultipleFiles = false,
  isLabelHidden = false,
  isRequired = false,
  labelText,
  onDrop,
}) => {
  const dropzoneRef = useRef<DropzoneRef>(null);

  const handleOpenDialog = () => {
    if (!dropzoneRef.current) {
      return;
    }

    dropzoneRef.current.open();
  };

  return (
    <React.Fragment>
      {!file ? (
        <React.Fragment>
          <Label forId="dropzone-file-upload" isHidden={isLabelHidden} isRequired={isRequired} labelText={labelText} />
          <button
            className={cx('c-dropzone--container', additionalClassNames)}
            data-testid="qa-dropzone-file-upload-container"
            id="dropzone-file-upload"
            type="button"
            onClick={handleOpenDialog}
          >
            <Dropzone ref={dropzoneRef} multiple={isAcceptMultipleFiles} noClick noKeyboard onDropAccepted={onDrop}>
              {({ getRootProps, getInputProps, isDragActive }) => {
                return (
                  <div
                    data-testid="qa-dropzone-file-upload"
                    // eslint-disable-next-line react/jsx-props-no-spreading
                    {...getRootProps({
                      className: cx('c-dropzone', {
                        'c-dropzone--is-active': Boolean(isDragActive),
                      }),
                    })}
                  >
                    <Label forId="dropzone-file-upload-input" labelText="Drop file or click to upload" />
                    <input
                      id="dropzone-file-upload-input"
                      title="dropzone-file-upload-input"
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      {...getInputProps()}
                    />

                    {!isAcceptMultipleFiles && (
                      <span>
                        <strong>Maximum</strong> 1 file
                      </span>
                    )}
                  </div>
                );
              }}
            </Dropzone>
          </button>
        </React.Fragment>
      ) : (
        <React.Fragment>{children}</React.Fragment>
      )}
    </React.Fragment>
  );
};

export { DropzoneFileUpload };
