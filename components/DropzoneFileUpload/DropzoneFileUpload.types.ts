import { ReactNode } from 'react';

import { IAdditionalClassNames, IIsLabelHidden, IIsRequired, ILabelText } from '../../interfaces';

export interface DropzoneFileUploadProps extends IAdditionalClassNames, IIsLabelHidden, IIsRequired, ILabelText {
  /**
   * Provide children elements to the Dropzone
   */
  children?: ReactNode;
  /**
   * The dropped / uploaded file
   */
  file?: File[] | undefined;
  /**
   * Specify if the dropzone will accept multiple files on drop / upload
   */
  isAcceptMultipleFiles?: boolean;
  /**
   * Provide an onDrop function to handle the file drop / upload
   */
  onDrop: (acceptedFiles: File[]) => void;
}
