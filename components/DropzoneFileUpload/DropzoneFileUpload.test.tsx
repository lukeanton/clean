import React from 'react';
import { render, screen } from '@testing-library/react';
import { DropzoneFileUpload } from './DropzoneFileUpload';

describe('DropzoneFileUpload', () => {
  const handleDrop = () => {};

  describe('When there is no file uploaded', () => {
    test('it should display the dropzone', () => {
      render(<DropzoneFileUpload fileType="" onDrop={handleDrop} />);

      const dropzoneFileUpload = screen.queryByTestId('qa-dropzone-file-upload');
      const dropzoneFileUploadContainer = screen.queryByTestId('qa-dropzone-file-upload-container');
      expect(dropzoneFileUpload).toBeInTheDocument();
      expect(dropzoneFileUploadContainer).toBeInTheDocument();
    });

    test('it should not display the children', () => {
      const children = 'This is a children uploaded';

      render(<DropzoneFileUpload children={children} fileType="" onDrop={handleDrop} />);

      const uploadedFilePreview = screen.queryByText(children);
      expect(uploadedFilePreview).toBeNull();
    });

    describe('When there is file uploaded', () => {
      const blobParts: BlobPart[] = [];
      const file = new File(blobParts, 'file.png');

      test('it should not display the dropzone', () => {
        const children = 'This is a file';

        render(<DropzoneFileUpload children={children} file={file} fileType="png" onDrop={handleDrop} />);

        const dropzoneFileUpload = screen.queryByTestId('qa-dropzone-file-upload');
        const dropzoneFileUploadContainer = screen.queryByTestId('qa-dropzone-file-upload-container');
        expect(dropzoneFileUpload).toBeNull();
        expect(dropzoneFileUploadContainer).toBeNull();
      });

      test('it should display the children', () => {
        const children = 'This is a children uploaded';
        render(<DropzoneFileUpload children={children} file={file} fileType="xlsx" onDrop={handleDrop} />);

        const uploadedFilePreview = screen.queryByText(children);
        expect(uploadedFilePreview).toBeInTheDocument();
      });
    });
  });
});
