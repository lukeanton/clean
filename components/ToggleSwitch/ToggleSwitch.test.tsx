import React from 'react';

import { render, screen, fireEvent } from '@testing-library/react';
import { ToggleSwitch } from './ToggleSwitch';
import { ToggleSwitchProps } from './ToggleSwitch.types';

describe('ToggleSwitch', () => {
  let props: ToggleSwitchProps = {
    id: '',
    isChecked: false,
    onChange: () => {},
  };

  describe('When the toggle is rendered with no errors', () => {
    test('the toggle should render with no errors', () => {
      render(<ToggleSwitch {...props} />);
      const toggleSwitch = screen.queryByTestId('qa-toggle-switch');
      expect(toggleSwitch).toBeTruthy();
    });
  });

  describe('When the toggle is not checked and is clicked', () => {
    props.isChecked = true;
    test('the toggle should display checked', () => {
      render(<ToggleSwitch {...props} />);

      const input = screen.queryByTestId('qa-toggle-switch-input');
      expect(input).toBeChecked();
    });
  });
});
