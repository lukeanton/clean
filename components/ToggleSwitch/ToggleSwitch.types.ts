import { IAdditionalClassNames, IIdentifier, IIsDisabled, IIsLabelHidden, ILabelText } from '../../interfaces';

export interface ToggleSwitchProps extends IAdditionalClassNames, IIdentifier, IIsLabelHidden, IIsDisabled, ILabelText {
  isChecked: boolean;
  onChange: () => void;
}
