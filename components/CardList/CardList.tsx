import React from 'react';

import cx from 'classnames';

import { CardListProps } from './CardList.types';

import './CardList.css';

const CardList: React.FC<CardListProps> = ({ addNewButtonCard, additionalClassNames, items }) => {
  const cardListClassNames = cx('c-card-list', additionalClassNames);

  return (
    <div className={cardListClassNames} data-testid="qa-card">
      {items.map((item) => item)}
      {addNewButtonCard}
    </div>
  );
};

export { CardList };
