import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../interfaces';

export interface CardListProps extends IAdditionalClassNames {
  /*
   * Provide an add new button card component that will allow a user to add a new item
   */
  addNewButtonCard?: ReactNode;
  /*
   * Provide a array of card components that will be displayed as a list
   */
  items: ReactNode[];
}
