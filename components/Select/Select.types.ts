import {
  IAdditionalClassNames,
  IErrorMessage,
  IIdentifier,
  IIsDisabled,
  IIsLabelHidden,
  IIsRequired,
  ILabelText,
  IName,
} from '../../interfaces';

export interface SelectProps
  extends IAdditionalClassNames,
    IErrorMessage,
    IIdentifier,
    IIsDisabled,
    IIsLabelHidden,
    IIsRequired,
    ILabelText,
    IName {
  /**
   * The onChange function for selecting a dropdown option
   */
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  /**
   * The available dropdown items
   */
  options: IOption[];
  /**
   * The selected dropdown value
   */
  value?: number | string;
}

export interface IOption extends IIsDisabled, IName {
  /**
   * The unique id for the dropdown option
   */
  id: number;
  /**
   * The value of the dropdown option
   */
  value: number | string;
}
