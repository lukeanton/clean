import React from 'react';

import { render, screen, fireEvent } from '@testing-library/react';

import { Select } from './Select';

describe('Select', () => {
  const handleChange = () => {};

  const options = [
    { id: 1, name: 'Jack Torrance', value: 1 },
    { id: 2, name: 'Wendy Torrance', value: 2 },
    { id: 3, name: 'Danny Torrance', value: 3 },
    { id: 4, name: 'Mr. Ullman', value: 4 },
    { id: 5, name: 'Dick Halloran', value: 5 },
  ];

  it('should render the select dropdown on the screen', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} />);

    const select = screen.getByTestId('qa-select');

    expect(select).toBeInTheDocument();
  });

  it('should display all options provided to the select dropdown', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} />);

    const allOptions = screen.getAllByTestId('qa-select-options');

    expect(allOptions.length).toBe(options.length);
  });

  it('should show the default option when a value is not provided initially', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} />);

    const defaultOption = screen.queryByTestId('qa-select-default-option');

    expect(defaultOption).toBeInTheDocument();
  });

  it('should display the value when a value is selected or pre-selected', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} value="some value" />);

    const defaultOption = screen.queryByTestId('qa-select-default-option');

    expect(defaultOption).not.toBeInTheDocument();
  });

  it('should be disabled when isDisabled is provided to the select dropdown', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} value="some value" isDisabled />);
    const select = screen.getByTestId('qa-select');

    expect(select).toBeDisabled();
  });

  it('should display the expected text when the value is selected', () => {
    render(<Select name="dropdown" onChange={handleChange} options={options} value={2} />);

    const selectedOption = screen.getByDisplayValue('Wendy Torrance');

    expect(selectedOption).toBeInTheDocument();
  });

  describe('onChange', () => {
    describe('when onChange is passed in', () => {
      it('should call onChange with the expected argument', () => {
        const onChangeSpy = jest.fn();

        render(<Select name="dropdown" onChange={onChangeSpy} options={options} value={2} />);

        const select = screen.getByTestId('qa-select');

        fireEvent.change(select);

        expect(onChangeSpy).toBeCalledWith('2');
      });
    });
  });
});
