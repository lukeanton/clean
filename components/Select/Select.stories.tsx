import React from 'react';

import { Meta, Story } from '@storybook/react';
import { Select } from './Select';
import { SelectProps } from './Select.types';
import { Icons } from '../Icons';
import './Select.css';

const options = [
  { id: 1, name: 'Jack Torrance', value: 1 },
  { id: 2, name: 'Wendy Torrance', value: 2 },
  { id: 3, name: 'Danny Torrance', value: 3 },
  { id: 4, name: 'Mr. Ullman', value: 4 },
  { id: 5, name: 'Dick Halloran', value: 5 },
];

const optionsDisabled = [
  { id: 1, name: 'Jack Torrance', value: 1 },
  { id: 2, name: 'Wendy Torrance', value: 2 },
  { id: 3, name: 'Danny Torrance', value: 3, isDisabled: true },
  { id: 4, name: 'Mr. Ullman', value: 4 },
  { id: 5, name: 'Dick Halloran', value: 5, isDisabled: true },
];

export default {
  title: 'Components/Select',
  component: Select,
  argTypes: {
    options: {
      control: { type: 'none' },
    },
    value: {
      options: [1, 2, 3, 4, 5],
      control: { type: 'radio' },
    },
  },
} as Meta;

const handleChangeValue = (event) => {
  return event.target.value;
};

const Template: Story<SelectProps> = (args: any) => {
  return (
    <div style={{ width: '40%' }}>
      <Icons />
      <Select {...args} />
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
  labelText: 'Developers',
  isLabelHidden: true,
  options: options,
  name: 'dropdown',
  onChange: handleChangeValue,
};

export const value = Template.bind({});
value.args = {
  labelText: 'Developers',
  options: options,
  value: 2,
};

export const Disabled = Template.bind({});
Disabled.args = {
  labelText: 'Developers',
  options: options,
  value: 2,
  isDisabled: true,
};

export const DisabledItems = Template.bind({});
DisabledItems.args = {
  labelText: 'Developers',
  options: optionsDisabled,
  value: 4,
};

export const Required = Template.bind({});
Required.args = {
  labelText: 'Developers',
  options: options,
  isRequired: true,
};

export const RequiredWithValidationText = Template.bind({});
RequiredWithValidationText.args = {
  errorMessage: 'Developers selection is required',
  labelText: 'Developers',
  options: options,
  isRequired: true,
};
