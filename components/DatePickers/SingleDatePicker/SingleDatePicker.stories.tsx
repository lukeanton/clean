import React from 'react';
import { Story, Meta } from '@storybook/react';
import { SingleDatePicker } from './SingleDatePicker';

export default {
  title: 'components/SingleDatePicker',
  component: SingleDatePicker,
} as Meta;

const Template = (args) => <SingleDatePicker {...args} />;

export const Basic = Template.bind({});
Basic.args = {};
