export interface SingleDatePickerProps {
  maxDate?: string;
  minDate?: string;
  onChangeHandler?: (date: Date) => void;
  selectedDate: Date | undefined;
}
