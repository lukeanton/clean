import { IDateRange } from '../DatePickers.interfaces';

export interface DateRangePickerProps {
  onChangeHandler?: (date: IDateRange) => void;
  selectedDate: IDateRange | undefined;
}
