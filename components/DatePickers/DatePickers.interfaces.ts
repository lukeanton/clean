export interface IDateRange {
  end: Date;
  start: Date;
}
