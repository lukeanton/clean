import { IErrorMessage } from '../../interfaces';

export interface ConfirmationMessagesProps extends IErrorMessage {
  id: string | undefined;
  successMessage?: string;
}
