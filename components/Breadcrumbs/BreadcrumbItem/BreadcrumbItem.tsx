import React from 'react';

import cx from 'classnames';

import { BreadcrumbItemDivider } from '../BreadcrumbItemDivider';

import { BreadcrumbItemProps } from './BreadcrumbItem.types';

import './BreadcrumbItem.css';

const BreadcrumbItem: React.FC<BreadcrumbItemProps> = ({ additionalClassNames, content, divider, isLast = false }) => {
  return (
    <li className={cx('c-breadcrumb-item', additionalClassNames)} data-testid="qa-breadcrumb-item">
      {content}
      {!isLast && <div className="c-breadcrumb-item__divider-container">{divider ?? <BreadcrumbItemDivider />}</div>}
    </li>
  );
};

export { BreadcrumbItem };
