import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../../interfaces';

export interface IBreadcrumbItem extends IAdditionalClassNames {
  /**
   * The content to be rendered
   */
  content: ReactNode;
  /**
   * A unique key that is required for React
   */
  key: string;
}

export interface BreadcrumbItemProps extends Omit<IBreadcrumbItem, 'key'> {
  /**
   * The divider between items
   */
  divider?: ReactNode;
  /**
   * Flag for last item in the list
   */
  isLast?: boolean;
}
