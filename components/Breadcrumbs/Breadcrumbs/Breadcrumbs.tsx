import React from 'react';

import { BreadcrumbItem } from '../BreadcrumbItem';

import { BreadcrumbsProps } from './Breadcrumbs.types';

import './Breadcrumbs.css';

const Breadcrumbs: React.FC<BreadcrumbsProps> = ({ itemDivider, items }) => {
  const lastIndex = items.length - 1;

  return (
    <nav aria-label="Breadcrumbs" data-testid="qa-breadcrumbs">
      <ol className="c-breadcrumbs" data-testid="qa-breadcrumbs">
        {items.map(({ content, key }, index) => {
          return <BreadcrumbItem key={key} content={content} divider={itemDivider} isLast={index === lastIndex} />;
        })}
      </ol>
    </nav>
  );
};

export { Breadcrumbs };
