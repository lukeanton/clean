import { ReactNode } from 'react';

import { IBreadcrumbItem } from '../BreadcrumbItem';

export interface BreadcrumbsProps {
  /**
   * The divider between items
   */
  itemDivider?: ReactNode;
  /**
   * Items in the breadcrumb trail
   */
  items: IBreadcrumbItem[];
}
