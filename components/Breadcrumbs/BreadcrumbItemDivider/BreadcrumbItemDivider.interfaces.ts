import { ReactNode } from 'react';

export interface BreadcrumbItemDividerProps {
  divider?: ReactNode;
}
