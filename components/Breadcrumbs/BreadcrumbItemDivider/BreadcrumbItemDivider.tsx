import React from 'react';

import { BreadcrumbItemDividerProps } from './BreadcrumbItemDivider.interfaces';

import './BreadcrumbItemDivider.css';

const BreadcrumbItemDivider = ({ divider = '/' }: BreadcrumbItemDividerProps) => {
  return (
    <span className="c-breadcrumb-item-divider" data-testid="qa-breadcrumb-item-divider" aria-hidden>
      {divider}
    </span>
  );
};

export { BreadcrumbItemDivider };
