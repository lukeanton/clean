import { IAdditionalClassNames, IForId, IIsRequired, ILabelText } from '../../interfaces';
import { SpacingSizeType } from '../../types';

export interface LabelProps extends IAdditionalClassNames, IForId, IIsRequired, ILabelText {
  /**
   * Specify if the label is hidden visually
   */
  isHidden?: boolean;
  /**
   * The spacing between the label and input
   */
  spacing?: SpacingSizeType;
}
