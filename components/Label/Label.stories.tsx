import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Label } from './Label';
import { LabelProps } from './Label.types';

export default {
  title: 'Components/Label',

  component: Label,
} as Meta;

const Template: Story<LabelProps> = (args) => <Label {...args} />;

export const Default = Template.bind({});

Default.args = {
  forId: 'description',
  labelText: 'Description',
  isRequired: false,
};
