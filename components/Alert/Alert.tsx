import React, { useEffect, useState } from 'react';

import cx from 'classnames';

import { ButtonIconOnly } from '../ButtonIconOnly';
import { Icon } from '../Icon';

import { AlertProps } from './Alert.types';

import './Alert.css';

const Alert: React.FC<AlertProps> = ({ iconId, isDisplayCloseButton, message, timeAlertIsVisibleInMilliseconds = 15000, title }) => {
  const [isVisible, setIsVisible] = useState<boolean>(true);

  const handleClick = () => {
    setIsVisible(false);
  };

  useEffect(() => {
    if (!isVisible) {
      return undefined;
    }

    const timerId = setTimeout(() => {
      setIsVisible(false);
    }, timeAlertIsVisibleInMilliseconds);

    return () => clearTimeout(timerId);
  }, [isVisible, timeAlertIsVisibleInMilliseconds]);

  return (
    <React.Fragment>
      <div
        className={cx('c-message', {
          'h-hide-visually': !isVisible,
          'c-message--has-close-button': !isDisplayCloseButton,
        })}
        data-testid="qa-alert"
        role="alert"
      >
        <Icon className="c-message___icon" id={iconId} />
        <div className="c-message___content">
          {title ? <h4 className="c-message___title">{title}</h4> : null}
          <p className="c-message___text">{message}</p>
        </div>
        {isDisplayCloseButton ? (
          <ButtonIconOnly
            additionalClassNames="c-message___close"
            iconId="id_close_icon"
            text="Dismiss notification"
            onClick={handleClick}
          />
        ) : null}
      </div>
    </React.Fragment>
  );
};

export { Alert };
