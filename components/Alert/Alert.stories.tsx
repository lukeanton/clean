import React from 'react';

import { Meta, Story } from '@storybook/react';

import { AlertProps } from './Alert.types';

import { Icons } from '../Icons';
import { Alert } from './Alert';

export default {
  title: 'Components/Alert',
  component: Alert,
  argTypes: {
    onClick: { control: 'none' },
    text: { control: 'none' },
    type: { control: 'none' },
    icon: {
      control: {
        type: 'select',
        options: [
          'id_success_icon',
          'id_error_icon',
          'id_warning_icon',
          'id_info_icon',
          'id_alert_close_icon',
          'id_close_icon',
          'id_alert_close_icon_secondary',
        ],
      },
    },
  },
} as Meta;

const Template: Story<AlertProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <Alert {...args} />
    </React.Fragment>
  );
};

export const Error = Template.bind({});
Error.args = {
  message: 'Updating your email address will disconnect your apple account with this profile.',
  iconId: 'id_error_icon',
  isDisplayCloseButton: true,
};

export const Info = Template.bind({});
Info.args = {
  message: 'This is an information message',
  iconId: 'id_info_icon',
  isDisplayCloseButton: true,
};

export const Success = Template.bind({});
Success.args = {
  message: 'This is a success message',
  iconId: 'id_success_icon',
  isDisplayCloseButton: true,
};

export const Warning = Template.bind({});
Warning.args = {
  message: 'This is a warning message',
  iconId: 'id_warning_icon',
  isDisplayCloseButton: true,
};

export const WarningWithTitle = Template.bind({});
WarningWithTitle.args = {
  message: 'Updating your email address will disconnect your apple account with this profile.',
  title: 'Updating email address',
  iconId: 'id_warning_icon',
};

export const WarningWithTitleAndClose = Template.bind({});
WarningWithTitleAndClose.args = {
  message: 'Updating your email address will disconnect your apple account with this profile.',
  title: 'Updating email address',
  iconId: 'id_warning_icon',
  isDisplayCloseButton: true,
};
