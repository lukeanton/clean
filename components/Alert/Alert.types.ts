import { IIconId } from '../../interfaces';

export interface AlertProps extends IIconId {
  /**
   * Boolean value to display close button
   */
  isDisplayCloseButton?: boolean;
  /**
   * Message displayed in notification
   */
  message: string;
  /**
   * Time the alert is visible in milliseconds
   */
  timeAlertIsVisibleInMilliseconds?: number;
  /**
   * Title displayed in notification
   */
  title?: string;
}
