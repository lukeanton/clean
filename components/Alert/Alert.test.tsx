import React from 'react';

import { render, screen, fireEvent, waitFor } from '@testing-library/react';

import { Alert } from './Alert';

describe('Alert', () => {
  test('it should display the alert items', () => {
    render(<Alert iconId="id_close_icon" message="Test message" />);
    const alert = screen.getByText('Test message');
    expect(alert).not.toHaveClass('h-hide-visually');
    expect(alert).toBeInTheDocument();
  });

  describe('When timeout is beyond 5 seconds', () => {
    test('it should not display the alert item', () => {
      render(<Alert iconId="id_close_icon" message="Test message" />);
      waitFor(
        () => {
          const alert = screen.queryByTestId('qa-alert');
          expect(alert).toHaveClass('h-hide-visually');
        },
        { timeout: 6000 }
      );
    });
  });

  describe('When click to close the alert', () => {
    test('it should not display the alert', () => {
      render(<Alert iconId="id_close_icon" message="Test message" />);
      const closeButton = screen.getByRole('button');
      fireEvent.click(closeButton);
      const alert = screen.queryByTestId('qa-alert');

      expect(alert).toHaveClass('h-hide-visually');
    });
  });
});
