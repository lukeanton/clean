export interface CodeBlockProps {
  /**
   * The code content
   */
  content: string;
}
