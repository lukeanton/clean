import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta } from '@storybook/react/types-6-0';

import { CodeBlock } from '../CodeBlock/CodeBlock';

export default {
  title: 'Components/CodeBlock',
  component: CodeBlock,
} as Meta;

export const Base = () => <CodeBlock content="<p>Mate</p>" />;

export const CSS = () => <CodeBlock content="p {color: black;}" />;
