import React from 'react';

import dompurify from 'dompurify';

import { CodeBlockProps } from './CodeBlock.types';

import './CodeBlock.css';

const CodeBlock: React.FC<CodeBlockProps> = ({ content }) => {
  const { sanitize } = dompurify;

  return (
    <pre className="c-code-block">
      <code
        className="c-code__code"
        // We are sanitising input here as content comes from a rich text editor and will evaluate if there is a performance implication
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: sanitize(content) }}
      />
    </pre>
  );
};

export { CodeBlock };
