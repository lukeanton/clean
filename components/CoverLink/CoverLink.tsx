import React from 'react';

import cx from 'classnames';

import { CoverLinkProps } from './CoverLink.types';
import './CoverLink.css';

const CoverLink: React.FC<CoverLinkProps> = ({ additionalClassNames, href, linkComponent = 'a', supportiveText }) => {
  const coverLinkClassNames = cx('c-cover-link', additionalClassNames);

  const Link = linkComponent;

  return (
    <Link className={coverLinkClassNames} data-testid="qa-cover-link" href={href}>
      <span className="h-hide-visually">{supportiveText}</span>
    </Link>
  );
};

export { CoverLink };
