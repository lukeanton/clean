import React from 'react';
import { Meta, Story } from '@storybook/react';
import { CoverLinkProps } from './CoverLink.types';
import { CoverLink } from './CoverLink';

export default {
  title: 'Components/CoverLink',

  component: CoverLink,
} as Meta;

const Template: Story<CoverLinkProps> = (args) => {
  return <CoverLink {...args} />;
};

export const Base = Template.bind({});

Base.args = {
  href: 'https://www.google.com.au',
  supportiveText: 'google search',
};
