import React from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';

import { PreloaderProps } from './Preloader.types';

import './Preloader.css';

const Preloader: React.FC<PreloaderProps> = ({ additionalClassNames, iconId = 'id_preloader_icon', isLoading = false, message }) => {
  if (!isLoading) {
    return null;
  }

  const preloaderClassNames = cx('c-preloader', additionalClassNames);

  return (
    <div className={preloaderClassNames} data-testid="qa-preloader" role="status">
      <span className="preloader__spinner" data-testid="qa-preloader-spinner">
        <Icon className="c-preloader__spinner-icon" id={iconId} />
      </span>
      <span className="c-preloader__message" data-testid="qa-preloader-message">
        {message}
      </span>
    </div>
  );
};

export { Preloader };
