import React from 'react';

import { render, screen } from '@testing-library/react';

import { Preloader } from './Preloader';

describe('Preloader', () => {
  it('should display the preloader item', () => {
    render(<Preloader isLoading={true} iconId="id_preloader_icon" />);

    const preloader = screen.queryByTestId('qa-preloader');

    expect(preloader).toBeInTheDocument();
  });

  describe('Message prop', () => {
    describe('When there is a preloader message', () => {
      it('should display the message', () => {
        const message = 'This is a test message';

        render(<Preloader isLoading={true} iconId="id_preloader_icon" message={message} />);

        const preloader = screen.getByText(message);

        expect(preloader).toBeInTheDocument();
      });
    });

    describe('When there is not a preloader message', () => {
      it('should not display the message', () => {
        render(<Preloader isLoading={true} iconId="id_preloader_icon" />);

        const message = screen.queryByTestId('qa-preloader-message');

        expect(message).toHaveTextContent('');
      });
    });
  });

  describe('additionalClassName prop', () => {
    describe('When additionalClassName assigned to preloader component', () => {
      it('should have the class assigned to asset', () => {
        const additionalClassNames = 'project-preloader';

        render(<Preloader isLoading={true} iconId="id_preloader_icon" additionalClassNames={additionalClassNames} />);

        const preloader = screen.queryByTestId('qa-preloader');

        expect(preloader).toHaveClass('project-preloader');
      });
    });

    describe('When additionalClassName is not assigned to preloader component', () => {
      it('should have the class assigned to asset', () => {
        render(<Preloader isLoading={true} iconId="id_preloader_icon" />);

        const preloader = screen.queryByTestId('qa-preloader-spinner');

        expect(preloader).not.toHaveClass('project-preloader');
      });
    });
  });
});
