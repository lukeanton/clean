import { PartialPick } from '@netfront/common-library';

import { IAdditionalClassNames, IIconId } from '../../interfaces';

export interface PreloaderProps extends IAdditionalClassNames, PartialPick<IIconId, 'iconId'> {
  /**
   * The boolean whether loading is displayed
   */
  isLoading: boolean;
  /**
   * The message displayed wit the preloader
   */
  message?: string;
}
