import React from 'react';

import cx from 'classnames';
import { SketchPicker } from 'react-color';

import { ColorPickerProps } from './ColorPicker.types';

const ColorPicker: React.FC<ColorPickerProps> = ({ additionalClassNames, onChange, color, width = '300px' }) => {
  return <SketchPicker className={cx('c-color-picker', additionalClassNames)} color={color} width={width} onChange={onChange} />;
};

export { ColorPicker };
