import { ColorResult } from 'react-color';

import { IAdditionalClassNames } from '../../interfaces';

interface ColorPickerProps extends IAdditionalClassNames {
  /**
   * The color value
   */
  color: string;
  /**
   * The action to call value change an input
   */
  onChange: (updatedColor: ColorResult) => void;
  /**
   * Width of the color picker
   */
  width?: string;
}

export type { ColorPickerProps, ColorResult };
