/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-key */
import React from 'react';

import { capitalCase } from 'change-case';
import cx from 'classnames';
import { useSortBy, useTable } from 'react-table';

import { Icon } from '../Icon';

import { TableProps } from './Table.interfaces';

import './Table.css';

// eslint-disable-next-line @typescript-eslint/ban-types
function Table<T extends object>({ additionalClassNames, columns, data }: TableProps<T>) {
  const tableClassNames = cx('c-table', additionalClassNames);

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable<T>(
    {
      columns,
      data,
    },
    useSortBy
  );

  return (
    <div className="c-table-container">
      <table className={tableClassNames} {...getTableProps()}>
        <thead className="c-table__thead">
          {headerGroups.map(({ getHeaderGroupProps, headers }) => (
            <tr className="c-table__thead-tr" {...getHeaderGroupProps()}>
              {headers.map(({ getHeaderProps, getSortByToggleProps, isSorted, isSortedDesc, minWidth, render, width }) => (
                <th
                  className="c-table__thead-th"
                  {...getHeaderProps({
                    style: {
                      minWidth,
                      width,
                    },
                  })}
                  {...getSortByToggleProps()}
                >
                  <div className="c-table__thead-th-contents-container">
                    <div className="c-table__thead-th-contents-text">{render('Header')}</div>
                    {isSorted && (
                      <div className="c-table__thead-th-contents-sort">
                        <Icon className="c-table__sort-icon" id={isSortedDesc ? 'id_caret_down_icon' : 'id_caret_up_icon'} />
                      </div>
                    )}
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="c-table__tbody" {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);

            const { cells, getRowProps } = row;

            return (
              <tr className="c-table__tbody-tr" {...getRowProps()}>
                {cells.map(({ column: { id, minWidth, width }, getCellProps, render }) => {
                  return (
                    <td
                      className="c-table__tbody-td"
                      data-hr={capitalCase(id)}
                      {...getCellProps({
                        style: {
                          minWidth,
                          width,
                        },
                      })}
                    >
                      {render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export { Table };
