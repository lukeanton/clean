import { IAdditionalClassNames } from 'interfaces';
import { TableOptions } from 'react-table';

// eslint-disable-next-line @typescript-eslint/ban-types
export interface TableProps<T extends object> extends IAdditionalClassNames, TableOptions<T> {}
