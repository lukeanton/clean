import React from 'react';
import { Meta } from '@storybook/react';

import { Layout } from './Layout';
import { LayoutItem } from '../LayoutItem/LayoutItem';

export default {
  title: 'Layout/Layout',
  component: Layout,
  argTypes: {
    as: { control: 'none' },
    children: { control: 'none' },
  },
} as Meta;

export const FullWidth = (args) => (
  <Layout layoutId={1} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
  </Layout>
);

export const EvenSplit = (args) => (
  <Layout layoutId={2} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
  </Layout>
);

export const TwoColWideStart = (args) => (
  <Layout layoutId={3} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
  </Layout>
);

export const TwoColWideEnd = (args) => (
  <Layout layoutId={4} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
  </Layout>
);

export const TwoColSidebarStart = (args) => (
  <Layout layoutId={5} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
  </Layout>
);

export const TwoColSidebarEnd = (args) => (
  <Layout layoutId={6} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
  </Layout>
);

export const ThreeColWideStart = (args) => (
  <Layout layoutId={8} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 3</div>
    </LayoutItem>
  </Layout>
);

export const ThreeColWideEnd = (args) => (
  <Layout layoutId={9} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem borderColor="orange">
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem backgroundColor="pink">
      <div className="test-item">Item 3</div>
    </LayoutItem>
  </Layout>
);

export const EvenSplitWithGap = (args) => (
  <Layout layoutId={2} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 3</div>
    </LayoutItem>
  </Layout>
);
EvenSplitWithGap.args = {
  gap: 'medium',
};

export const OthersWithGap = (args) => (
  <Layout layoutId={5} {...args}>
    <LayoutItem>
      <div className="test-item">Item 1</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 2</div>
    </LayoutItem>
    <LayoutItem>
      <div className="test-item">Item 3</div>
    </LayoutItem>
  </Layout>
);
OthersWithGap.args = {
  gap: 'large',
};
