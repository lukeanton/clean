import { LayoutIdType } from '../../../types';

export interface LayoutProps {
  /**
   * Type of element to be rendered
   */
  as?: string | React.ComponentType;
  /**
   * The size of gutters for the layout
   */
  gap: 'none' | 'small' | 'medium' | 'large';
  /**
   * Id provided by the Ekardo API to determine which layout to use
   */
  layoutId: LayoutIdType;
}
