import React from 'react';
import { render, screen } from '@testing-library/react';

import { Layout } from './Layout';
import { LayoutProps } from './Layout.types';

describe('Layout Component', () => {
  test('renders Layout component', () => {
    render(
      <Layout layoutId={1} gap="large">
        Hello
      </Layout>
    );

    expect(screen.queryByText('/Hello/')).toBeNull();
  });

  test('renders Layout component 2', () => {
    render(
      <Layout layoutId={1} gap="small">
        Hello
      </Layout>
    );

    expect(screen.queryByText('/Hello 2/')).toBeNull();
  });
});
