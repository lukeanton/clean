import { IBackgroundColor, IBorderColor } from '../../../interfaces';

export interface LayoutItemProps extends IBackgroundColor, IBorderColor {
  /**
   * Type of element to be rendered
   */
  as?: string | React.ComponentType;
}
