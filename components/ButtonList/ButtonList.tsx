import React from 'react';

import cx from 'classnames';

import { Avatar } from '../Avatar';

import { ButtonListProps } from './ButtonList.interfaces';

import './ButtonList.css';

const ButtonList: React.FC<ButtonListProps> = ({
  avatarSize = 'small',
  additionalClassNames,
  countText = '',
  isAvatarVisible = false,
  itemCountThreshold = 5,
  listItems,
  onListItemClick,
}) => {
  return (
    <React.Fragment>
      <div className={cx('c-button-list', additionalClassNames)}>
        {listItems.map(({ id, label }) => (
          <button
            key={id}
            className="c-button-list__item"
            title="Search list item"
            type="button"
            onClick={() => onListItemClick(id, label)}
          >
            <div className="c-button-list__item-content">
              {id !== 0 ? (
                <Avatar
                  additionalClassNames={cx('c-button-list__item-content__avatar', { 'h-hide-visually': !isAvatarVisible })}
                  size={avatarSize}
                  title={label}
                />
              ) : null}
              <span className="c-button-list__item-content__label">{label}</span>
            </div>
          </button>
        ))}
      </div>
      {listItems.length && listItems.length > itemCountThreshold ? (
        <div className="c-button-list__item-count">
          {listItems.length} {countText}
        </div>
      ) : null}
    </React.Fragment>
  );
};

export { ButtonList };
