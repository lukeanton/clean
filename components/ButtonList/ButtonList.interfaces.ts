import { IAdditionalClassNames } from '../../interfaces';

export interface ButtonListProps extends IAdditionalClassNames {
  /*
   * The size of the avatar
   */
  avatarSize?: 'small' | 'medium';
  /*
   * The text inside the count text label which explains the type of list items
   */
  countText?: string;
  /*
   * Boolean value to decide whether to display avatar
   */
  isAvatarVisible?: boolean;
  /*
   * Display item count threshold
   */
  itemCountThreshold?: number;
  /*
   * List items
   */
  listItems: IListItem[];
  /*
   * Provide a function that will be triggered when the list item is clicked
   */
  onListItemClick: (id: number | string, label: string) => void;
}

export interface IListItem {
  /*
   * The id of the list item
   */
  id: number | string;
  /*
   * The label of the list item
   */
  label: string;
}
