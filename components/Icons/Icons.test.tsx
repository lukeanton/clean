import React from 'react';

import { render, screen } from '@testing-library/react';

import { Icons } from './Icons';

describe('Icons', () => {
  it('Should render the icons component in the document', () => {
    render(<Icons />);

    const icons = screen.queryByTestId('qa-icons');

    expect(icons).toBeInTheDocument();
  });
});
