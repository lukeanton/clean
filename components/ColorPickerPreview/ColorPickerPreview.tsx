import React from 'react';

import cx from 'classnames';
import validateColor from 'validate-color';

import { DEFAULT_COLOR } from '../ColorPicker/ColorPicker.constants';

import { ColorPickerPreviewProps } from './ColorPickerPreview.types';
import './ColorPickerPreview.css';

const ColorPickerPreview: React.FC<ColorPickerPreviewProps> = ({ additionalClassNames, selectedColor, onClick }) => (
  <React.Fragment>
    <span className="h-hide-visually">Current color is ${selectedColor}.</span>
    <button
      className={cx('c-color-picker-preview__button', additionalClassNames)}
      style={{ backgroundColor: validateColor(selectedColor) ? selectedColor : DEFAULT_COLOR }}
      type="button"
      onClick={onClick}
    >
      <span className="h-hide-visually">Pick color</span>
    </button>
  </React.Fragment>
);

export { ColorPickerPreview };
