import { IAdditionalClassNames, IOnClickButton } from '../../interfaces';

export interface ColorPickerPreviewProps extends IAdditionalClassNames, IOnClickButton {
  /**
   * The color value selected
   */
  selectedColor: string;
}
