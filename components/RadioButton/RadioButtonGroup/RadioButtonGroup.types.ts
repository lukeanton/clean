import React from 'react';

import { IAdditionalClassNames, IIdentifier, ILabelText, IName } from '../../../interfaces';

export interface IRadioButtonItems extends IIdentifier, ILabelText {
  /**
   * The value of an radio input
   */
  value: string;
}

export interface RadioButtonGroupProps extends IAdditionalClassNames, IName {
  /**
   * The radio button items
   */
  items?: IRadioButtonItems[];
  /**
   * The action to call value change an input
   */
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  /**
   * The value selected among radio inputs
   */
  selectedValue: number | undefined | string;
  /**
   * The title of an radio group
   */
  title?: string;
}
