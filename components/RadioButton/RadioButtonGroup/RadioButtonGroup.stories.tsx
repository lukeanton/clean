import React, { useState } from 'react';

import { Meta, Story } from '@storybook/react';
import { RadioButtonGroup } from './RadioButtonGroup';
import { RadioButtonGroupProps } from './RadioButtonGroup.types';

export default {
  title: 'Components/RadioButtonGroup',
  component: RadioButtonGroup,
} as Meta;

const items = [
  {
    id: 'id_movie_seat_1',
    labelText: 'Isle seat',
    value: 'IS',
  },
  {
    id: 'id_movie_seat_2',
    labelText: 'Box seat',
    value: 'BS',
  },
  {
    id: 'id_movie_seat_3',
    labelText: 'Best Available',
    value: 'BA',
  },
];

const Template: Story<RadioButtonGroupProps> = () => {
  const [selectedValue, setSelectedValue] = useState('IS');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = event;

    setSelectedValue(value);
  };

  return (
    <div style={{ width: '40%' }}>
      <RadioButtonGroup items={items} name="seats" selectedValue={selectedValue} title="Seats" onChange={handleChange} />
    </div>
  );
};

export const Base = Template.bind({});
