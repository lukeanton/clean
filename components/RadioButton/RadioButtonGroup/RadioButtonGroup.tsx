import React from 'react';

import cx from 'classnames';

import { RadioButtonItem } from '../RadioButtonItem/RadioButtonItem';

import { RadioButtonGroupProps } from './RadioButtonGroup.types';

import { Spacing } from '../../Spacing';

import './RadioButtonGroup.css';

const RadioButtonGroup: React.FC<RadioButtonGroupProps> = ({ additionalClassNames, name, items = [], onChange, selectedValue, title }) => {
  const radioButtonGroupClassNames = cx('c-radio-button-group', additionalClassNames);

  return (
    <fieldset className={radioButtonGroupClassNames}>
      <legend className="c-radio-button-group__title">{title}</legend>
      {items.map(({ id, labelText, value }) => {
        const isChecked = value === selectedValue;
        return (
          <Spacing>
            <RadioButtonItem key={id} id={id} isChecked={isChecked} labelText={labelText} name={name} value={value} onChange={onChange} />
          </Spacing>
        );
      })}
    </fieldset>
  );
};

export { RadioButtonGroup };
