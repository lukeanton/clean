import { IAdditionalClassNames, IIdentifier, ILabelText, IName } from '../../../interfaces';

export interface RadioButtonItemProps extends IAdditionalClassNames, IIdentifier, ILabelText, IName {
  /**
   * The boolean value to decide whether the radio button is checked
   */
  isChecked: boolean;
  /**
   * The action to call value change an input
   */
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  /**
   * The value of an radio input
   */
  value: number | string;
}
