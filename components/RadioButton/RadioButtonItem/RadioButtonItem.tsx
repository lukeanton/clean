import React from 'react';

import cx from 'classnames';

import { RadioButtonItemProps } from './RadioButtonItem.types';

import { Label } from '../../Label';

import './RadioButtonItem.css';

const RadioButtonItem: React.FC<RadioButtonItemProps> = ({ additionalClassNames, id, isChecked, labelText, name, onChange, value }) => {
  const radioButtonItemClassNames = cx('c-radio-button', additionalClassNames);

  return (
    <div className={radioButtonItemClassNames}>
      <input checked={isChecked} className="c-radio-button__item" id={id} name={name} type="radio" value={value} onChange={onChange} />
      <Label additionalClassNames="c-radio-button__label" forId={id} labelText={labelText} spacing="none" />
    </div>
  );
};

export { RadioButtonItem };
