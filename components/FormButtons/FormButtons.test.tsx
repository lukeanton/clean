import React from 'react';

import { render, screen } from '@testing-library/react';

import { FormButtons } from './FormButtons';
import { FormButtonsProps } from './FormButtons.types';

describe('FormButtons', () => {
  const handleClose = () => {};
  const handleDelete = () => {};
  const handleSave = () => {};

  const props: FormButtonsProps = {
    additionalClassNames: 'c-form-buttons',
    onClose: handleClose,
    onDelete: handleDelete,
    onSave: handleSave,
  };

  describe('when all handlers are passed in', () => {
    it('should render all buttons on the screen', () => {
      render(<FormButtons onClose={handleClose} onDelete={handleDelete} onSave={handleSave} />);

      const closeButton = screen.queryByText('Close');
      const deleteButton = screen.queryByText('Delete');
      const saveButton = screen.queryByText('Save');

      expect(closeButton).toBeTruthy();
      expect(deleteButton).toBeTruthy();
      expect(saveButton).toBeTruthy();
    });
  });

  describe('when only onClose is passed in', () => {
    it('should only render the close button', () => {
      render(<FormButtons onClose={handleClose} />);

      const closeButton = screen.queryByText('Close');
      const deleteButton = screen.queryByText('Delete');
      const saveButton = screen.queryByText('Save');

      expect(closeButton).toBeTruthy();
      expect(deleteButton).not.toBeTruthy();
      expect(saveButton).not.toBeTruthy();
    });
  });

  describe('when only onDelete is passed in', () => {
    it('should only render the close button', () => {
      render(<FormButtons onDelete={handleDelete} />);

      const closeButton = screen.queryByText('Close');
      const deleteButton = screen.queryByText('Delete');
      const saveButton = screen.queryByText('Save');

      expect(closeButton).not.toBeTruthy();
      expect(deleteButton).toBeTruthy();
      expect(saveButton).not.toBeTruthy();
    });
  });

  describe('when only onSave is passed in', () => {
    it('should only render the close button', () => {
      render(<FormButtons onSave={handleSave} />);

      const closeButton = screen.queryByText('Close');
      const deleteButton = screen.queryByText('Delete');
      const saveButton = screen.queryByText('Save');

      expect(closeButton).not.toBeTruthy();
      expect(deleteButton).not.toBeTruthy();
      expect(saveButton).toBeTruthy();
    });
  });
});
