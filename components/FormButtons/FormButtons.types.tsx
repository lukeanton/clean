import { IAdditionalClassNames } from '../../interfaces';

export interface FormButtonsProps extends IAdditionalClassNames {
  isCloseButtonDisabled?: boolean;
  isDeleteButtonDisabled?: boolean;
  isSaveButtonDisabled?: boolean;
  onClose: () => void;
  onDelete?: () => void;
}
