import React from 'react';

import cx from 'classnames';

import { Button } from '../Buttons';

import { FormButtonsProps } from './FormButtons.types';

import './FormButtons.css';

const FormButtons: React.FC<FormButtonsProps> = ({
  additionalClassNames,
  isCloseButtonDisabled = false,
  isDeleteButtonDisabled = false,
  isSaveButtonDisabled = false,
  onClose,
  onDelete,
}) => {
  return (
    <div className={cx('c-form-buttons', additionalClassNames)} data-testid="qa-crud-buttons">
      <Button isDisabled={isCloseButtonDisabled} text="Close" variant="secondary" onClick={onClose} />
      {onDelete && <Button isDisabled={isDeleteButtonDisabled} text="Delete" variant="danger--tertiary" onClick={onDelete} />}
      <Button isDisabled={isSaveButtonDisabled} text="Save" type="submit" variant="primary" />
    </div>
  );
};

export { FormButtons };
