import React from 'react';

import cx from 'classnames';

import { FooterProps } from './Footer.types';

import './Footer.css';

const Footer: React.FC<FooterProps> = ({
  additionalClassNames,
  date,
  footerEndChildren,
  footerEndLinkItems,
  linkComponent = 'a',
  projectName,
}) => {
  const dateTime = date.toDateString();
  const currentYear = date.getFullYear();

  const Link = linkComponent;

  const footerClassNames = cx('c-footer', additionalClassNames);

  return (
    <footer className={footerClassNames} role="contentinfo">
      <div className="c-footer__start">
        <time dateTime={dateTime}>{currentYear}</time> ©{' '}
        {projectName ? <span className="c-footer__project-name">{projectName}</span> : null}
      </div>

      <nav aria-label="Legal and contact" className="c-footer__end">
        {footerEndLinkItems ? (
          <ul className="c-footer__link-list">
            {footerEndLinkItems.map(({ name, href }) => (
              <li className="c-footer-link__list-item">
                <Link className="c-footer__link-item" href={href}>
                  {name}
                </Link>
              </li>
            ))}
          </ul>
        ) : (
          footerEndChildren
        )}
      </nav>
    </footer>
  );
};

export { Footer };
