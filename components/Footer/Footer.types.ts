import { ElementType, ReactNode } from 'react';

import { IAdditionalClassNames } from '../../interfaces';

export interface IFooterEndItems {
  /**
   * Link to the provided page
   */
  href?: string;
  /**
   * Name to the link displayed
   */
  name?: string;
}

export interface FooterProps extends IAdditionalClassNames {
  /**
   * The date to get the year from
   */
  date: Date;
  /**
   * Children used in the footer end
   */
  footerEndChildren?: ReactNode;
  /**
   * The navigation items displayed on a footer end
   */
  footerEndLinkItems?: IFooterEndItems[];
  /**
   * The custom link component used in the footer
   */
  linkComponent?: ElementType;
  /*
  Name of the project displayed
  */
  projectName?: string;
}
