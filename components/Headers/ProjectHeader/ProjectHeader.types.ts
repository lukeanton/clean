import { AppHeaderProps } from '../AppHeader';

export interface ProjectHeaderProps extends AppHeaderProps {
  /**
   * The text displayed inside avatar
   */
  avatarText?: string;
}
