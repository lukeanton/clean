import React from 'react';

import cx from 'classnames';

import { AppHeader } from '../AppHeader';

import { ProjectHeaderProps } from './ProjectHeader.types';

import { Avatar } from '../../Avatar';

import './ProjectHeader.css';

const ProjectHeader: React.FC<ProjectHeaderProps> = ({ additionalClassNames, children, avatarText, logo }) => {
  const projectHeaderClassNames = cx('c-project-header', additionalClassNames);

  return (
    <AppHeader additionalClassNames={projectHeaderClassNames} logo={logo}>
      {avatarText ? <Avatar title={avatarText} /> : null}
      {children}
    </AppHeader>
  );
};

export { ProjectHeader };
