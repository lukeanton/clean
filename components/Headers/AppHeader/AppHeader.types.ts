import { ElementType } from 'react';

import { BaseHeaderProps } from '../BaseHeader';

import { IconIdType } from '../../../types';

export type AppHeaderLogoHrefType = {
  href?: string;
};

export type AppHeaderLogoType = (
  | {
      iconId: IconIdType;
      imageSrc?: never;
    }
  | {
      iconId?: never;
      imageSrc: string;
    }
) &
  AppHeaderLogoHrefType;

export interface AppHeaderProps extends BaseHeaderProps {
  /**
   * The custom link component used in the app header
   */
  linkComponent?: ElementType;
  logo?: AppHeaderLogoType;
}
