import React from 'react';

import cx from 'classnames';

import { BaseHeader } from '../BaseHeader';

import { AppHeaderLogoType, AppHeaderProps } from './AppHeader.types';

import { Icon } from '../../Icon';

import './AppHeader.css';

const AppHeader: React.FC<AppHeaderProps> = ({ additionalClassNames, children, linkComponent = 'a', logo }) => {
  const Link = linkComponent;

  const { href = '/', iconId = 'id_ekardo_icon', imageSrc } = logo ?? ({} as AppHeaderLogoType);

  const appHeaderClassNames = cx('c-app-header', additionalClassNames);

  return (
    <BaseHeader additionalClassNames={appHeaderClassNames}>
      <div className="c-app-header__start">
        <Link className="c-app-header__link c-app-header__icon-link" href={href}>
          {imageSrc ? (
            <img alt="Project logo" className="c-app-header__image" src={imageSrc} />
          ) : (
            <Icon className="c-app-header__logo-icon" id={iconId} />
          )}
        </Link>
      </div>
      <div className="c-app-header__end">{children}</div>
    </BaseHeader>
  );
};

export { AppHeader };
