import React from 'react';

import cx from 'classnames';

import { BaseHeaderProps } from './BaseHeader.types';

import './BaseHeader.css';

const BaseHeader: React.FC<BaseHeaderProps> = ({ additionalClassNames, children }) => {
  const headerClassNames = cx('c-base-header', additionalClassNames);

  return <header className={headerClassNames}>{children}</header>;
};

export { BaseHeader };
