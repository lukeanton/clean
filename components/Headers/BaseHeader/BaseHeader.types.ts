import { IAdditionalClassNames } from '../../../interfaces';

export interface BaseHeaderProps extends IAdditionalClassNames {}
