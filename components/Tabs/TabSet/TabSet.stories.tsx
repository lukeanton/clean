import React, { useState } from 'react';
import { FormButtons, Icons, Input, ITab, Spacing, TabSet, Textarea, ToggleSwitch } from '../../../components';

const General = ({ onSideBarClose }: { onSideBarClose: () => void }) => {
  return (
    <div>
      <Spacing>
        <Input id="title" labelText="Title" name="title" type="text" onChange={() => null} />
      </Spacing>
      <Spacing>
        <Textarea id="description" labelText="Description" name="description" onChange={() => null} />
      </Spacing>
      <Spacing>
        <Input id="url" labelText="Url" name="url" type="text" onChange={() => null} />
      </Spacing>
      <Spacing>
        <ToggleSwitch id="publish" labelText="Publish Page" isChecked onChange={() => null} />
      </Spacing>
      <div>
        <FormButtons onClose={onSideBarClose} />
      </div>
    </div>
  );
};

const Seo = ({ onSideBarClose }: { onSideBarClose: () => void }) => {
  return (
    <div>
      <Spacing>
        <Input id="seo-title" labelText="Seo Title" name="seoTitle" type="text" onChange={() => null} />
      </Spacing>
      <Spacing>
        <Textarea id="seo-description" labelText="Seo Description" name="seoDescription" onChange={() => null} />
      </Spacing>
      <div>
        <FormButtons onClose={onSideBarClose} onDelete={() => console.log('Delete')} />
      </div>
    </div>
  );
};

const Related = ({ onSideBarClose }: { onSideBarClose: () => void }) => {
  return (
    <div>
      <Spacing>
        <Input id="resources" labelText="Resources" name="resources" type="text" onChange={() => null} />
      </Spacing>
      <div>
        <FormButtons onClose={onSideBarClose} onDelete={() => console.log('Delete')} />
      </div>
    </div>
  );
};

const Navigation = ({ onSideBarClose }: { onSideBarClose: () => void }) => {
  return (
    <div>
      <Spacing>
        <Input id="navigation" labelText="Navigation" name="navigation" type="text" onChange={() => null} />
      </Spacing>
      <div>
        <FormButtons onClose={onSideBarClose} onDelete={() => console.log('Delete')} />
      </div>
    </div>
  );
};

export default {
  title: 'components/TabSet',
  component: TabSet,
};

export const Default = () => {
  const [isSideBarOpen, setIsSideBarOpen] = useState<boolean>(false);

  const handleSideBarClose = () => {
    setIsSideBarOpen((currentValue) => !setIsSideBarOpen);
  };

  const tabs: ITab[] = [
    {
      iconId: 'id_general_tab_icon',
      id: 'general',
      label: 'General',
      view: General,
    },
    {
      iconId: 'id_seo_tab_icon',
      id: 'seo',
      label: 'Seo',
      view: Seo,
    },
    {
      iconId: 'id_related_tab_icon',
      id: 'related',
      label: 'Related',
      view: Related,
    },
    {
      iconId: 'id_navigation_tab_icon',
      id: 'navigation',
      label: 'Navigation',
      view: Navigation,
    },
  ];

  return (
    <React.Fragment>
      <Icons />
      <TabSet defaultActiveTabId={tabs[1].id} tabs={tabs} />
    </React.Fragment>
  );
};
