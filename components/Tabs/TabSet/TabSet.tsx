import React, { KeyboardEvent, useState } from 'react';

import cx from 'classnames';

import { Tab } from '../Tab';

import { EVENT_CODES } from './TabSet.constants';
import { TabSetProps } from './TabSet.types';

import './TabSet.css';

const TabSet = ({ defaultActiveTabId, tabs = [], theme = 'underline' }: TabSetProps) => {
  const totalTabs = tabs.length;

  const [activeTabId, setActiveTabId] = useState<string>(defaultActiveTabId);

  const handleKeyDown = (event: KeyboardEvent<HTMLAnchorElement>) => {
    const { code } = event;

    let nextTabIndex = tabs.findIndex(({ id }) => id === activeTabId);

    if (code === EVENT_CODES.leftArrow) {
      nextTabIndex -= 1;

      if (nextTabIndex < 0) {
        nextTabIndex = 0;
      }
    } else if (code === EVENT_CODES.rightArrow) {
      nextTabIndex += 1;

      if (nextTabIndex >= totalTabs) {
        nextTabIndex = totalTabs - 1;
      }
    }

    setActiveTabId(tabs[nextTabIndex].id);
  };

  const handleSelectTab = (id: string) => {
    setActiveTabId(id);
  };

  return (
    <div className={cx('c-tabset', `c-tabset--${theme}`)}>
      <nav className={cx('c-tabset__nav', `c-tabset__nav--${theme}`)}>
        <ul className={cx('c-tablist__nav', `c-tablist__nav--${theme}`)}>
          {tabs.map(({ id, label, iconId }, index) => (
            <Tab
              key={id}
              activeTabId={activeTabId}
              iconId={iconId}
              id={id}
              index={index}
              labelText={label}
              theme={theme}
              onKeyDown={handleKeyDown}
              onSelectTab={handleSelectTab}
            />
          ))}
        </ul>
      </nav>
      {tabs
        .filter(({ id }) => id === activeTabId)
        .map(({ id, view }) => {
          const TabView = view;

          return (
            <section
              key={`${theme}-tabset-panel-${id}`}
              aria-labelledby={`${theme}-tabset-panel-${id}`}
              className={cx('c-tabset__view', `c-${theme}-tabset__view`)}
              role="tabpanel"
            >
              <TabView />
            </section>
          );
        })}
    </div>
  );
};

export { TabSet };
