// TODO: Move this to the common library
const EVENT_CODES = Object.freeze({
  leftArrow: 'ArrowLeft',
  rightArrow: 'ArrowRight',
});

export { EVENT_CODES };
