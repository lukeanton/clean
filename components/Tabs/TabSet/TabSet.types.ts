import { ElementType } from 'react';

import { IconIdType, TabThemeType } from '../../../types';

export interface ITab {
  /**
   * Specify an iconId if you would like to display an icon above the tab text
   */
  iconId?: IconIdType;
  /**
   * Specify an id for the tab
   */
  id: string;
  /**
   * Specify a meaningful label that describes what is displayed in the tabs view
   */
  label: string;
  /**
   * Provide a view that will be displayed when a tab is active
   */
  view: ElementType;
}

export interface TabSetProps {
  /**
   * Specify the default tab that will be active when the page renders
   */
  defaultActiveTabId: string;
  /**
   * Provide an array of tabs to be displayed
   */
  tabs: ITab[];
  /**
   * Specify how you would like the tabset to appear
   */
  theme?: TabThemeType;
}
