import React, { KeyboardEvent, MouseEvent } from 'react';

import cx from 'classnames';

import { TabProps } from './Tab.types';

import { Icon } from '../../Icon';

import './Tab.css';

const Tab: React.FC<TabProps> = ({ activeTabId, iconId, id, index, labelText, onKeyDown, onSelectTab, theme }) => {
  const handleKeyDown = (event: KeyboardEvent<HTMLAnchorElement>) => {
    onKeyDown(event);
  };

  const handleTabClick = (event: MouseEvent<HTMLAnchorElement>, tabId: string) => {
    event.preventDefault();
    onSelectTab(tabId);
  };

  const isSelected = id === activeTabId;
  const anchorClassNames = cx('c-tab__link', `c-${theme}-tab__link`, {
    [`c-${theme}-tab__link--selected`]: isSelected,
  });

  return (
    <li key={`tab-${id}`} className={cx('c-tab', `c-${theme}-tab`)}>
      <a
        aria-selected={isSelected}
        className={anchorClassNames}
        href={`#${id}`}
        id={`tab-${id}`}
        role="tab"
        tabIndex={index}
        onClick={(event) => handleTabClick(event, id)}
        onKeyDown={handleKeyDown}
      >
        <div className={cx('c-tab__label', `c-${theme}-tab__label`)}>
          {iconId ? <Icon className={cx('c-tab-icon', `c-${theme}__icon`)} id={iconId} /> : null}
          <span data-testid="qa-tab-label">{labelText}</span>
        </div>
      </a>
    </li>
  );
};

export { Tab };
