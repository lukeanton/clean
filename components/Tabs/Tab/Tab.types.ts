import { KeyboardEvent } from 'react';

import { IconIdType, TabThemeType } from '../../../types';

export interface TabProps {
  /**
   * Specify the tab that is currently active
   */
  activeTabId: string;
  /**
   * Specify an optional iconId that will be displayed above the tabLabel
   */
  iconId?: IconIdType;
  /**
   * Specify an id that will uniquely identify the tab
   */
  id: string;
  /**
   * Specify the tab index used to manage keyboard focus
   */
  index: number;
  /**
   * Specify the text that will describe the tab.
   */
  labelText: string;
  /**
   * Provide a function that will be triggered when the configured key is pressed
   */
  onKeyDown: (event: KeyboardEvent<HTMLAnchorElement>) => void;
  /**
   * Provide a function that will be triggered when the tab is selected
   */
  onSelectTab: (id: string) => void;
  /**
   * Specify the theme that will alter the style of tabs you would like to display
   */
  theme?: TabThemeType;
}
