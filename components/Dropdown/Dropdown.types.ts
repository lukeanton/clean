import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../interfaces';

export interface DropdownProps extends IAdditionalClassNames {
  /**
   * The text of the default button
   */
  buttonText?: string;
  /**
   * The unique id of the dropdown
   */
  dropdownId: string;
  /**
   * Boolean value to decide whether the trigger button is disabled
   */
  isDisabled?: boolean;
  /*
   * Boolean value to decide whether the dropdown content is visible
   */
  isDisplayContent: boolean;
  /*
   * Provide a function that will be set display information to the state
   */
  onDisplayContent: (isDisplay: boolean) => void;
  /**
   * The children used inside dropdown
   */
  trigger?: ReactNode;
}
