import React from 'react';

import cx from 'classnames';

import { DropdownProps } from './Dropdown.types';

import './Dropdown.css';

const Dropdown: React.FC<DropdownProps> = ({
  additionalClassNames,
  buttonText,
  children,
  dropdownId,
  isDisplayContent,
  isDisabled = false,
  onDisplayContent,
  trigger,
}) => {
  return (
    <div className={cx('c-dropdown', additionalClassNames)}>
      <button
        aria-controls={dropdownId}
        aria-expanded={isDisplayContent}
        aria-haspopup="true"
        className={cx('c-dropdown-toggle', {
          'c-dropdown-toggle--disabled': isDisabled,
        })}
        disabled={isDisabled}
        type="button"
        onClick={() => onDisplayContent(!isDisplayContent)}
      >
        {trigger ?? buttonText}
      </button>
      <div aria-hidden={!isDisplayContent} id={dropdownId}>
        {isDisplayContent ? children : null}
      </div>
    </div>
  );
};

export { Dropdown };
