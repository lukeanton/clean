import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { NavigationButton } from './NavigationButton';
import { IconIdType, NavigationButtonDirectionType, RotationCssSuffixType } from '../../../types';

describe('NavigationButton', () => {
  const direction: NavigationButtonDirectionType = 'next';
  const iconId: IconIdType = 'id_caret_icon';
  const navigationButtonTestId = `qa-navigation-button-${direction}`;
  const text = 'navigation button text';

  let onClickSpy: () => void;

  beforeEach(() => {
    onClickSpy = jest.fn();
  });

  it('should display the NavigationButton in the document', () => {
    render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

    const navigationButton = screen.queryByTestId(navigationButtonTestId);

    expect(navigationButton).toBeInTheDocument();
  });

  it('should display the c-navigation-button css class', () => {
    render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

    const navigationButton = screen.queryByTestId(navigationButtonTestId);

    expect(navigationButton).toHaveClass('c-navigation-button');
  });

  describe('Icon component', () => {
    it('should display the Icon in the document', () => {
      render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

      const icon = screen.queryByTestId('qa-icon');

      expect(icon).toBeInTheDocument();
    });

    describe('rotationCssSuffix prop', () => {
      const baseIconClassNames = `c-navigation-button__icon c-navigation-button__icon--${direction} `;

      describe('when the rotationCssSuffix is set', () => {
        const rotationCssSuffix: RotationCssSuffixType = '90';

        it('should have the correct class names', () => {
          const iconClassNames = `${baseIconClassNames} c-navigation-button__icon--${rotationCssSuffix}`;

          render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} rotationCssSuffix="90" />);

          const icon = screen.getByTestId('qa-icon');

          expect(icon).toHaveClass(iconClassNames);
        });
      });

      describe('when the rotationCssSuffix is not set', () => {
        it('should have the correct class names', () => {
          render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

          const icon = screen.getByTestId('qa-icon');

          expect(icon).toHaveClass(baseIconClassNames);
        });
      });
    });
  });

  describe('additionalClassNames prop', () => {
    describe('when the additionalClassNames prop is set', () => {
      it('should have the correct class names', () => {
        const additionalClassNames = 'test-class';

        render(<NavigationButton iconId={iconId} additionalClassNames={additionalClassNames} direction={direction} onClick={onClickSpy} />);

        const navigationButton = screen.getByTestId(navigationButtonTestId);

        expect(navigationButton).toHaveClass(additionalClassNames);
      });
    });
  });

  describe('isDisabled prop', () => {
    describe('when the isDisabled prop is true', () => {
      it('should disable the navigation button', () => {
        render(<NavigationButton isDisabled iconId={iconId} direction={direction} onClick={onClickSpy} />);

        const navigationButton = screen.getByTestId(navigationButtonTestId);

        expect(navigationButton).toBeDisabled();
      });
    });

    describe('when the isDisabled prop is false', () => {
      it('should not disable the navigation button', () => {
        render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

        const navigationButton = screen.getByTestId(navigationButtonTestId);

        expect(navigationButton).not.toBeDisabled();
      });
    });
  });

  describe('text prop', () => {
    describe('when the text is set', () => {
      it('should display the text in the document', () => {
        render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} text={text} />);

        const buttonText = screen.queryByText(text);

        expect(buttonText).toBeInTheDocument();
      });
    });

    describe('when the text is not set', () => {
      it('should not display the text in the document', () => {
        render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} />);

        const buttonText = screen.queryByText(text);

        expect(buttonText).not.toBeInTheDocument();
      });
    });
  });

  describe('onClick prop', () => {
    describe('when the navigation button is clicked', () => {
      it('should call the onClick function one time', () => {
        render(<NavigationButton iconId={iconId} direction={direction} onClick={onClickSpy} text={text} />);

        const navigationButton = screen.getByTestId(navigationButtonTestId);

        fireEvent.click(navigationButton);

        expect(onClickSpy).toBeCalledTimes(1);
      });
    });
  });
});
