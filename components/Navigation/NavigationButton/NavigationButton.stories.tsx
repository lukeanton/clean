import React from 'react';
import { Meta, Story } from '@storybook/react';
import { NavigationButton } from './NavigationButton';
import { action } from '@storybook/addon-actions';
import { Icons } from '../../Icons';
import { NavigationButtonProps } from './NavigationButton.types';
import { IconIdType } from '../../../types';

const iconId: IconIdType = 'id_enter_icon';

export default {
  title: 'Components/Navigation/NavigationButton',
  component: NavigationButton,
  argTypes: {
    iconId: {
      defaultValue: iconId,
    },
  },
} as Meta;

const Template: Story<NavigationButtonProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <NavigationButton {...args} />
    </React.Fragment>
  );
};

export const PreviousArrow = Template.bind({});
PreviousArrow.args = {
  onClick: action('Previous button clicked'),
  direction: 'previous',
  rotationCssSuffix: '180',
};

export const NextArrow = Template.bind({});
NextArrow.args = {
  onClick: action('Next button clicked'),
  direction: 'next',
};

export const PreviousEnter = Template.bind({});
PreviousEnter.args = {
  onClick: action('Previous button clicked'),
  direction: 'previous',
  text: 'Previous page',
  rotationCssSuffix: '180',
};

export const NextEnter = Template.bind({});
NextEnter.args = {
  onClick: action('Next button clicked'),
  direction: 'next',
  text: 'Next page',
};
