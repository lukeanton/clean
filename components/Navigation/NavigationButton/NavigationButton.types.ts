import { IAdditionalClassNames, IIconId, IIsDisabled, IOnClickButton } from '../../../interfaces';
import { NavigationButtonDirectionType, RotationCssSuffixType } from '../../../types';

export interface NavigationButtonProps extends IAdditionalClassNames, IIconId, IIsDisabled, IOnClickButton {
  /**
   * The direction of the navigation
   */
  direction: NavigationButtonDirectionType;
  /**
   * The rotation angle of the icon
   */
  rotationCssSuffix?: RotationCssSuffixType;
  /**
   * The text of the nav button
   */
  text?: string;
}
