import React from 'react';

import cx from 'classnames';

import { NavigationButtonProps } from './NavigationButton.types';

import { Icon } from '../../Icon';

import './NavigationButton.css';

const NavigationButton: React.FC<NavigationButtonProps> = ({
  additionalClassNames = '',
  direction,
  iconId,
  isDisabled = false,
  onClick,
  rotationCssSuffix,
  text,
}) => {
  const buttonClassNames = cx('c-navigation-button', `c-navigation-button--${direction}`, additionalClassNames);

  const iconClassNames = cx('c-navigation-button__icon', `c-navigation-button__icon--${direction}`, {
    [`c-navigation-button__icon--${rotationCssSuffix}`]: Boolean(rotationCssSuffix),
  });

  return (
    <button
      className={buttonClassNames}
      data-testid={`qa-navigation-button-${direction}`}
      disabled={isDisabled}
      type="button"
      onClick={onClick}
    >
      <Icon className={iconClassNames} id={iconId} />
      {Boolean(text) && <span className={`c-navigation-button__text c-navigation-button__text--${direction}`}>{text}</span>}
    </button>
  );
};

export { NavigationButton };
