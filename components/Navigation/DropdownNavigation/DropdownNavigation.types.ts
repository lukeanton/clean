import { ElementType } from 'react';

import { IAdditionalClassNames, ISupportiveText } from '../../../interfaces';

export interface DropdownNavigationProps extends IAdditionalClassNames, ISupportiveText {
  /**
   * The custom link component used in dropdown navigation
   */
  linkComponent?: ElementType;
  /**
   * The navigation items displayed
   */
  listItems?: IDropdownNavigationItems[];
}

export interface IDropdownNavigationItems {
  /**
   * Link to the provided page
   */
  href: string;
  /**
   * Name to the link displayed
   */
  name: string;
}
