import React from 'react';

import cx from 'classnames';

import { DropdownNavigationProps } from './DropdownNavigation.types';

import './DropdownNavigation.css';

const DropdownNavigation: React.FC<DropdownNavigationProps> = ({
  additionalClassNames,
  children,
  supportiveText,
  linkComponent = 'a',
  listItems = [],
}) => {
  const dropdownNavigationClassNames = cx('c-dropdown-navigation', additionalClassNames);

  const Link = linkComponent;

  return (
    <nav aria-label={supportiveText} className={dropdownNavigationClassNames}>
      {children}
      {listItems.length ? (
        <ul className="c-dropdown-navigation__list">
          {listItems.map(({ name, href }) => (
            <li key={name} className="c-dropdown-navigation__list-item">
              <Link className="c-dropdown-navigation__list-item-link" href={href}>
                {name}
              </Link>
            </li>
          ))}
        </ul>
      ) : null}
    </nav>
  );
};

export { DropdownNavigation };
