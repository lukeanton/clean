import React from 'react';
import { Meta, Story } from '@storybook/react';
import { PageNavigation } from './PageNavigation';
import { Icons } from '../../Icons';
import { PageNavigationProps } from './PageNavigation.types';
import { IconIdType } from '../../../types';

const iconId: IconIdType = 'id_enter_icon';

const handleClick = () => {
  console.log('click');
};

const nextIcon = {
  iconId: iconId,
  isDisabled: false,
  onClick: handleClick,
};

const previousIcon = {
  iconId: iconId,
  isDisabled: false,
  onClick: handleClick,
};

export default {
  title: 'Components/Navigation/PageNavigation',
  component: PageNavigation,
} as Meta;

const Template: Story<PageNavigationProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <PageNavigation {...args} />
    </React.Fragment>
  );
};

export const NextAndPreviousActive = Template.bind({});
NextAndPreviousActive.args = {
  nextIcon: nextIcon,
  previousIcon: previousIcon,
};

export const NavigationStart = Template.bind({});
NavigationStart.args = {
  nextIcon: nextIcon,
  previousIcon: { ...previousIcon, isDisabled: true },
};

export const NavigationEnd = Template.bind({});
NavigationEnd.args = {
  nextIcon: { ...nextIcon, isDisabled: true },
  previousIcon: previousIcon,
};
