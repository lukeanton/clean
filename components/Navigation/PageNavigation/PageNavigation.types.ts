import { IAdditionalClassNames, IIconId, IIsDisabled, IOnClickButton } from '../../../interfaces';

export interface IIcon extends IIconId, IIsDisabled, IOnClickButton {}

export interface PageNavigationProps extends IAdditionalClassNames {
  /**
   * The next icon to be mapped to the SVG icon
   */
  nextIcon: IIcon;
  /**
   * The previous icon to be mapped to the SVG icon
   */
  previousIcon: IIcon;
}
