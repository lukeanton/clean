import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { PageNavigation } from './PageNavigation';
import { IIcon } from './PageNavigation.types';

describe('PageNavigation', () => {
  const nextIcon: IIcon = {
    iconId: 'id_caret_icon',
    isDisabled: false,
    onClick: () => {},
  };
  const previousIcon: IIcon = {
    iconId: 'id_caret_icon',
    isDisabled: false,
    onClick: () => {},
  };

  it('should render the page navigation in the document', () => {
    render(<PageNavigation nextIcon={nextIcon} previousIcon={previousIcon} />);

    const pageNavigation = screen.getByLabelText('previous and next page navigation');

    expect(pageNavigation).toBeInTheDocument();
  });

  describe('additionalClassNames prop', () => {
    describe('when the additionalClassNames prop is set', () => {
      it('should have the test-class css class', () => {
        const additionalClassNames = 'test-class';

        render(<PageNavigation additionalClassNames={additionalClassNames} nextIcon={nextIcon} previousIcon={previousIcon} />);

        const pageNavigation = screen.getByLabelText('previous and next page navigation');

        expect(pageNavigation).toHaveClass(additionalClassNames);
      });
    });
  });

  describe('nextIcon prop', () => {
    it('should render the next navigation button in the document', () => {
      render(<PageNavigation nextIcon={nextIcon} previousIcon={previousIcon} />);

      const nextNavigationButton = screen.getByText('Next page');

      expect(nextNavigationButton).toBeInTheDocument();
    });
  });

  describe('previousIcon prop', () => {
    it('should render the previous navigation button in the document', () => {
      render(<PageNavigation nextIcon={nextIcon} previousIcon={previousIcon} />);

      const previousNavigationButton = screen.getByText('Previous page');

      expect(previousNavigationButton).toBeInTheDocument();
    });
  });
});
