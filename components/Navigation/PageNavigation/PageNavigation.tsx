import React from 'react';

import cx from 'classnames';

import { NavigationButton } from '../NavigationButton';

import { PageNavigationProps } from './PageNavigation.types';
import './PageNavigation.css';

const PageNavigation: React.FC<PageNavigationProps> = ({ additionalClassNames = '', nextIcon, previousIcon }) => {
  const { iconId: nextIconId, isDisabled: isNextIconDisabled, onClick: onNextIconClick } = nextIcon;
  const { iconId: previousIconId, isDisabled: isPreviousIconDisabled, onClick: onPreviousIconClick } = previousIcon;

  const navigationClassNames = cx('c-navigation-container', additionalClassNames);

  return (
    <nav aria-label="previous and next page navigation" className={navigationClassNames}>
      <NavigationButton
        direction="previous"
        iconId={previousIconId}
        isDisabled={isPreviousIconDisabled}
        rotationCssSuffix="180"
        text="Previous page"
        onClick={onPreviousIconClick}
      />
      <NavigationButton
        direction="next"
        iconId={nextIconId}
        isDisabled={isNextIconDisabled}
        rotationCssSuffix="0"
        text="Next page"
        onClick={onNextIconClick}
      />
    </nav>
  );
};

export { PageNavigation };
