import React from 'react';

import cx from 'classnames';

import { DropdownNavigation } from '../DropdownNavigation';

import { OrganisationNavigationProps } from './OrganisationNavigation.types';

import { Icon } from '../../Icon';

import './OrganisationNavigation.css';

const OrganisationNavigation: React.FC<OrganisationNavigationProps> = ({
  additionalClassNames,
  linkComponent = 'a',
  organisation,
  smallText,
}) => {
  const { logoIconId = 'id_group_icon', url, name, dropdownNavigationItems } = organisation;

  const Link = linkComponent;

  return (
    <DropdownNavigation
      additionalClassNames={cx('c-organisation-navigation', additionalClassNames)}
      listItems={dropdownNavigationItems}
      supportiveText="Organisation navigation"
    >
      <div className="c-organisation-navigation__title">
        <span className="c-organisation-navigation__small-text">{smallText}</span>
        <Link className="c-organisation-navigation__link" href={url}>
          {logoIconId ? <Icon className="c-organisation-navigation__icon" id={logoIconId} /> : null}
          {name}
        </Link>
      </div>
    </DropdownNavigation>
  );
};

export { OrganisationNavigation };
