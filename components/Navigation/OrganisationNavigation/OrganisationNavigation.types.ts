import { ElementType } from 'react';

import { IDropdownNavigationItems } from '../DropdownNavigation/DropdownNavigation.types';

import { IAdditionalClassNames } from '../../../interfaces';
import { IconIdType } from '../../../types';

export interface OrganisationNavigationProps extends IAdditionalClassNames {
  /**
   * The element used as a link
   */
  linkComponent?: ElementType;
  /**
   * The organisation information
   */
  organisation: IOrganisation;
  /**
   * Small text displayed above the organisation name
   */
  smallText?: string;
}

export interface IOrganisation {
  /**
   * The items listed in the navigation
   */
  dropdownNavigationItems: IDropdownNavigationItems[];
  /**
   * The organisation icon
   */
  logoIconId?: IconIdType;
  /**
   * The items listed in the navigation
   */
  name: string;
  /**
   * The organisation url
   */
  url: string;
}
