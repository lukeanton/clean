import React from 'react';

import cx from 'classnames';

import { NavButtonProps } from './NavButton.types';

import { Icon } from '../../../Icon';

import './NavButton.css';

const NavButton: React.FC<NavButtonProps> = ({ ariaControls = 'main-nav-dropdown', iconId, isMenuVisible = false, onToggleMenu }) => {
  const defaultIcon = isMenuVisible ? 'id_close_nav_menu_icon' : 'id_nav_menu_icon';

  const navButtonClassNames = cx('c-nav-button', { 'c-nav-button--is-active': isMenuVisible });

  return (
    <button
      aria-controls={ariaControls}
      aria-expanded={!isMenuVisible}
      className={navButtonClassNames}
      data-testid="qa-nav-button"
      type="button"
      onClick={onToggleMenu}
    >
      <Icon className="c-nav-button__icon" id={iconId || defaultIcon} />
      <span className="h-hide-visually">Menu</span>
    </button>
  );
};

export { NavButton };
