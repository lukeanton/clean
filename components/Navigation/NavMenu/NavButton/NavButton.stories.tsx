import React from 'react';

import { Meta, Story } from '@storybook/react';
import { NavButton } from './NavButton';
import { action } from '@storybook/addon-actions';
import { NavButtonProps } from './NavButton.types';
import { Icons } from '../../../Icons';

export default {
  title: 'Components/Navigation/NavButton',
  component: NavButton,
} as Meta;

const Template: Story<NavButtonProps> = (args: any) => {
  return (
    <React.Fragment>
      <Icons />
      <NavButton {...args} />
    </React.Fragment>
  );
};

export const Default = Template.bind({});
Default.args = {
  iconId: undefined,
  isMenuVisible: true,
  onToggleMenu: action('handleOpenMenu =>'),
};

export const Close = Template.bind({});
Close.args = {
  iconId: 'id_caret_icon',
  isMenuVisible: true,
  onToggleMenu: action('handleCloseMenu =>'),
};

export const CustomIcon = Template.bind({});
CustomIcon.args = {
  iconId: 'id_enter_icon',
  isMenuVisible: true,
  onToggleMenu: action('handleToggle =>'),
};
