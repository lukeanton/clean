import React from 'react';

import { render, screen } from '@testing-library/react';
import { NavButton } from './NavButton';

describe('NavButton', () => {
  const handleToggleMenu = () => {};

  describe('when the icon renders without errors', () => {
    test('the nav button will display on the screen', () => {
      render(<NavButton iconId="id_enter_icon" isMenuVisible={false} onToggleMenu={handleToggleMenu} />);
      const navButton = screen.queryByTestId('qa-nav-button');

      expect(navButton).toBeInTheDocument();
      expect(navButton).toHaveAttribute('aria-expanded', 'true');
    });
  });

  describe('when the nav button is active', () => {
    test('the nav button will be active', () => {
      render(<NavButton iconId="id_enter_icon" isMenuVisible onToggleMenu={handleToggleMenu} />);

      const navButton = screen.queryByTestId('qa-nav-button');
      expect(navButton).toHaveAttribute('aria-expanded', 'false');
    });
  });
});
