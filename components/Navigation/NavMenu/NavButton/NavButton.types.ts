import { IIconId } from '../../../../interfaces';

export interface NavButtonProps extends IIconId {
  /**
   * The aria controls accessibility selection for the component
   */
  ariaControls?: string;
  /**
   * The boolean whether navigation menu is displayed
   */
  isMenuVisible: boolean;
  /**
   * The function to toggle the navigation
   */
  onToggleMenu: () => void;
}
