import React from 'react';

import cx from 'classnames';

import { NavButton } from '../NavButton';

import { NavMenuProps } from './NavMenu.types';

import { Icon } from '../../../Icon';

import './NavMenu.css';

const NavMenu: React.FC<NavMenuProps> = ({ iconId, isMenuVisible = false, linkComponent = 'a', navItems, onToggleMenu }) => {
  const navMenuClassNames = cx('c-nav-menu', { 'c-nav-menu--is-hidden': !isMenuVisible });

  const Link = linkComponent;

  return (
    <nav aria-label="main" data-testid="qa-nav-menu" id="main_navigation">
      <NavButton iconId={iconId} isMenuVisible={isMenuVisible} onToggleMenu={onToggleMenu} />

      <ul className={navMenuClassNames} data-testid="qa-nav-menu-list">
        {navItems.map(({ iconId: menuIconId, id, label }) => {
          const href = `#${id}`;

          return (
            <li key={id} className="c-nav-menu__item">
              <Link className="c-nav-menu__link" href={href} id={`${id}-nav-item`}>
                <span className="c-nav-menu__text" data-testid="qa-nav-text">
                  {menuIconId ? <Icon className="c-nav-menu__icon" id={menuIconId} /> : null}
                  {label}
                </span>
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export { NavMenu };
