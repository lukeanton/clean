import React from 'react';

import { render, screen } from '@testing-library/react';
import { NavMenu } from './NavMenu';
import { INavItem } from './NavMenu.types';

describe('NavMenu', () => {
  const navigationItems: INavItem[] = [{ iconId: 'id_arrow_icon', id: 'home', label: 'Home' }];

  const handleToggleMenu = () => {};

  const { label } = navigationItems[0];

  describe('when the nav menu renders without errors', () => {
    describe('when the nav menu is active', () => {
      test('the nav menu will be displayed on the screen', () => {
        render(
          <NavMenu iconId="id_arrow_icon" isMenuVisible linkComponent={null} navItems={navigationItems} onToggleMenu={handleToggleMenu} />
        );
        const navMenu = screen.queryByTestId('qa-nav-menu');
        const navMenuList = screen.queryByTestId('qa-nav-menu-list');
        const navText = screen.queryByTestId('qa-nav-text');

        expect(navMenu).toBeInTheDocument();
        expect(navText).toHaveTextContent(label);
        expect(navMenuList).not.toHaveClass('c-nav-menu--is-hidden');
      });
    });
  });

  describe('when the nav menu is not active', () => {
    test('the nav menu will no be displayed', () => {
      render(
        <NavMenu
          iconId="id_arrow_icon"
          isMenuVisible={false}
          linkComponent={null}
          navItems={navigationItems}
          onToggleMenu={handleToggleMenu}
        />
      );

      const navMenuList = screen.queryByTestId('qa-nav-menu-list');
      expect(navMenuList).toHaveClass('c-nav-menu--is-hidden');
    });
  });
});
