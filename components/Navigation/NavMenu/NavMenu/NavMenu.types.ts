import { ElementType } from 'react';

import { PartialPick } from '@netfront/common-library';

import { NavButtonProps } from '../NavButton';

import { IIconId, IIdentifier } from '../../../../interfaces';

export interface INavItem extends PartialPick<IIconId, 'iconId'>, IIdentifier {
  /**
   * The label used in a navigation menu asset
   */
  label: string;
}

export interface NavMenuProps extends NavButtonProps {
  /**
   * The link used in a navigation menu asset
   */
  linkComponent?: ElementType;
  /**
   * The items display in a navigation menu asset
   */
  navItems: INavItem[];
}
