import React, { useState } from 'react';

import { Meta, Story } from '@storybook/react';
import { NavMenu } from './NavMenu';
import { NavMenuProps } from './NavMenu.types';
import { Icons } from '../../../Icons';

const navItems = [
  { iconId: 'id_home_icon', id: 'home', label: 'Home' },
  { iconId: 'id_about_icon', id: 'about', label: 'About' },
  { iconId: 'id_contact_icon', id: 'contact', label: 'Contact' },
];

export default {
  title: 'Components/Navigation/NavMenu',
  component: NavMenu,
} as Meta;

const Template: Story<NavMenuProps> = (args: any) => {
  const [isMenuVisible, setIsMenuVisible] = useState(false);

  const handleToggleMenu = () => {
    setIsMenuVisible(!isMenuVisible);
  };

  return (
    <React.Fragment>
      <Icons />
      <NavMenu isMenuVisible={isMenuVisible} onToggleMenu={handleToggleMenu} {...args} />
    </React.Fragment>
  );
};

export const Default = Template.bind({});

Default.args = { navItems: navItems };
