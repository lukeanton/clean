import React from 'react';

import cx from 'classnames';

import { DropdownNavigation } from '../DropdownNavigation';

import { UserNavigationProps } from './UserNavigation.types';

import './UserNavigation.css';

const UserNavigation: React.FC<UserNavigationProps> = ({ additionalClassNames, userNavigationListItems }) => {
  const userNavigationClassNames = cx('c-user-navigation', additionalClassNames);

  return (
    <DropdownNavigation
      additionalClassNames={userNavigationClassNames}
      listItems={userNavigationListItems}
      supportiveText="User navigation"
    />
  );
};

export { UserNavigation };
