import { IDropdownNavigationItems } from '../DropdownNavigation/DropdownNavigation.types';

import { IAdditionalClassNames } from '../../../interfaces';

export interface UserNavigationProps extends IAdditionalClassNames {
  /**
   * The items listed in the navigation
   */
  userNavigationListItems: IDropdownNavigationItems[];
}
