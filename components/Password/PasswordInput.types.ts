import {
  IAdditionalClassNames,
  IErrorMessage,
  IIdentifier,
  IIsDisabled,
  ILabelText,
  IIsLabelHidden,
  IIsReadOnly,
  IIsRequired,
  IName,
  IPlaceholder,
} from '../../interfaces';

export interface PasswordInputProps
  extends IAdditionalClassNames,
    IErrorMessage,
    IIdentifier,
    IIsDisabled,
    ILabelText,
    IIsLabelHidden,
    IIsReadOnly,
    IIsRequired,
    IPlaceholder,
    IName {
  /**
   * The boolean value to decide the password value is the current user password
   */
  isCurrent?: boolean;
  /**
   * The action to call value change the password
   */
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
