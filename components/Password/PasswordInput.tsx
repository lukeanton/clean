import React, { useState } from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';
import { Input } from '../Input';

import { PasswordInputProps } from './PasswordInput.types';
import './PasswordInput.css';

const PasswordInput: React.FC<PasswordInputProps> = ({
  additionalClassNames,
  errorMessage,
  id = 'password',
  labelText = 'Password',
  isCurrent,
  isDisabled = false,
  isLabelHidden = false,
  isReadOnly = false,
  isRequired = false,
  name = 'password',
  placeholder,
  onChange,
  ...rest
}) => {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  return (
    <div className={cx('c-password', additionalClassNames)}>
      <Input
        autoComplete={isCurrent ? 'current-password' : 'new-password'}
        errorMessage={errorMessage}
        id={id}
        isDisabled={isDisabled}
        isLabelHidden={isLabelHidden}
        isReadOnly={isReadOnly}
        isRequired={isRequired}
        labelText={labelText}
        name={name}
        placeholder={placeholder}
        type={isPasswordVisible ? 'text' : 'password'}
        onChange={onChange}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...rest}
      />
      <button className="c-password__button" type="button" onClick={() => setIsPasswordVisible(!isPasswordVisible)}>
        <span className="h-hide-visually">Toggle password visibility</span>
        <Icon className="c-password__icon" id={isPasswordVisible ? 'id_eye_hide_icon' : 'id_eye_show_icon'} />
      </button>
    </div>
  );
};

export { PasswordInput };
