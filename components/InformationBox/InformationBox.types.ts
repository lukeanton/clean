import { ElementType } from 'react';

import { PartialPick } from '@netfront/common-library';

import { IAdditionalClassNames, IIconId } from '../../interfaces';

export interface InformationBoxProps extends IAdditionalClassNames, PartialPick<IIconId, 'iconId'> {
  /**
   * The heading tag as a message
   */
  headingTagComponent?: ElementType;
  /**
   * The message displayed in the box
   */
  message?: string;
}
