import React from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';

import './InformationBox.css';

import { InformationBoxProps } from './InformationBox.types';

const InformationBox: React.FC<InformationBoxProps> = ({
  additionalClassNames,
  children,
  iconId = 'id_info_icon',
  headingTagComponent = 'h2',
  message = '',
}) => {
  const informationBoxClassNames = cx('c-information-box', additionalClassNames);

  const Heading = headingTagComponent;

  const informationBoxIconClassName = cx('c-information-box__icon', {
    'c-information-box__default-icon': iconId === 'id_information_box_icon',
  });

  return (
    <div className={informationBoxClassNames}>
      <Icon className={informationBoxIconClassName} id={iconId} />
      {message ? <Heading className="c-information-box__message">{message}</Heading> : children}
    </div>
  );
};

export { InformationBox };
