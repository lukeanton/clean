import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Spinner } from './Spinner';

import { SpinnerProps } from './Spinner.interfaces';

import { Icons } from '../Icons';

export default {
  title: 'Components/Spinner',
  component: Spinner,
} as Meta;

const Template: Story<SpinnerProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <Spinner {...args} />
    </React.Fragment>
  );
};

export const Base = Template.bind({});
Base.args = {
  isLoading: true,
  spinnerIconSize: 'default',
};

export const Small = Template.bind({});
Small.args = {
  isLoading: true,
  spinnerIconSize: 'small',
};

export const Medium = Template.bind({});
Medium.args = {
  isLoading: true,
  spinnerIconSize: 'medium',
};

export const Large = Template.bind({});
Large.args = {
  isLoading: true,
  spinnerIconSize: 'large',
};
