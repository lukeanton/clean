import { ICheckboxItem } from './Checkbox/Checkbox.types';

import { IErrorMessage, IIsDisabled, IIsLabelHidden, IIsRequired, IName } from '../../interfaces';

export interface ICheckboxGroup extends IErrorMessage, IIsDisabled, IIsLabelHidden, IIsRequired, IName {
  /**
   * The radio options
   */
  items?: ICheckboxItem[];
  /**
   * Legend text for the group
   */
  legendText: string;
  /**
   * The function to call when a radio is interacted with
   */
  onChange: (values: string[]) => void;
  /**
   * The selected items
   */
  values?: string[];
}
