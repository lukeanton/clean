import React, { SyntheticEvent, useState } from 'react';
import { Story, Meta } from '@storybook/react';
import { ButtonIconOnly } from '../ButtonIconOnly';
import { CheckboxGroup } from '../CheckboxGroup';
import { Icons } from '../Icons';
import { ICheckboxGroup } from './CheckboxGroup.types';

export default {
  title: 'Components/CheckboxGroup',
  component: CheckboxGroup,
  argTypes: {
    isDirty: { control: 'none' },
    name: { control: 'none' },
    type: { control: 'none' },
    onChange: { control: 'none' },
  },
} as Meta;

const Template: Story<ICheckboxGroup> = (args) => (
  <React.Fragment>
    <Icons />
    <CheckboxGroup
      labelText="Fruits"
      items={[
        { labelText: 'Apples', id: 'apple', value: 'apple' },
        { labelText: 'Oranges', id: 'orange', value: 'orange' },
        { labelText: 'Bananas', id: 'banana', value: 'banana' },
      ]}
      name="base"
      values={['apple', 'orange']}
      {...args}
    />
  </React.Fragment>
);

export const Base = () => {
  const [values, setValues] = useState<string[]>(['apple']);

  const handleChange = (values: string[]) => {
    setValues(values);
  };

  const handleDelete = (id: string) => {
    console.log({ id });
  };

  return (
    <React.Fragment>
      <Icons />
      <CheckboxGroup
        labelText="Fruits"
        items={[
          { labelText: 'Apples', id: 'apple', value: 'apple' },
          { labelText: 'Oranges', id: 'orange', value: 'orange' },
          {
            labelText: 'Bananas',
            id: 'banana',
            value: 'banana',
            deleteButton: (
              <ButtonIconOnly
                additionalClassNames="c-user-generated-response__discard-button"
                iconId="id_bin_icon"
                text="Discard"
                onClick={() => handleDelete('banana')}
              />
            ),
          },
        ]}
        name="base"
        values={values}
        onChange={handleChange}
      />
    </React.Fragment>
  );
};

export const HasError = Template.bind({});

HasError.args = {
  errorMessage: 'I am some error',
  labelText: 'Error Example',
  name: 'err',
};

export const HiddenLabel = Template.bind({});

HiddenLabel.args = {
  isLabelHidden: true,
  labelText: 'Hidden Label example',
  name: 'hidden',
};

export const Required = Template.bind({});

Required.args = {
  isRequired: true,
  labelText: 'Required example',
  name: 'req',
};
