import { ReactNode } from 'react';

import { IAdditionalClassNames, IIdentifier, IIsDisabled, IIsLabelHidden, ILabelText, IName } from '../../../interfaces';

export interface ICheckboxItem extends IIdentifier, ILabelText {
  /**
   * Delete button used to delete user generated responses
   */
  deleteButton?: ReactNode;
  /**
   * Image attached
   */
  imageSrc?: string;
  /**
   * Unique value of the item
   */
  value: string;
}

export interface CheckboxProps extends IAdditionalClassNames, ICheckboxItem, IIsDisabled, IIsLabelHidden, IName {
  /**
   * The class name given to the label
   */
  checkboxLabelClassName?: string;
  /**
   * If the item has been selected
   */
  isChecked?: boolean;
  /**
   * The function to call when a checkbox is interacted with
   */
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
