import React from 'react';

import cx from 'classnames';

import { CheckboxProps } from './Checkbox.types';

import { Icon } from '../../Icon';
import { Label } from '../../Label';

import './Checkbox.css';

const Checkbox: React.FC<CheckboxProps> = ({
  additionalClassNames,
  checkboxLabelClassName,
  deleteButton,
  id,
  imageSrc,
  isChecked = false,
  isDisabled = false,
  isLabelHidden = false,
  labelText,
  name,
  onChange,
  value,
}) => (
  <div className="c-checkbox">
    <input
      checked={isChecked}
      className={cx('c-checkbox__input', 'h-hide-visually', additionalClassNames)}
      disabled={isDisabled}
      id={id}
      name={name}
      type="checkbox"
      value={value}
      onChange={onChange}
    />
    {imageSrc && <img alt="" className="c-checkbox__image" src={imageSrc} />}
    <Label
      additionalClassNames={cx('c-checkbox__label', checkboxLabelClassName)}
      forId={id}
      isHidden={isLabelHidden}
      labelText={labelText}
      spacing="none"
    >
      <span aria-hidden="true" className="c-checkbox__icon-container">
        <Icon className="c-checkbox__icon" id="id_tick_icon" />
      </span>
    </Label>
    {deleteButton}
  </div>
);

export { Checkbox };
