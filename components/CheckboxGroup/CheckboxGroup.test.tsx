import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { CheckboxGroup } from './CheckboxGroup';

describe('CheckboxGroup', () => {
  const labelText = 'Test label';
  const name = 'test-name';

  let onChangeSpy: () => void;

  beforeEach(() => {
    onChangeSpy = jest.fn();
  });

  describe('errorMessage prop', () => {
    const errorMessageTestId = `id_${name}_error`;

    describe('when the error message prop is set', () => {
      const errorMessageText = 'error message';

      it('should display the error message in the document when queried by test id', () => {
        render(<CheckboxGroup errorMessage={errorMessageText} labelText={labelText} name={name} onChange={onChangeSpy} />);

        const errorMessage = screen.queryByTestId(errorMessageTestId);
        const checkboxGroupFieldset = screen.queryByTestId('qa-checkbox-group-fieldset');

        expect(errorMessage).toBeInTheDocument();
        expect(checkboxGroupFieldset).toHaveAttribute('aria-describedby', errorMessageTestId);
      });

      it('should display the error message in the document when queried by text', () => {
        render(<CheckboxGroup errorMessage={errorMessageText} labelText={labelText} name={name} onChange={onChangeSpy} />);

        const errorMessage = screen.getByText(errorMessageText);

        expect(errorMessage).toBeInTheDocument();
      });
    });

    describe('when the error message prop is not set', () => {
      it('should not display the error message in the document when queried by test id', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const errorMessage = screen.queryByTestId(`id_${name}_error`);
        const checkboxGroupFieldset = screen.queryByTestId('qa-checkbox-group-fieldset');

        expect(errorMessage).not.toBeInTheDocument();
        expect(checkboxGroupFieldset).not.toHaveAttribute('aria-describedby');
      });

      it('should not display the error message in the document when queried by text', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxGroupFieldset = screen.queryByTestId('qa-checkbox-group-fieldset');
        const errorMessage = screen.queryByTestId(`id_${name}_error`);

        expect(checkboxGroupFieldset).not.toHaveAttribute('aria-describedby');
        expect(errorMessage).not.toBeInTheDocument();
      });
    });
  });

  describe('isLabelHidden prop', () => {
    describe('when the isLabelHidden prop is true', () => {
      it('should have the h-hide-visually css class', () => {
        render(<CheckboxGroup isLabelHidden labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxGroupLabel = screen.queryByTestId('qa-label');

        expect(checkboxGroupLabel).toHaveClass('h-hide-visually');
      });
    });

    describe('when the isLabelHidden prop is false', () => {
      it('should not have the h-hide-visually css class', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxGroupLabel = screen.queryByTestId('qa-label');

        expect(checkboxGroupLabel).not.toHaveClass('h-hide-visually');
      });
    });
  });

  describe('isRequired prop', () => {
    describe('when the isRequired prop is true', () => {
      it('should display the required message text in the document', () => {
        render(<CheckboxGroup isRequired labelText={labelText} name={name} onChange={onChangeSpy} />);

        const requiredStar = screen.getByText('*');

        expect(requiredStar).toBeInTheDocument();
      });
    });

    describe('when the required prop is false', () => {
      it('should not display the required message text in the document', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const requiredStar = screen.queryByText('*');

        expect(requiredStar).not.toBeInTheDocument();
      });
    });
  });

  describe('items prop', () => {
    const items = [
      {
        id: 'item-id-1',
        imageSrc: '/img-1',
        labelText: 'item-label-1',
        value: 'item-value-1',
      },
      {
        id: 'item-id-2',
        imageSrc: '/img-2',
        labelText: 'item-label-2',
        value: 'item-value-2',
      },
    ];

    describe('when the items prop is set', () => {
      it('should display the correct number of checkboxes', () => {
        render(<CheckboxGroup items={items} labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxes = screen.queryAllByTestId('qa-checkbox');

        expect(checkboxes).toHaveLength(items.length);
      });
    });

    describe('when the items prop is not set', () => {
      it('should display the correct number of checkboxes', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxes = screen.queryAllByTestId('qa-checkbox');

        expect(checkboxes).toHaveLength(0);
      });
    });

    describe('isDisabled prop', () => {
      describe('when the isDisabled prop is true', () => {
        it('should disable all of the checkboxes', () => {
          render(<CheckboxGroup isDisabled labelText={labelText} name={name} onChange={onChangeSpy} />);

          const checkboxes = screen.queryAllByTestId('qa-checkbox');

          checkboxes.forEach((checkbox) => {
            expect(checkbox).toBeDisabled();
          });
        });
      });

      describe('when the isDisabled prop is false', () => {
        it('should not disable any of the checkboxes', () => {
          render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

          const checkboxes = screen.queryAllByTestId('qa-checkbox');

          checkboxes.forEach((checkbox) => {
            expect(checkbox).not.toBeDisabled();
          });
        });
      });
    });

    describe('onChange prop', () => {
      it('should call onChange with the correct argument', () => {
        render(<CheckboxGroup items={items} labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxes = screen.queryAllByTestId('qa-checkbox');
        const firstCheckbox = checkboxes[0];

        fireEvent.click(firstCheckbox);

        expect(onChangeSpy).toBeCalledWith([firstCheckbox.getAttribute('value')]);
      });
    });

    describe('values prop', () => {
      describe('when the values prop is set', () => {
        it('should check all of the checkboxes', () => {
          const values = items.map(({ id }) => id);

          render(<CheckboxGroup items={items} labelText={labelText} name={name} values={values} onChange={onChangeSpy} />);

          const checkboxes = screen.queryAllByTestId('qa-checkbox');

          checkboxes.forEach((checkbox) => {
            expect(checkbox).toBeChecked();
          });
        });
      });

      describe('when the values prop is not set', () => {
        it('should not check all of the checkboxes', () => {
          render(<CheckboxGroup items={items} labelText={labelText} name={name} onChange={onChangeSpy} />);

          const checkboxes = screen.queryAllByTestId('qa-checkbox');

          checkboxes.forEach((checkbox) => {
            expect(checkbox).not.toBeChecked();
          });
        });
      });
    });
  });

  describe('labelText prop', () => {
    describe('when the label text is set', () => {
      it('should display the correct label text in the document', () => {
        render(<CheckboxGroup labelText={labelText} name={name} onChange={onChangeSpy} />);

        const checkboxGroupLabel = screen.getByText(labelText);

        expect(checkboxGroupLabel).toBeInTheDocument();
      });
    });
  });
});
