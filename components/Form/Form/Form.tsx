import React from 'react';

import { FormProps } from './Form.types';

const Form: React.FC<FormProps> = ({ children, ...props }) => (
  <form
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...props}
  >
    {children}
  </form>
);

export { Form };
