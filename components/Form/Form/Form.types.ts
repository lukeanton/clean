import { ReactNode } from 'react';

export interface FormProps extends React.HTMLProps<HTMLFormElement> {
  /**
   * Content that lives inside the item - typically a type of form field
   */
  children: ReactNode;
}
