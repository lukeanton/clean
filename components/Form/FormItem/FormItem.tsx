import React from 'react';

import { FormItemProps } from './FormItem.types';

import './FormItem.css';

const FormItem: React.FC<FormItemProps> = ({ children }) => <div className="c-form-item">{children}</div>;

export { FormItem };
