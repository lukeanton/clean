import { IAdditionalClassNames } from '../../../interfaces';
import { AssetType } from '../../../types';

export interface AssetIconCardProps extends IAdditionalClassNames {
  /**
   * Specify the asset type
   */
  type: AssetType;
}
