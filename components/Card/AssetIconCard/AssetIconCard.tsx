import React from 'react';

import cx from 'classnames';

import { Card } from '../Card';

import { ASSET_TYPE_ICON_MAP } from './AssetIconCard.constants';
import { AssetIconCardProps } from './AssetIconCard.types';

import { IconIdType } from '../../../types';
import { Icon } from '../../Icon';

import './AssetIconCard.css';

const AssetIconCard: React.FC<AssetIconCardProps> = ({ additionalClassNames, type }) => {
  const iconId: IconIdType = ASSET_TYPE_ICON_MAP[type];

  return (
    <Card additionalClassNames={cx('c-asset-icon-card', additionalClassNames)}>
      <div className="c-asset-icon-card__container">
        <Icon className="c-asset-icon-card__icon" id={iconId} />
      </div>
    </Card>
  );
};

export { AssetIconCard };
