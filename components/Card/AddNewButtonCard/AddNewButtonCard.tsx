import React from 'react';

import { ButtonCard } from '../ButtonCard';

import { AddNewButtonCardProps } from './AddNewButtonCard.interfaces';

import { CoverButton } from '../../Buttons';

const AddNewButtonCard = ({ onClick }: AddNewButtonCardProps) => {
  return (
    <ButtonCard coverButton={<CoverButton supportiveText="Add new" onClick={onClick} />} iconId="id_add_circle_icon" title="Add new" />
  );
};

export { AddNewButtonCard };
