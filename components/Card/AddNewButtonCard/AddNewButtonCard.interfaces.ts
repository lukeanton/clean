export interface AddNewButtonCardProps {
  onClick: () => void;
}
