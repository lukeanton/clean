import React from 'react';

import cx from 'classnames';

import { Card } from '../Card';

import { ImageCardProps } from './ImageCard.types';

import './ImageCard.css';

const ImageCard: React.FC<ImageCardProps> = ({ additionalClassNames, image: { altText, src }, settingsButton, supportiveText, title }) => {
  const imageCardClassNames = cx('c-image-card', additionalClassNames);

  return (
    <React.Fragment>
      <Card additionalClassNames={imageCardClassNames} settingsButton={settingsButton} title={title}>
        <div className="c-image-card__body">
          <img alt={altText} src={src} />
        </div>
        <span className="h-hide-visually">{supportiveText}</span>
      </Card>
    </React.Fragment>
  );
};

export { ImageCard };
