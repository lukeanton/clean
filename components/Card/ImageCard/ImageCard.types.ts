import { CardProps } from '../Card';

import { IImage, ISupportiveText } from '../../../interfaces';

export interface ImageCardProps extends Omit<CardProps, 'children'>, ISupportiveText {
  /**
   * Image source
   */
  image: IImage;
}
