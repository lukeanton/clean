import React from 'react';
import { Meta, Story } from '@storybook/react';
import { LinkCard } from './LinkCard';
import { LinkCardProps } from './LinkCard.types';
import { CoverLink } from '../../CoverLink';
import avatarImage from '../../../helpers/storybook/images/avatar_sample.png';

import { Icons } from '../../Icons';

export default {
  title: 'Components/LinkCard',

  component: LinkCard,
} as Meta;

const Template: Story<LinkCardProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <LinkCard {...args} />
    </React.Fragment>
  );
};

export const Base = Template.bind({});
Base.args = {
  coverLink: <CoverLink href="https://www.netfront.com.au" supportiveText="Go to profile with avatar" />,
};

export const withAvatar = Template.bind({});
withAvatar.args = {
  avatarText: 'Burak Seyhan',
  coverLink: <CoverLink href="https://www.ebay.com.au" supportiveText="Go to profile with avatar" />,
  avatarImageSrc: avatarImage,
};

export const withAvatarText = Template.bind({});
withAvatarText.args = {
  avatarText: 'Burak Seyhan',
  coverLink: <CoverLink href="https://www.google.com.au" supportiveText="Go to profile with avatar text" />,
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  coverLink: <CoverLink href="https://www.amazon.com.au" supportiveText="Go to profile" />,
  iconId: 'id_enter_icon',
};

export const WithText = Template.bind({});
WithText.args = {
  coverLink: <CoverLink href="https://www.google.com.au" supportiveText="Go to profile with text" />,
  title: 'Go to Profile',
};

export const WithAvatarAndText = Template.bind({});
WithAvatarAndText.args = {
  avatarText: 'Burak Seyhan',
  avatarImageSrc: avatarImage,
  coverLink: <CoverLink href="https://www.google.com.au" supportiveText="Go to profile with avatar and text" />,
  title: 'Go to Profile',
};

export const WithIconAndText = Template.bind({});
WithIconAndText.args = {
  iconId: 'id_enter_icon',
  title: 'Go to Profile',
  coverLink: <CoverLink href="https://www.google.com.au" supportiveText="Go to profile with icon and text" />,
};
