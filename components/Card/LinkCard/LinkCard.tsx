import React from 'react';

import cx from 'classnames';

import { Card } from '../Card';

import { LinkCardProps } from './LinkCard.types';

import { Avatar } from '../../Avatar';
import { Icon } from '../../Icon';

import './LinkCard.css';

const LinkCard: React.FC<LinkCardProps> = ({ additionalClassNames, avatarImageSrc, avatarText, coverLink, iconId, title }) => {
  const linkCardIconClassNames = cx('c-link-card__icon', {
    'c-link-card__icon--has-no-title': !title,
    'c-link-card__icon--has-title': title,
  });

  const avatarImage = avatarImageSrc ? <img alt={avatarText} src={avatarImageSrc} /> : undefined;

  return (
    <Card additionalClassNames={cx('c-link-card', additionalClassNames)}>
      <React.Fragment>
        {avatarText ? <Avatar image={avatarImage} title={avatarText} /> : null}
        {iconId ? <Icon className={linkCardIconClassNames} id={iconId} /> : null}
        {title ? <span className="c-link-card__text">{title}</span> : null}
      </React.Fragment>
      {coverLink}
    </Card>
  );
};

export { LinkCard };
