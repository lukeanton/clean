import { PartialPick } from '@netfront/common-library';

import { IAdditionalClassNames, ICoverLink, IIconId } from '../../../interfaces';

export interface LinkCardProps extends IAdditionalClassNames, ICoverLink, PartialPick<IIconId, 'iconId'> {
  /*
   * The image path set as avatar
   */
  avatarImageSrc?: string;
  /**
   * The text displayed inside avatar
   */
  avatarText?: string;
  /**
   * The text on the button card
   */
  title?: string;
}
