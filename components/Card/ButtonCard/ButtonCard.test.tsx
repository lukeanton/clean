import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { ButtonCard } from './ButtonCard';
import { ButtonCardProps } from './ButtonCard.types';

describe('ButtonCard', () => {
  const handleClick = () => {};

  const props: ButtonCardProps = {
    title: '',
    supportiveText: '',
    onClick: handleClick,
  };

  it('should display the ButtonCard item', () => {
    render(<ButtonCard supportiveText="test text" onClick={handleClick} />);
    const buttonCard = screen.queryByTestId('qa-card');

    expect(buttonCard).toBeInTheDocument();
    expect(buttonCard).toHaveClass('c-button-card');
  });

  describe('When title added to ButtonCard component', () => {
    const title = 'Add New';
    it('should display the title inside ButtonCard', () => {
      render(<ButtonCard supportiveText="test text" onClick={handleClick} title={title} />);
      const buttonCard = screen.queryByText(title);

      expect(buttonCard).toBeInTheDocument();
      expect(buttonCard).toHaveClass('c-button-card__text');
    });

    it('should display the ButtonCard icon with text item', () => {
      render(<ButtonCard supportiveText="test text" iconId="id_caret_icon" onClick={handleClick} title={title} />);
      const buttonIcon = screen.queryByTestId('qa-icon');

      expect(buttonIcon).toBeInTheDocument();
      expect(buttonIcon).toHaveClass('c-button-card__icon--has-title');
    });
  });

  describe('When card button clicked', () => {
    it('should display the sidebar', () => {
      let isDisplaySidebar = false;

      const onClick = () => {
        isDisplaySidebar = true;
      };

      props.onClick = onClick;

      render(<ButtonCard supportiveText="test text" onClick={onClick} />);

      const buttonCard = screen.getByRole('button');

      expect(isDisplaySidebar).toBeFalsy();
      fireEvent.click(buttonCard);
      expect(isDisplaySidebar).toBeTruthy();
    });
  });

  describe('When there is a supportive text', () => {
    it('should include the supportive text', () => {
      const supportiveText = 'Go to Google';

      render(<ButtonCard supportiveText={supportiveText} onClick={handleClick} />);

      const buttonCard = screen.getByText(supportiveText);
      expect(buttonCard).toBeInTheDocument();
    });
  });
});
