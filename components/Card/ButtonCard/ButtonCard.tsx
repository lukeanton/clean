import React from 'react';

import cx from 'classnames';

import { Card } from '../Card';

import { ButtonCardProps } from './ButtonCard.types';

import { Icon } from '../../Icon';

import './ButtonCard.css';

const ButtonCard: React.FC<ButtonCardProps> = ({ additionalClassNames, coverButton, iconId, title }) => {
  const buttonCardIconClassNames = cx('c-button-card__icon', {
    'c-button-card__icon--has-no-title': !title,
    'c-button-card__icon--has-title': title,
  });

  return (
    <Card additionalClassNames={cx('c-button-card', additionalClassNames)}>
      {iconId ? <Icon className={buttonCardIconClassNames} id={iconId} /> : null}
      {title ? <span className="c-button-card__text">{title}</span> : null}
      {coverButton}
    </Card>
  );
};

export { ButtonCard };
