import { ReactNode } from 'react';

import { PartialPick } from '@netfront/common-library';

import { CardProps } from '../Card';

import { IAdditionalClassNames, IIconId } from '../../../interfaces';

export interface ButtonCardProps extends Pick<CardProps, 'title'>, PartialPick<IIconId, 'iconId'>, IAdditionalClassNames {
  /**
   * The cover button
   */
  coverButton: ReactNode;
}
