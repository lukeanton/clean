import React from 'react';
import { Meta, Story } from '@storybook/react';
import { ButtonCard } from './ButtonCard';
import { ButtonCardProps } from './ButtonCard.types';
import { action } from '@storybook/addon-actions';
import { CoverButton } from '../../Buttons/CoverButton';

import { Icons } from '../../Icons/Icons';

export default {
  title: 'Components/ButtonCard',
  component: ButtonCard,
} as Meta;

const Template: Story<ButtonCardProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <ButtonCard {...args} />
    </React.Fragment>
  );
};

export const Base = Template.bind({});
Base.args = {
  coverButton: <CoverButton onClick={action('Button card clicked')} supportiveText="Add new project" />,
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  iconId: 'id_add_circle_icon',
  coverButton: <CoverButton onClick={action('Button card with icon clicked')} supportiveText="Add new project" />,
};

export const WithText = Template.bind({});
WithText.args = {
  coverButton: <CoverButton onClick={action('Button card with text clicked')} supportiveText="Add new project" />,
  title: 'Add New Project',
};

export const WithIconAndText = Template.bind({});
WithIconAndText.args = {
  coverButton: <CoverButton onClick={action('Button card with icon and text clicked')} supportiveText="Add new project" />,
  iconId: 'id_add_circle_icon',
  onClick: action('Button card with icon and text clicked'),
  title: 'Add New',
};
