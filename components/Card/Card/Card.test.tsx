import React from 'react';
import { render, screen } from '@testing-library/react';
import { Card } from './Card';

describe('Card', () => {
  it('should display the Card', () => {
    render(<Card children="test children" />);
    const card = screen.queryByTestId('qa-card');

    expect(card).toBeInTheDocument();
    expect(card).toHaveClass('c-card');
  });

  describe('When class assigned to Card component', () => {
    it('should have the class assigned to asset', () => {
      const additionalClassNames = 'project-card';

      render(<Card children="test children" additionalClassNames={additionalClassNames} />);
      const card = screen.queryByTestId('qa-card');
      expect(card).toHaveClass('project-card');
    });
  });

  describe('When Card has a children', () => {
    it('should display the children', () => {
      const children = 'Test Children';

      render(<Card children={children} />);

      const card = screen.getByText(children);
      expect(card).toBeInTheDocument();
    });
  });
});
