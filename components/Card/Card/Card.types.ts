import { ReactNode } from 'react';

import { IAdditionalClassNames, ITitle } from '../../../interfaces';

export interface CardProps extends IAdditionalClassNames, ITitle {
  /**
   * The settings button
   */
  settingsButton?: ReactNode;
}
