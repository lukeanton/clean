import React from 'react';

import cx from 'classnames';

import { CardProps } from './Card.types';
import './Card.css';

const Card: React.FC<CardProps> = ({ additionalClassNames, children, settingsButton, title }) => {
  const cardClassNames = cx('c-card', additionalClassNames);

  const cardInnerClassNames = cx({
    'c-card__header': title,
    'c-card__header--is-title-only': !settingsButton,
    'c-card__header--has-button': Boolean(settingsButton),
  });

  return (
    <article className="c-card__container" data-testid="qa-card">
      <div className={cardClassNames}>
        <div className={cardInnerClassNames}>
          <span className="c-card__title">{title}</span>
          {settingsButton}
        </div>
        {children}
      </div>
    </article>
  );
};

export { Card };
