import React from 'react';
import { Meta, Story } from '@storybook/react';
import { CardProps } from './Card.types';
import { Card } from './Card';

export default {
  title: 'Components/Card',
  component: Card,
} as Meta;

const Template: Story<CardProps> = (args) => {
  return <Card {...args} />;
};

export const Base = Template.bind({});
Base.args = {
  children: '',
};

export const WithChildren = Template.bind({});
WithChildren.args = {
  children: 'Children will be here',
};
