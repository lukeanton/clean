import React from 'react';

import cx from 'classnames';

import { Card } from '../Card';

import { NavigationCardProps } from './NavigationCard.types';

import { Avatar } from '../../Avatar';
import { Icon } from '../../Icon';

import './NavigationCard.css';

const NavigationCard: React.FC<NavigationCardProps> = ({
  additionalClassNames,
  avatarImageSrc,
  coverLink,
  iconId,
  settingsButton,
  title,
}) => {
  const navigationCardClassNames = cx('c-navigation-card', additionalClassNames);

  const avatarImage = avatarImageSrc ? <img alt={title} src={avatarImageSrc} /> : null;

  return (
    <React.Fragment>
      <Card additionalClassNames={navigationCardClassNames} settingsButton={settingsButton} title={title}>
        <div className="c-navigation-card__body">
          {iconId ? <Icon className="c-navigation-card__icon" id={iconId} /> : <Avatar image={avatarImage} title={title} />}
        </div>
        {coverLink}
      </Card>
    </React.Fragment>
  );
};

export { NavigationCard };
