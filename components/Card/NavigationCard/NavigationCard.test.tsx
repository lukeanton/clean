import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { NavigationCard } from './NavigationCard';

describe('NavigationCard', () => {
  it('it should display the NavigationCard item', () => {
    render(<NavigationCard supportiveText="test text" href="test url" title="test title" />);
    const navigationCard = screen.queryByTestId('qa-card');

    expect(navigationCard).toBeInTheDocument();
    expect(navigationCard).toHaveClass('c-navigation-card');
  });

  describe('When class assigned to NavigationCard component', () => {
    it('it should have the class assigned to asset', () => {
      const additionalClassName = 'project-navigation-card';

      render(<NavigationCard additionalClassNames={additionalClassName} supportiveText="test text" href="test url" title="test title" />);
      const navigationCard = screen.queryByTestId('qa-card');
      expect(navigationCard).toHaveClass('project-navigation-card');
    });
  });

  describe('When there is a title', () => {
    it('it should display the title', () => {
      const title = 'Netfront Test Title';

      render(<NavigationCard supportiveText="test text" href="test url" title={title} />);

      const navigationCard = screen.getByText(title);
      expect(navigationCard).toBeInTheDocument();
    });
  });

  describe('When there is a supportive text', () => {
    it('it should include the supportive text', () => {
      const supportiveText = 'Go to Google';

      render(<NavigationCard supportiveText={supportiveText} href="test url" title="test title" />);

      const navigationCard = screen.getByText(supportiveText);
      expect(navigationCard).toBeInTheDocument();
    });
  });

  describe('When there is an href', () => {
    it('it should have the link href', () => {
      const href = 'https://www.google.com.au';

      render(<NavigationCard supportiveText="test text" href={href} title="test title" />);

      const navigationCard = screen.getByRole('link');
      expect(navigationCard).toHaveAttribute('href', href);
    });
  });

  describe('When settings button is not displayed', () => {
    it('it should not display the settings button', () => {
      render(<NavigationCard supportiveText="test text" href="test url" title="test title" />);

      const settingsButton = screen.queryByRole('button');

      expect(settingsButton).toBeNull();
    });
  });

  describe('When settings button is displayed', () => {
    it('it should display the settings button', () => {
      render(<NavigationCard supportiveText="test text" href="test url" title="test title" />);

      const settingsButton = screen.getByRole('button');

      expect(settingsButton).toBeInTheDocument();
    });
  });

  describe('When settings button clicked', () => {
    it('it should display the sidebar', () => {
      let isDisplaySidebar = false;

      const onClick = () => {
        isDisplaySidebar = true;
      };

      render(<NavigationCard supportiveText="test text" href="test url" title="test title" onSettingsButtonClick={onClick} />);

      const settingsButton = screen.getByRole('button');

      expect(isDisplaySidebar).toBeFalsy();
      fireEvent.click(settingsButton);
      expect(isDisplaySidebar).toBeTruthy();
    });
  });
});
