import { ReactNode } from 'react';

import { PartialPick } from '@netfront/common-library';

import { CardProps } from '../Card';

import { IIconId } from '../../../interfaces';

export interface NavigationCardProps extends Omit<CardProps, 'children'>, PartialPick<IIconId, 'iconId'> {
  /*
   * The image source for the avatar
   */
  avatarImageSrc?: string;
  /**
   * The cover link
   */
  coverLink: ReactNode;
  /*
   * The title for the avatar
   */
  title?: string;
}
