import React from 'react';

import cx from 'classnames';

import { PagePreviewCardProps } from './PagePreviewCard.types';

import './PagePreviewCard.css';

const PagePreviewCard: React.FC<PagePreviewCardProps> = ({
  additionalClassNames,
  coverLink,
  image: { altText, src },
  isHero = false,
  isPageTitle = false,
  subTitle,
  title,
}) => {
  const pagePreviewCardClassNames = cx('c-page-preview-card', additionalClassNames);

  const containerClassNames = cx('c-page-preview-card__container', {
    'c-page-preview-card--is-hero': isHero,
  });
  const TitleTag = isPageTitle ? 'h1' : 'span';

  return (
    <article className={containerClassNames}>
      <div className={pagePreviewCardClassNames}>
        <div className="c-page-preview-card__image-container">
          <img alt={altText} className="c-page-preview-card__image" src={src} />
        </div>
        <span className="c-page-preview-card__sub-title">{subTitle}</span>
        <TitleTag className="c-page-preview-card__title">{title}</TitleTag>
        {coverLink}
      </div>
    </article>
  );
};

export { PagePreviewCard };
