import { IAdditionalClassNames, ICoverLink, IImage, ISubTitle, ITitle } from '../../../interfaces';

export interface PagePreviewCardProps extends IAdditionalClassNames, ICoverLink, ISubTitle, ITitle {
  /**
   * Specify an image tag and alt text to display an image
   */
  image: IImage;
  /**
   * The boolean value that decides wether the card displayed as a hero
   */
  isHero?: boolean;
  /**
   * The boolean value that decides wether the title is being used as the page title
   */
  isPageTitle?: boolean;
}
