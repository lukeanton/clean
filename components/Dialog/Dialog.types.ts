import Modal from 'react-modal';

import { IAdditionalClassNames } from '../../interfaces';
import { AppRootIdType, DialogType } from '../../types';

export interface DialogProps extends IAdditionalClassNames, Modal.Props {
  /**
   * Define the apps root id
   */
  appRootId?: AppRootIdType;
  /**
   * Specify the text that will be displayed within dialogs confirm button
   */
  confirmText?: string;
  /**
   * The boolean value that decides whether the modal is open
   */
  isOpen: boolean;
  /**
   * Provide a function that will be triggered when the cancel button is clicked
   */
  onCancel?: () => void;
  /**
   * Provide a function that will be triggered when the close button is clicked
   */
  onClose: () => void;
  /**
   * Provide a function that will be triggered when the confirm button is clicked
   */
  onConfirm?: () => void;
  /*
   * Specify a meaningful title that will be displayed as the dialogs heading
   */
  title: string;
  /*
   * Specify the type of dialog you would like to display
   */
  type?: DialogType;
}
