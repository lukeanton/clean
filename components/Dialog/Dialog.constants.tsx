import { DialogType, IconIdType } from '../../types';

export const ICON_ID_TYPES: Record<DialogType, IconIdType> = Object.freeze({
  error: 'id_error_icon',
  info: 'id_info_icon',
  success: 'id_success_icon',
  warning: 'id_warning_icon',
});
