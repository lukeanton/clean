import React, { useEffect, useState, useRef } from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';

import { BackTopProps } from './BackTop.types';

import './BackTop.css';

const BackTop: React.FC<BackTopProps> = ({ additionalClassNames, iconId = 'id_arrow_icon', text }) => {
  const containerRef = useRef<HTMLDivElement>(null);

  const [isIntersecting, setIsIntersecting] = useState(true);

  const scrollToTop = (event: React.UIEvent<HTMLElement>) => {
    event.preventDefault();

    window.scrollTo({
      behavior: 'smooth',
      top: 0,
    });
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const createObserver = (callback: any) => {
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          callback(entry.isIntersecting);
        });
      },
      {
        root: null,
        rootMargin: '0px',
        threshold: 0,
      }
    );
    return observer;
  };

  useEffect(() => {
    if (!containerRef.current) {
      return undefined;
    }

    const container = containerRef.current;
    const containerObserver = createObserver(setIsIntersecting);

    containerObserver.observe(container);

    return () => {
      containerObserver.unobserve(container);
    };
  }, [containerRef]);

  const backTopClassNames = cx('c-back-top-container', additionalClassNames, {
    'c-back-to-top-container--is-intersecting': !isIntersecting,
  });

  return (
    <div ref={containerRef} className={backTopClassNames} data-testid="qa-back-to-top-button">
      <a className="c-back-to-top__button" href="#top" onClick={scrollToTop}>
        {text ? (
          <span className="c-back-to-top__text">
            <span className="h-hide-visually">Back to </span> {text}
          </span>
        ) : null}
        <Icon className="c-back-to-top__icon" id={iconId} />
      </a>
    </div>
  );
};

export { BackTop };
