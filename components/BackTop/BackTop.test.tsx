import React from 'react';

import { render, screen } from '@testing-library/react';

import 'intersection-observer';

import { BackTop } from './BackTop';

describe('BackTop', () => {
  it('has the Back to top button in the dom', () => {
    render(<BackTop />);

    const backTop = screen.queryByTestId('qa-back-to-top-button');
    expect(backTop).toBeInTheDocument();
  });

  describe('When back to top button has text', () => {
    it('should display the text', () => {
      render(<BackTop text="top" />);

      const backTop = screen.queryByTestId('qa-back-to-top-button');
      expect(backTop).toHaveTextContent('top');
    });
  });

  describe('When back to top button has additionalClassName', () => {
    it('should have the additionalClassName', () => {
      render(<BackTop additionalClassNames="test-class" />);

      const backTop = screen.queryByTestId('qa-back-to-top-button');
      expect(backTop).toHaveClass('test-class');
    });
  });

  describe('when scroll is intersecting with the back to top container', () => {
    it('should not display the BackTop item', () => {
      render(<BackTop />);

      const backTop = screen.queryByTestId('qa-back-to-top-button');
      expect(backTop).not.toHaveClass('c-back-to-top-container--is-intersecting');
    });
  });

  // TODO: Add the is not intersecting scenario
});
