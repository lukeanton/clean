import { PartialPick } from '@netfront/common-library';

import { IAdditionalClassNames, IIconId } from '../../interfaces';

export interface BackTopProps extends IAdditionalClassNames, PartialPick<IIconId, 'iconId'> {
  /**
   * The text used in the button
   */
  text?: string;
}
