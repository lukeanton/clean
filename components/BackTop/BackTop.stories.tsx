import React from 'react';
import { Meta } from '@storybook/react';
import { BackTop } from './BackTop';

import { Icons } from '../Icons';

export default {
  title: 'Components/BackTop',
  component: BackTop,
} as Meta;

const Template = (args) => {
  return (
    <React.Fragment>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra
        aliquet eget sit amet tellus cras adipiscing enim. Adipiscing tristique risus nec feugiat in fermentum. Scelerisque in dictum non
        consectetur a erat nam at. Duis tristique sollicitudin nibh sit amet commodo nulla. Proin libero nunc consequat interdum varius sit
        amet. In fermentum posuere urna nec tincidunt praesent semper. Volutpat sed cras ornare arcu dui. Odio euismod lacinia at quis risus
        sed vulputate odio. Duis ultricies lacus sed turpis tincidunt id. Tristique risus nec feugiat in. Faucibus interdum posuere lorem
        ipsum dolor sit amet consectetur.
      </p>
      <p>
        Iaculis eu non diam phasellus vestibulum lorem sed risus. Quis imperdiet massa tincidunt nunc. Tellus elementum sagittis vitae et
        leo. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Eget sit amet tellus cras. Integer feugiat scelerisque varius
        morbi enim nunc faucibus. Posuere lorem ipsum dolor sit amet consectetur adipiscing. Venenatis urna cursus eget nunc scelerisque
        viverra mauris. Luctus venenatis lectus magna fringilla urna porttitor. At auctor urna nunc id cursus metus aliquam. In vitae turpis
        massa sed elementum tempus. Enim eu turpis egestas pretium aenean pharetra.
      </p>
      <p>
        Commodo odio aenean sed adipiscing diam. Sit amet mauris commodo quis imperdiet massa tincidunt. Blandit libero volutpat sed cras
        ornare arcu dui. Arcu felis bibendum ut tristique et egestas quis ipsum suspendisse. Fermentum et sollicitudin ac orci phasellus
        egestas tellus rutrum. Arcu non sodales neque sodales. Nam at lectus urna duis. Nisl nisi scelerisque eu ultrices vitae auctor eu
        augue ut. Egestas congue quisque egestas diam in arcu. Phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor.
        Praesent elementum facilisis leo vel fringilla. Tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce.
      </p>
      <BackTop {...args} />
      <Icons />
    </React.Fragment>
  );
};

export const Base = Template.bind({});
Base.args = {};

export const TextAndIcon = Template.bind({});
TextAndIcon.args = {
  text: 'top',
};
