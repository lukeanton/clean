/* eslint-disable @typescript-eslint/no-shadow */
import React from 'react';

import cx from 'classnames';
import GoogleMapReact from 'google-map-react';

import { MapMarker } from '../MapMarker';

import { DEFAULT_LOCATION, DEFAULT_ZOOM_SCALE } from './Map.constants';
import { MapProps } from './Map.types';

import './Map.css';

const Map: React.FC<MapProps> = ({
  additionalClassNames,
  isDraggable = false,
  isGrayscale = false,
  isVisible = true,
  location = DEFAULT_LOCATION,
  markers = [],
  zoomScale = DEFAULT_ZOOM_SCALE,
}) => {
  const GOOGLE_MAP_API_KEY = String(process.env.REACT_APP_GOOGLE_MAP_API_KEY);

  if (!GOOGLE_MAP_API_KEY) {
    throw new Error('Please configure the Google Map Key using the environment variable REACT_APP_GOOGLE_MAP_API_KEY');
  }

  const { latitude, longitude } = location;

  if (!isVisible) {
    return null;
  }

  return (
    <div
      className={cx('c-google-map', additionalClassNames, {
        'c-google-map--is-greyscale': isGrayscale,
      })}
    >
      <div className="c-google-map__inner">
        <GoogleMapReact
          bootstrapURLKeys={{
            key: GOOGLE_MAP_API_KEY,
          }}
          center={{
            lat: latitude,
            lng: longitude,
          }}
          defaultZoom={zoomScale}
          options={{
            draggable: isDraggable,
            draggableCursor: !isDraggable ? 'default' : 'grab',
            fullscreenControl: false,
          }}
        >
          {markers.map(({ description, latitude: markerLatitude, longitude: markerLongitude, title }) => {
            return <MapMarker key={title} description={description} lat={markerLatitude} lng={markerLongitude} title={title} />;
          })}
        </GoogleMapReact>
      </div>
    </div>
  );
};

export { Map };
