import { IMapCoordinates } from '@netfront/common-library';

import { IMapMarker } from '../MapMarker';

import { IAdditionalClassNames } from '../../../interfaces';
import { FitBoundsExternalPaddingType } from '../../../types';

export interface MapProps extends IAdditionalClassNames {
  fitBoundsExternalPadding?: FitBoundsExternalPaddingType;
  isDraggable?: boolean;
  isGrayscale?: boolean;
  isVisible?: boolean;
  location?: IMapCoordinates;
  markers?: IMapMarker[];
  text?: string;
  zoomScale?: number;
}
