export const DEFAULT_LOCATION = {
  latitude: -25.2744,
  longitude: 133.7751,
};

export const DEFAULT_ZOOM_SCALE = 4;
