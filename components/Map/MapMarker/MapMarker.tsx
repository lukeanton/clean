import React from 'react';

import { MapMarkerProps } from './MapMarker.types';

import './MapMarker.css';

const MapMarker: React.FC<MapMarkerProps> = ({ description, title }) => {
  const hasDetails = Boolean(title || description);

  return (
    <div className="c-google-map-marker">
      <div aria-label="More info" className="c-google-map-marker__icon" />
      {hasDetails && (
        <div className="c-google-map-marker__details" role="status">
          {title ? <span className="c-google-map-marker__title">{title}</span> : null}
          {description ? <p className="c-google-map-marker__description">{description}</p> : null}
        </div>
      )}
    </div>
  );
};
export { MapMarker };
