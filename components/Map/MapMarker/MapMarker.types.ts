import { IMapCoordinates } from '@netfront/common-library';

export interface IMapMarker extends IMapCoordinates {
  description?: string;
  title?: string;
}

export interface MapMarkerProps extends Omit<IMapMarker, 'latitude' | 'longitude'> {
  lat: number;
  lng: number;
}
