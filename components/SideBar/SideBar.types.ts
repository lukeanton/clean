import { ReactNode } from 'react';

export interface SideBarProps {
  additionalClassNames?: string;
  children: ReactNode;
  isDisplayCloseButton?: boolean;
  isSideBarOpen: boolean;
  onClose: () => void;
}
