import React from 'react';

import cx from 'classnames';

import { ButtonIconOnly } from '../ButtonIconOnly';

import { SideBarProps } from './SideBar.types';

import './SideBar.css';

const SideBar: React.FC<SideBarProps> = ({ additionalClassNames, children, isDisplayCloseButton, isSideBarOpen, onClose }) => {
  return (
    <div className={cx('c-sidebar', additionalClassNames)}>
      <div
        className={cx({
          'c-sidebar__overlay': isSideBarOpen,
          'c-sidebar__overlay--is-hidden': !isSideBarOpen,
        })}
        data-testid="sidebar-overlay"
        role="presentation"
        onClick={onClose}
      />
      <div
        aria-hidden={!isSideBarOpen}
        className={cx('c-sidebar__container', {
          'c-sidebar__container--is-hidden': !isSideBarOpen,
        })}
        data-testid="qa-sidebar-slideout"
        id="offcanvas-sidebar"
        role="dialog"
      >
        {isDisplayCloseButton && (
          <div className="c-sidebar__close-button">
            <ButtonIconOnly additionalClassNames="border-none" iconId="id_close_icon" text="close sidebar" onClick={onClose} />
          </div>
        )}
        {children}
      </div>
    </div>
  );
};

export { SideBar };
