import React, { useState, useEffect } from 'react';

import cx from 'classnames';

import { ButtonList } from '../ButtonList';
import { Dropdown } from '../Dropdown';
import { Icon } from '../Icon';

import { SelectWithSearchProps, ISearchList } from './SelectWithSearch.types';

import './SelectWithSearch.css';

const SelectWithSearch: React.FC<SelectWithSearchProps> = ({
  additionalClassNames,
  buttonText = 'Click to search and select',
  countText = 'accounts',
  isAvatarVisible = false,
  isDisplaySearchContent,
  isDropdownDisabled = false,
  searchList,
  onDisplaySearchContent,
  onSearchItemClick,
}) => {
  const [searchListFiltered, setSearchListFiltered] = useState<ISearchList[]>(searchList);
  const [searchValue, setSearchValue] = useState<string>('');
  const [selectedItemLabel, setSelectedItemLabel] = useState<string>(buttonText);

  const dropdownTrigger: React.ReactNode = (
    <div
      className={cx('c-select-with-search__trigger', {
        'h-with-no-border-left-right-radius': isDisplaySearchContent,
      })}
    >
      <span className="c-select-with-search__trigger-text">{!searchList.length ? 'No items provided' : selectedItemLabel}</span>
      <Icon
        className={cx('c-select-with-search__trigger-icon', {
          'h-rotate-180': isDisplaySearchContent,
        })}
        id="id_arrow_icon"
      />
    </div>
  );

  const handleSearchListItemClick = (id: string | number, label: string) => {
    setSelectedItemLabel(label);
    onDisplaySearchContent(false);
    onSearchItemClick(id);
  };

  const handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = event;

    setSearchValue(value);
  };

  useEffect(() => {
    if (!isDisplaySearchContent) {
      return;
    }

    setSearchValue('');
  }, [isDisplaySearchContent]);

  useEffect(() => {
    if (!searchList) {
      return;
    }

    const filteredSearchList = searchList.filter(({ label }) =>
      label.toLocaleLowerCase().trim().includes(searchValue.trim().toLocaleLowerCase())
    );

    setSearchListFiltered(filteredSearchList);
  }, [searchValue, searchList]);

  return (
    <Dropdown
      additionalClassNames={cx('c-select-with-search', additionalClassNames, {
        'c-select-with-search--disabled': !searchList.length,
      })}
      dropdownId="select-search-dropdown"
      isDisabled={!searchList.length || isDropdownDisabled}
      isDisplayContent={isDisplaySearchContent}
      trigger={dropdownTrigger}
      onDisplayContent={onDisplaySearchContent}
    >
      <div
        className={cx('c-select-with-search__content', {
          'h-hide-visually': !isDisplaySearchContent,
        })}
      >
        <div className="c-select-with-search__search">
          <Icon className="c-select-with-search__search-icon" id="id_search_icon" />
          <input
            className="c-select-with-search__input"
            placeholder="Search"
            type="text"
            value={searchValue}
            onChange={handleSearchInputChange}
          />
          <button className="c-select-with-search__cross-button" title="Clear search" type="button" onClick={() => setSearchValue('')}>
            {searchValue ? <Icon className="c-select-with-search__cross-icon" id="id_close_icon" /> : null}
          </button>
        </div>
        <ButtonList
          additionalClassNames="c-select-with-search__list"
          countText={countText}
          isAvatarVisible={isAvatarVisible}
          listItems={searchListFiltered}
          onListItemClick={handleSearchListItemClick}
        />
      </div>
    </Dropdown>
  );
};

export { SelectWithSearch };
