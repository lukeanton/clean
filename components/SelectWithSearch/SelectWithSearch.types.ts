import { IListItem } from '../ButtonList';

import { IAdditionalClassNames } from '../../interfaces';

export interface ISearchList extends IListItem {}

export interface SelectWithSearchProps extends IAdditionalClassNames {
  /*
   * The text inside the reveal search button
   */
  buttonText?: string;
  /*
   * The text inside the count text label which explains the type of list items
   */
  countText?: string;
  /*
   * Boolean value to decide whether to display avatar
   */
  isAvatarVisible?: boolean;
  /*
   * Boolean value to decide whether to display search content
   */
  isDisplaySearchContent: boolean;
  /*
   * Boolean value to decide whether to disable dropdown
   */
  isDropdownDisabled?: boolean;
  /*
   * Provide a function that will be set display search content to the state
   */
  onDisplaySearchContent: (isDisplay: boolean) => void;
  /*
   * Provide a function that will be triggered when the search list item is clicked
   */
  onSearchItemClick: (selectedId: number | string) => void;
  /*
   * List displayed as search items
   */
  searchList: ISearchList[];
}
