import React from 'react';

import { render, screen, fireEvent } from '@testing-library/react';
import { Tooltip } from './Tooltip';
import { TooltipProps } from './Tooltip.types';

describe('Tooltip', () => {
  let props: TooltipProps = {
    iconId: undefined,
    placement: undefined,
    text: 'i am a tooltip',
  };
  let { text } = props;

  describe('when the tooltip is rendered without errors', () => {
    test('the tooltip will show tooltip text on hover by default', () => {
      render(<Tooltip {...props} />);

      const tooltipText = screen.queryByTestId('qa-tooltip-text');

      expect(tooltipText).toBeNull();
    });
  });

  describe('when the tooltip is rendered with a trigger type of click', () => {
    test('the tooltip will show tooltip text will stay visible after it is clicked', () => {
      render(<Tooltip {...props} />);

      const tooltipText = screen.queryByTestId('qa-tooltip-text');

      expect(tooltipText).toBeNull();
    });
  });
});
