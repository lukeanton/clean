import { IAdditionalClassNames, IIconId } from '../../interfaces';
import { TooltipPlacementType } from '../../types';

export interface TooltipProps extends IAdditionalClassNames, IIconId {
  /*
   * The boolean value that decides wether the tooltip is shown on hover
   */
  isShownOnHover?: boolean;
  /*
   * Specify the placement of the tooltip text
   */
  placement?: TooltipPlacementType;
  /*
   * Specify meaningful text that describes the tooltip
   */
  text: string;
}
