import React, { useState } from 'react';
import { RangeInput } from './RangeInput';

export default {
  title: 'components/RangeInput',
  component: RangeInput,
};

export const Default = () => {
  const [selectedValues, setSelectedValues] = useState<number[]>([2]);

  const handleChange = (values: number[]) => {
    setSelectedValues(values);
  };

  return <RangeInput maxValue={8} minValue={0} selectedValues={selectedValues} onChange={handleChange} step={2} />;
};
