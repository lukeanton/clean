export interface RangeInputProps {
  maxValue?: number;
  minValue?: number;
  /**
   * The function to call when a button is interacted with
   */
  onChange: (values: number[]) => void;
  selectedValues?: number[];
  step?: number;
}
