import last from 'lodash.last';

const getIdFromKey = (key: string): number => {
  return Number(last(key.split('/')));
};

export { getIdFromKey };
