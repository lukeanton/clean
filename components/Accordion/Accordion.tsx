import React from 'react';

import cx from 'classnames';
import TreeMenu from 'react-simple-tree-menu';

import { ButtonIconOnly } from '../ButtonIconOnly';
import { PlayButton } from '../Buttons/PlayButton';
import { ToggleSwitch } from '../ToggleSwitch';

import { getIdFromKey } from './Accordion.helpers';
import { AccordionProps } from './Accordion.interfaces';

import './Accordion.css';

const Accordion = ({ audioAssetBaseUrl, data, onToggleSwitchClick, selectedIds = [] }: AccordionProps) => {
  return (
    <div className="c-accordion">
      <TreeMenu data={data}>
        {({ items }) => {
          return (
            <React.Fragment>
              {items.map(({ hasNodes, key, isOpen, label, level, parent, toggleNode }) => {
                const handleDropdownClick = () => {
                  if (!(hasNodes && toggleNode)) {
                    return;
                  }

                  toggleNode();
                };

                const id = getIdFromKey(key);
                const isChecked = Boolean(selectedIds.find((selectedId) => selectedId === id));

                const dropdownCaret = hasNodes && (
                  <div className="c-accordion__item-dropdown-icon-container">
                    <ButtonIconOnly
                      additionalClassNames={cx('c-accordion__item-dropdown-icon', {
                        'c-accordion__item-dropdown-icon--rotate': isOpen,
                      })}
                      iconId="id_caret_down_icon"
                      text=""
                      onClick={() => handleDropdownClick()}
                    />
                  </div>
                );

                const toggleSwitch =
                  onToggleSwitchClick && parent ? (
                    <div className="c-accordion__item-toggle">
                      <ToggleSwitch id={key} isChecked={isChecked} labelText="" onChange={() => onToggleSwitchClick(id)} />
                    </div>
                  ) : null;

                const playButton =
                  audioAssetBaseUrl && parent ? (
                    <PlayButton audioSrc={`${audioAssetBaseUrl}/${parent.toLowerCase()}-${label.toLowerCase()}.mp3`} />
                  ) : null;

                return (
                  <div
                    className={cx({
                      'c-accordion__item-parent-container': !parent,
                      'c-accordion__item-child-container': parent,
                    })}
                  >
                    <div key={key.toLowerCase()} className="c-accordion__item-container">
                      <div className="c-accordion__item-contents-container">
                        <div
                          className={cx('c-accordion__item-label', {
                            [`c-accordion__item-label-${level}`]: level > 0,
                          })}
                        >
                          {label}
                        </div>
                        <div className="c-accordion__item-interaction">
                          {playButton}
                          {toggleSwitch}
                        </div>
                      </div>
                      {dropdownCaret}
                    </div>
                  </div>
                );
              })}
            </React.Fragment>
          );
        }}
      </TreeMenu>
    </div>
  );
};

export { Accordion };
