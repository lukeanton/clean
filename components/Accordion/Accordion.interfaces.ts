import { TreeNodeInArray } from 'react-simple-tree-menu';

export interface AccordionProps {
  /*
   * Base url of the audio asset e.g. https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com
   */
  audioAssetBaseUrl?: string;
  /*
   * Provide the data that will be displayed within the accordion
   */
  data: TreeNodeInArray[];
  /**
   * Boolean value to decide whether to display PlayButton next to the label
   */
  isDisplayPlayAudioButton?: boolean;
  /*
   * Provide a function that will be triggered when the toggle switch is clicked
   */
  onToggleSwitchClick?: (selectedId: number) => void;
  /*
   * Specify the selected id's
   */
  selectedIds?: number[];
}
