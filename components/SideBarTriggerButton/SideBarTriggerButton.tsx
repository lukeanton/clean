import React from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';

import { SideBarTriggerButtonProps } from './SideBarTriggerButton.types';

import './SideBarTriggerButton.css';

const SideBarTriggerButton: React.FC<SideBarTriggerButtonProps> = ({
  ariaControls = 'offcanvas-sidebar',
  buttonContent,
  isSideBarOpen,
  onClick,
}) => {
  const { iconId, text } = buttonContent;

  return (
    <button
      aria-controls={ariaControls}
      aria-expanded={isSideBarOpen}
      aria-label={iconId ? text : undefined}
      className={cx('c-sidebar-trigger-button', {
        'c-sidebar-trigger-button--is-icon': Boolean(iconId),
      })}
      data-testid="qa-sidebar-trigger"
      type="button"
      onClick={onClick}
    >
      {iconId ? <Icon className="c-sidebar-trigger-button__icon" id={iconId} /> : text}
    </button>
  );
};

export { SideBarTriggerButton };
