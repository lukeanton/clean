import { IconIdType } from '../../types';

export type TriggerButtonContent =
  | {
      iconId?: IconIdType;
      text?: never;
    }
  | {
      iconId?: never;
      text: string;
    };

export interface SideBarTriggerButtonProps {
  ariaControls?: string;
  buttonContent: TriggerButtonContent;
  isSideBarOpen: boolean;
  onClick: () => void;
}
