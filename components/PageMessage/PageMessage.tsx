import React from 'react';

import cx from 'classnames';

import { LinkButton } from '../Buttons';
import { Icon } from '../Icon';

import { PageMessageProps } from './PageMessage.types';

import './PageMessage.css';

const PageMessage: React.FC<PageMessageProps> = ({
  additionalClassNames,
  contactUrl,
  dashboardUrl,
  headerText,
  iconIdEnd,
  iconIdStart,
  message,
}) => {
  const pageMessageClassNames = cx('c-page-message', additionalClassNames);

  return (
    <div className={pageMessageClassNames}>
      {iconIdStart ? <Icon className="c-page-message__icon" id={iconIdStart} /> : null}
      <div className="c-page-message__content">
        <h1 className="c-page-message__content-header">{headerText}</h1>
        <div className="c-page-message__content-border" />
        <p className="c-page-message__content-message">{message}</p>
        <div className="c-page-message__content-border" />
        <div className="c-page-message__content-button-group">
          <LinkButton size="medium" text="Dashboard" url={dashboardUrl} />
          <LinkButton backgroundColor="lightslategrey" size="medium" text="Contact" url={contactUrl} />
        </div>
      </div>
      {iconIdEnd ? <Icon className="c-page-message__icon" id={iconIdEnd} /> : null}
    </div>
  );
};

export { PageMessage };
