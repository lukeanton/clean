import { IAdditionalClassNames } from '../../interfaces';
import { IconIdType } from '../../types';

export interface PageMessageProps extends IAdditionalClassNames {
  /**
   * The url path to the contact
   */
  contactUrl: string;
  /**
   * The url path to the dashboard
   */
  dashboardUrl: string;
  /**
   * The header text displayed with the PageMessage
   */
  headerText: string;
  /**
   * The end icon used in a PageMessage asset
   */
  iconIdEnd?: IconIdType;
  /**
   * The start icon used in a PageMessage asset
   */
  iconIdStart?: IconIdType;
  /**
   * The message displayed with the PageMessage
   */
  message: string;
}
