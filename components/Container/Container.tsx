import React from 'react';

import cx from 'classnames';

import { ContainerProps } from './Container.types';

import './Container.css';

const Container: React.FC<ContainerProps> = ({ additionalClassNames, alignment = 'center', children, isNarrow = false }) => {
  return (
    <div
      className={cx('l-container', `l-container--is-align-${alignment}`, additionalClassNames, {
        'l-container--is-narrow': isNarrow,
      })}
    >
      {children}
    </div>
  );
};
export { Container };
