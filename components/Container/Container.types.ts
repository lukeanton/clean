import { IAdditionalClassNames } from '../../interfaces';
import { AlignmentType } from '../../types';

export interface ContainerProps extends IAdditionalClassNames {
  alignment?: AlignmentType;
  isNarrow?: boolean;
}
