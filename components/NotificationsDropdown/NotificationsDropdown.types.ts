import { IAdditionalClassNames } from '../../interfaces';

export interface NotificationsDropdownProps extends IAdditionalClassNames {
  /**
   * The navigation items displayed
   */
  notificationListItems: INotificationsDropdownListItems[];
  /**
   * The max number of displayed list items
   */
  numberOfDisplayedNotificationListItems?: number;
  /**
   * Provide an onClick function that will be triggered when the button is clicked
   */
  onClick: (id: string) => void;
}

export interface INotificationsDropdownListItems {
  /**
   * notification item id
   */
  id: string;
  /**
   * Boolean to decide whether to notification is accepted
   */
  isRead?: boolean;
  /**
   * text to the link displayed
   */
  text: string;
}
