import React, { useState, useEffect } from 'react';

import cx from 'classnames';

import { NotificationsDropdownProps } from './NotificationsDropdown.types';

import './NotificationsDropdown.css';

const NotificationsDropdown: React.FC<NotificationsDropdownProps> = ({
  additionalClassNames,
  onClick,
  notificationListItems,
  numberOfDisplayedNotificationListItems = 3,
}) => {
  const notificationsDropdownClassNames = cx('c-notifications-dropdown', additionalClassNames);

  const [shouldDisplayAllNotifications, setShouldDisplayAllNotifications] = useState(false);
  const [displayedListItems, setDisplayedListItems] = useState(notificationListItems.slice(0, numberOfDisplayedNotificationListItems));

  const handleClick = () => {
    setShouldDisplayAllNotifications(!shouldDisplayAllNotifications);
  };

  useEffect(() => {
    setDisplayedListItems(
      shouldDisplayAllNotifications ? notificationListItems : notificationListItems.slice(0, numberOfDisplayedNotificationListItems)
    );
  }, [shouldDisplayAllNotifications]);

  const { length: totalNotifications } = notificationListItems;

  const buttonText = shouldDisplayAllNotifications
    ? `See less notifications (${numberOfDisplayedNotificationListItems})`
    : `See all notifications (${totalNotifications})`;

  return (
    <div className={notificationsDropdownClassNames}>
      {totalNotifications ? (
        <React.Fragment>
          <ol className="c-notifications-dropdown__list">
            {displayedListItems.map(({ id, text, isRead }) => (
              <li key={text} className="c-notifications-dropdown__list-item">
                <div
                  className={cx('c-notifications-dropdown__list-item-status', {
                    'c-notifications-dropdown__list-item-read': isRead,
                    'c-notifications-dropdown__list-item-not-read': !isRead,
                  })}
                />
                <button className="c-notifications-dropdown__list-item-button" type="button" onClick={() => onClick(id)}>
                  {text}
                </button>
              </li>
            ))}
          </ol>
          {numberOfDisplayedNotificationListItems < totalNotifications ? (
            <button className="c-notifications-dropdown__button" type="button" onClick={handleClick}>
              {buttonText}
            </button>
          ) : null}
        </React.Fragment>
      ) : (
        <span>You have no notifications</span>
      )}
    </div>
  );
};

export { NotificationsDropdown };
