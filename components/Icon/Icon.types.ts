import { IconIdType } from '../../types';

export interface IconProps {
  /**
   * The class to apply to the icon
   */
  className?: string;
  /**
   * Whether to hide the icon from screen readers
   */
  hasAriaHidden?: boolean;
  /**
   * The id of the icon to be mapped to the SVG icon
   */
  id: IconIdType;
}
