import React from 'react';

import { render, screen } from '@testing-library/react';
import { Icon } from './Icon';
import { IconIdType } from '../../types';

describe('Icon', () => {
  const id: IconIdType = 'id_tick_icon';

  it('Should display the Icon component in the document', () => {
    render(<Icon id={id} />);

    const icon = screen.getByTestId('qa-icon');

    expect(icon).toBeInTheDocument();
  });

  it('Should have the xlink:href attribute with the correct id ', () => {
    render(<Icon id={id} />);

    const iconUse = screen.getByTestId('qa-icon-use');

    expect(iconUse).toHaveAttribute('xlink:href', '#' + id);
  });

  describe('className prop', () => {
    const className = 'c-class-name';

    describe('When the className prop is set', () => {
      it('Should have the c-class-name css class', () => {
        render(<Icon className={className} id={id} />);

        const icon = screen.getByTestId('qa-icon');

        expect(icon).toHaveClass(className);
      });
    });

    describe('When the className prop is not set', () => {
      it('Should not have the c-class-name css class', () => {
        render(<Icon id={id} />);

        const icon = screen.getByTestId('qa-icon');

        expect(icon).not.toHaveClass(className);
      });
    });
  });

  describe('hasAriaHidden prop', () => {
    describe('When the hasAriaHidden prop is set to true by default', () => {
      it('Should have the aria-hidden attribute set to true', () => {
        render(<Icon id={id} />);

        const icon = screen.getByTestId('qa-icon');

        expect(icon).toHaveAttribute('aria-hidden', 'true');
      });
    });

    describe('When the hasAriaHidden prop is set to false', () => {
      it('Should have the aria-hidden attribute set to false', () => {
        render(<Icon hasAriaHidden={false} id={id} />);

        const icon = screen.getByTestId('qa-icon');

        expect(icon).toHaveAttribute('aria-hidden', 'false');
      });
    });
  });
});
