import { IIdentifier } from '../../interfaces';

export interface MessageProps extends IIdentifier {
  text: string | undefined;
  type: 'ERROR' | 'SUCCESS';
}
