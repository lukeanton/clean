import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Textarea } from './Textarea';

import { TextareaProps } from './Textarea.types';

import { action } from '@storybook/addon-actions';

export default {
  title: 'Components/Textarea',
  component: Textarea,
} as Meta;

const Template: Story<TextareaProps> = (args) => <Textarea {...args} />;

export const Base = Template.bind({});
Base.args = {
  id: 'description',
  name: 'description',
  onChange: action('Value =>'),
};

export const WithPlaceholder = Template.bind({});
WithPlaceholder.args = {
  id: 'description',
  name: 'description',
  placeholder: 'Please enter description',
  onChange: action('Value =>'),
};

export const WithLabel = Template.bind({});
WithLabel.args = {
  id: 'description',
  name: 'description',
  placeholder: 'Please enter description',
  labelText: 'Description',
  onChange: action('Value =>'),
};

export const Disabled = Template.bind({});
Disabled.args = {
  isDisabled: true,
  id: 'description',
  name: 'description',
  placeholder: 'Please enter description',
  labelText: 'Description',
};

export const Required = Template.bind({});
Required.args = {
  id: 'description',
  isRequired: true,
  name: 'description',
  placeholder: 'Please enter description',
  labelText: 'Description',
  value: 'Required value',
  onChange: action('Value =>'),
};

export const Correct = Template.bind({});
Correct.args = {
  id: 'description',
  isCorrect: true,
  name: 'description',
  placeholder: 'Please enter description',
  labelText: 'Description',
  onChange: action('Value =>'),
};
