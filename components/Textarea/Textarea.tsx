import React from 'react';

import cx from 'classnames';

import { Icon } from '../Icon';
import { getValidationResult } from '../Input/Input.helpers';
import { Label } from '../Label';

import { TextareaProps } from './Textarea.types';

import './Textarea.css';

const Textarea: React.FC<TextareaProps> = ({
  additionalClassNames,
  errorMessage,
  id,
  isCorrect = false,
  isDirty = false,
  isDisabled = false,
  isReadOnly = false,
  isRequired = false,
  isLabelHidden = false,
  labelText,
  name,
  onChange,
  placeholder,
  value,
}) => {
  const { iconId, isValid } = getValidationResult(errorMessage, isCorrect, isDirty);

  return (
    <React.Fragment>
      <Label
        additionalClassNames={cx('c-textarea--label', {
          'c-textarea-label--is-required': isRequired && !isValid,
        })}
        forId={id}
        isHidden={isLabelHidden}
        isRequired={isRequired}
        labelText={labelText}
      />
      <div className="c-textarea__container h-flex">
        {iconId ? <Icon className="c-textarea--icon" id={iconId} /> : null}
        <textarea
          className={cx('c-textarea', additionalClassNames, {
            'c-textarea--is-correct': isCorrect,
            'c-textarea--is-error': Boolean(errorMessage),
            'c-textarea--is-disabled': isDisabled,
            'c-textarea--is-required': isRequired && !isValid,
          })}
          data-testid="qa-textarea"
          disabled={isDisabled}
          id={id}
          name={name}
          placeholder={placeholder}
          readOnly={isReadOnly}
          required={isRequired}
          value={value}
          onChange={onChange}
        />
      </div>
    </React.Fragment>
  );
};

export { Textarea };
