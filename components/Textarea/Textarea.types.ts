import {
  IAdditionalClassNames,
  IErrorMessage,
  IIdentifier,
  IIsDisabled,
  IIsLabelHidden,
  IIsReadOnly,
  IIsRequired,
  ILabelText,
  IName,
  IPlaceholder,
} from '../../interfaces';

export interface TextareaProps
  extends IAdditionalClassNames,
    IErrorMessage,
    IIdentifier,
    IIsDisabled,
    IIsLabelHidden,
    IIsReadOnly,
    IIsRequired,
    ILabelText,
    IName,
    IPlaceholder {
  /**
   * The boolean value deciding whether the textarea content is correct
   */
  isCorrect?: boolean;
  /**
   * The boolean value deciding whether the textarea is dirty
   */
  isDirty?: boolean;
  /**
   * The action to call value change a textarea
   */
  onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
  /**
   * The value of a textarea
   */
  value?: string;
}
