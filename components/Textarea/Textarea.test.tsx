import React from 'react';
import { render, screen } from '@testing-library/react';
import { Textarea } from './Textarea';
import { TextareaProps } from './Textarea.types';

describe('Textarea', () => {
  const onChange = (e: any) => {
    console.log('Value', e.target.value);
  };

  const props: TextareaProps = {
    id: 'description',
    isDisabled: false,
    isRequired: false,
    name: 'description',
    onChange: onChange,
    placeholder: '',
    text: '',
    value: 'Lorem ipsum sit amet',
  };

  test('it should display the textarea item', () => {
    render(<Textarea {...props} />);
    const textarea = screen.getByTestId('qa-textarea');
    expect(textarea).toBeInTheDocument();
  });

  describe('When textarea has placeholder', () => {
    test('it should display the placeholder', () => {
      props.placeholder = 'Please enter';
      render(<Textarea {...props} isDisabled={true} />);

      const textarea = screen.getByPlaceholderText('Please enter');
      expect(textarea).toHaveAttribute('disabled');
      expect(textarea).toHaveClass('c-textarea--is-disabled');
    });
  });

  describe('When textarea is disabled', () => {
    test('it should disabled the textarea field', () => {
      render(<Textarea {...props} isDisabled={true} />);

      const textarea = screen.getByTestId('qa-textarea');
      expect(textarea).toHaveAttribute('disabled');
      expect(textarea).toHaveClass('c-textarea--is-disabled');
    });
  });

  describe('When textarea is required', () => {
    test('it should make the textarea field required', () => {
      render(<Textarea {...props} isRequired={true} />);

      const textarea = screen.getByTestId('qa-textarea');
      expect(textarea).toHaveAttribute('required');
      expect(textarea).toHaveClass('c-textarea--is-required');

      const label = screen.getByText('*');
      expect(label).toBeInTheDocument();
    });
  });

  describe('When textarea has label text', () => {
    test('it should display the label', () => {
      render(<Textarea {...props} text="Description" />);

      const label = screen.getByText('Description');
      expect(label).toBeInTheDocument();
    });
  });
});
