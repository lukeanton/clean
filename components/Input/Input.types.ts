import {
  IAdditionalClassNames,
  IErrorMessage,
  IIdentifier,
  IIsDisabled,
  IIsLabelHidden,
  IIsReadOnly,
  IIsRequired,
  ILabelText,
  IName,
  IPlaceholder,
} from '../../interfaces';

export interface InputProps
  extends IAdditionalClassNames,
    IErrorMessage,
    IIdentifier,
    IIsDisabled,
    IIsLabelHidden,
    IIsReadOnly,
    IIsRequired,
    ILabelText,
    IName,
    IPlaceholder {
  /**
   * Html input autocomplete attribute to decide the input value is current or new
   */
  autoComplete?: string;
  /**
   * The boolean value deciding whether the textarea is correct
   */
  isCorrect?: boolean;
  /**
   * The boolean value deciding whether the textarea is dirty
   */
  isDirty?: boolean;
  /**
   * The boolean value deciding whether the textarea content is correct
   */
  isValid?: boolean;
  /**
   * The maximum amount of characters an input can have
   */
  maxLength?: number;
  /**
   *  The highest number an input is allowed
   */
  maxNumber?: number;
  /**
   * The minimum amount of characters an input can have
   */
  minLength?: number;
  /**
   * The lowest number an input is allowed
   */
  minNumber?: number;
  /**
   * The action to call when user escapes from input
   */
  onBlur?: () => void;
  /**
   * The action to call value change an input
   */
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  /**
   * The action to focus on input
   */
  onFocus?: () => void;
  /**
   * The action to call when key press
   */
  onKeyPress?: () => void;
  /**
   * This value shows the success message
   */
  successMessage?: string;
  /**
   * The type of an input
   */
  type: 'email' | 'number' | 'password' | 'tel' | 'text';
  /**
   * The value of an input
   */
  value?: string | number;
}
