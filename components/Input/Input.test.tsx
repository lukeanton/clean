import React from 'react';

import { render, screen } from '@testing-library/react';

import { Input } from './Input';

// TODO: Add more test scenarios including all the props based
describe('Input', () => {
  const onChange = () => {};

  const placeholder = 'Please enter';

  it('should display the input item', () => {
    render(
      <Input
        id="test_id"
        isLabelHidden={false}
        labelText="Label"
        name="test_name"
        onChange={onChange}
        type="text"
        placeholder={placeholder}
      />
    );

    const input = screen.getByPlaceholderText(placeholder);

    expect(input).toBeInTheDocument();
  });

  describe('When input is disabled', () => {
    it('should disabled the input field', () => {
      render(
        <Input id="test_id" isLabelHidden={false} labelText="Label" name="test_name" onChange={onChange} type="text" isDisabled={true} />
      );

      const input = screen.queryByTestId('qa-input');

      expect(input).toHaveClass('c-input--is-disabled');
    });
  });
});
