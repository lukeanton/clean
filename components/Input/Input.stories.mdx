import { Meta, Story, Canvas } from '@storybook/addon-docs';
import { Input } from './Input';
import { Icon } from '../Icon';
import { Icons } from '../Icons';
import { Spacing } from '../Spacing';
import { ApiTable } from '../../helpers/storybook/ApiTable/ApiTable.tsx';

import './Input.css';
import '../../helpers/storybook/Storybook.css';

<Meta title="Components/Input" component={Input} />

# Input

<Icons />

<a
  style={{ display: 'flex', gap: '0.5rem', marginBottom: '1.5rem' }}
  href="https://xd.adobe.com/view/de6caa1b-d49f-4a15-84bc-1424e9a2a22b-8b3f/screen/f35eb469-76c6-4b33-a8ae-5746dfbd4b44/specs/"
  target="_blank"
>
  <Icon id="id_xd_icon" className="c-xd-icon" />
  <span>XD Design - Inputs</span>
</a>

## Table of Contents

- [Overview](#overview)
- [Basic Input](#basic-input)
- [Input Validation](#input-validation)
- [Visually Hidden Label](#visually-hidden-label)
- [Disabled Input](#disabled-input)
- [API](#api)

## Overview

The `Input` component is used for entering single line text such as email addresses and names.

## Basic Input

Here is a basic `Input` that could be used to collect a users first name.

<Canvas>
  <Story name="Input">
    <Spacing>
      <Input id="storybook-input" labelText="First Name" onChange={() => null} />
    </Spacing>
  </Story>
</Canvas>

## Input Validation

The `Input` component accepts various props that can be used to represent validation.

### Valid Input

Here is a valid `Input`.

<Canvas>
  <Story name="Valid Input">
    <Spacing>
      <Input id="firstName" isCorrect labelText="First Name" onChange={() => null} value="Beatrix" />
    </Spacing>
    <Spacing>
      <Input id="email-address" isCorrect labelText="Email Address" onChange={() => null} value="beatrixKiddo@bill.com" />
    </Spacing>
  </Story>
</Canvas>

## Invalid Input

Here is an invalid `Input`.

<Canvas>
  <Story name="Invalid Input">
    <Spacing>
      <Input id="lastName" errorMessage="This is not a valid last name" labelText="Last Name" onChange={() => null} value="9531" />
    </Spacing>
    <Spacing>
      <Input
        id="phoneNumber"
        errorMessage="This is not a valid phone number"
        labelText="Phone Number"
        onChange={() => null}
        type="tel"
        value="Beatrix"
      />
    </Spacing>
  </Story>
</Canvas>

## Required Input

If an `Input` field is required and cannot be submitted unless valid then specify the `isRequired` prop.

<Canvas>
  <Story name="Required Input">
    <Spacing>
      <Input id="required-input" isRequired labelText="First Name" onChange={() => null} value="Beatrix" />
    </Spacing>
  </Story>
</Canvas>

## Disabled Input

If you would like to disable the `Input`, specify the isDisabled prop.

<Canvas>
  <Story name="Disabled Input">
    <Spacing>
      <Input id="disabled-input" isDisabled labelText="First Name" onChange={() => null} value="Beatrix" />
    </Spacing>
  </Story>
</Canvas>

## Visually Hidden Label

All `Input`s require a label to allow screen readers to acceess elements.

If you would like to hide the label visually specify the isLabelHidden prop.

Here is the `Input` component with a visually hidden label.

<Canvas>
  <Story name="Visually Hidden Label">
    <Spacing>
      <Input id="hidden-label" isLabelHidden labelText="First Name" onChange={() => null} value="Beatrix" />
    </Spacing>
  </Story>
</Canvas>

## API

### Input

<ApiTable
  apiTableData={[
    {
      defaultProp: '-',
      description: <span>Specify an optional additionalClassNames to be applied to the parent node string</span>,
      propName: 'additionalClassNames',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>Input autocomplete attribute to decide the input value is current or new</span>,
      propName: 'autoComplete',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>Provide the text that is displayed when the control is in an invalid state</span>,
      propName: 'errorMessage',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: (
        <span>
          <code>Specify a custom id for the string</code>–
        </span>
      ),
      propName: 'id',
      type: 'string',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input is correct</span>,
      propName: 'isCorrect',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input has been dirty</span>,
      propName: 'isDirty',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input is disabled</span>,
      propName: 'isDisabled',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>Specify whether you want the underlying label to be visually hidden</span>,
      propName: 'isLabelHidden',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input is read only</span>,
      propName: 'isReadOnly',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input is required</span>,
      propName: 'isRequired',
      type: 'bool',
    },
    {
      defaultProp: 'false',
      description: <span>A boolean value that deciding wether the input is valid</span>,
      propName: 'isValid',
      type: 'bool',
    },
    {
      defaultProp: '-',
      description: <span>Provide the text that will be read by a screen reader when visiting this input</span>,
      propName: 'labelText',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>The maximum amount of characters an input can have</span>,
      propName: 'maxLength',
      type: 'number',
    },
    {
      defaultProp: '-',
      description: <span>The highest number an input is allowed</span>,
      propName: 'maxNumber',
      type: 'number',
    },
    {
      defaultProp: '-',
      description: <span>The minimum amount of characters an input can have</span>,
      propName: 'minLength',
      type: 'number',
    },
    {
      defaultProp: '-',
      description: <span>The lowest number an input is allowed</span>,
      propName: 'minNumber',
      type: 'number',
    },
    {
      defaultProp: '-',
      description: <span>Specify a name that can be referenced in javascript</span>,
      propName: 'name',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>Optionally provide an onBlur handler that is called whenever the Input is unfocused</span>,
      propName: 'onBlur',
      type: 'func',
    },
    {
      defaultProp: '-',
      description: <span>Provide an onChange handler that is called whenever the Input is changed</span>,
      propName: 'onChange',
      type: 'func',
    },
    {
      defaultProp: '-',
      description: <span>Provide an onFocus handler that is called whenever focussed on input</span>,
      propName: 'onFocus',
      type: 'func',
    },
    {
      defaultProp: '-',
      description: <span>Optionally provide an onKeyPress handler that is called whenever a key is pressed within Input</span>,
      propName: 'onKeyPress',
      type: 'func',
    },
    {
      defaultProp: 'primary',
      description: <span>Specify the placeholder attribute for the Input</span>,
      propName: 'placeholder',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>Provide the text that is displayed when the control is in a successful state</span>,
      propName: 'successMessage',
      type: 'string',
    },
    {
      defaultProp: 'text',
      description: <span>Specify the type of Input</span>,
      propName: 'type',
      type: 'string',
    },
    {
      defaultProp: '-',
      description: <span>Provide the current value of the Input</span>,
      propName: 'value',
      type: 'string',
    },
  ]}
/>
