import { IAdditionalClassNames } from '../../../interfaces';

export interface PlayButtonProps extends IAdditionalClassNames {
  /**
   * Source of the audio element
   */
  audioSrc: string;
}
