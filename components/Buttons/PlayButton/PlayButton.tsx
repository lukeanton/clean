import React, { useState, useEffect, useRef, MutableRefObject } from 'react';

import cx from 'classnames';

import { PlayButtonProps } from './PlayButton.interfaces';

import { Icon } from '../../Icon';

import './PlayButton.css';

const PlayButton: React.FC<PlayButtonProps> = ({ additionalClassNames, audioSrc }) => {
  const audioRef = useRef() as MutableRefObject<HTMLAudioElement>;
  const [isAudioPlayEnabled, setIsAudioPlayEnabled] = useState(false);

  const playButtonIconClassNames = cx('c-play-button__icon', {
    'c-play-button__play-icon': !isAudioPlayEnabled,
    'c-play-button__pause-icon': isAudioPlayEnabled,
  });

  const { readyState: audioReadyState } = audioRef.current || {};

  const handleClickAudioPlay = () => {
    setIsAudioPlayEnabled(!isAudioPlayEnabled);
  };

  useEffect(() => {
    if (!audioReadyState) {
      return;
    }

    if (audioReadyState < 4) {
      return;
    }

    if (isAudioPlayEnabled) {
      audioRef.current.play();
    } else {
      audioRef.current.pause();
    }
  }, [isAudioPlayEnabled, audioReadyState]);

  return (
    <React.Fragment>
      <button
        className={cx('c-play-button', additionalClassNames)}
        data-testid="qa-play-button"
        type="button"
        onClick={handleClickAudioPlay}
      >
        <span className="h-hide-visually">{isAudioPlayEnabled ? 'Pause audio' : 'Play audio'}</span>
        <Icon className={playButtonIconClassNames} id={isAudioPlayEnabled ? 'id_pause_icon' : 'id_play_icon'} />
      </button>
      <audio ref={audioRef} aria-label="audio" className="h-hide-visually" controls onEnded={handleClickAudioPlay}>
        <track kind="captions" />
        <source src={audioSrc} type="audio/mpeg" />
      </audio>
    </React.Fragment>
  );
};

export { PlayButton };
