import { IAdditionalClassNames, ISupportiveText } from '../../../interfaces';

export interface CopyButtonProps extends IAdditionalClassNames, ISupportiveText {
  copyElementRef: React.RefObject<
    HTMLDivElement | HTMLInputElement | HTMLParagraphElement | HTMLSpanElement | HTMLTableCellElement | HTMLTextAreaElement
  >;
  onCopyCompleted: () => void;
}
