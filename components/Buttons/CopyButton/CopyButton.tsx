import React from 'react';

import cx from 'classnames';

import { CopyButtonProps } from './CopyButton.types';

import { Icon } from '../../Icon';

import './CopyButton.css';

const CopyButton: React.FC<CopyButtonProps> = ({ additionalClassNames, copyElementRef, onCopyCompleted, supportiveText }) => {
  const copyClassNames = cx('c-copy-button', additionalClassNames);

  const handleCopyToClipBoard = () => {
    if (!copyElementRef.current) {
      return;
    }

    let copiedValue = '';
    const { current } = copyElementRef;

    if (current instanceof HTMLInputElement || current instanceof HTMLTextAreaElement) {
      copiedValue = current.value;
    } else if (
      current instanceof HTMLDivElement ||
      current instanceof HTMLParagraphElement ||
      current instanceof HTMLSpanElement ||
      current instanceof HTMLTableCellElement
    ) {
      copiedValue = current.innerText;
    }

    navigator.clipboard.writeText(copiedValue);

    onCopyCompleted();
  };

  return (
    <button className={copyClassNames} data-testid="qa-copy" type="button" onClick={handleCopyToClipBoard}>
      <Icon className="c-copy-button__icon" id="id_copy_icon" />
      <span className="h-hide-visually">{supportiveText}</span>
    </button>
  );
};

export { CopyButton };
