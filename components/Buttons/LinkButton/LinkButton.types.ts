import { ElementType } from 'react';

import { PartialPick } from '@netfront/common-library';

import { IAdditionalClassNames, IBackgroundColor, IBorderColor, IIdentifier } from '../../../interfaces';
import { ButtonSizeType } from '../../../types';

export interface LinkButtonProps extends IAdditionalClassNames, IBackgroundColor, IBorderColor, PartialPick<IIdentifier, 'id'> {
  /**
   * The element used as a link
   */
  linkComponent?: ElementType;
  /**
   * The size of the button link
   */
  size?: ButtonSizeType;
  /**
   * Where to display the linked URL, as the name for a browsing context (a tab, window, or `<iframe>`)
   */
  target?: '_blank' | '_self' | '_parent' | 'top';
  /**
   * The displayed text for the link
   */
  text: string;
  /**
   * The URL that the hyperlink points to
   */
  url: string;
}
