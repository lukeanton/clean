import React from 'react';
import { Story, Meta } from '@storybook/react';

import { LinkButton } from '.';

export default {
  title: 'Components/LinkButton',
  component: LinkButton,
  argTypes: {
    id: { control: 'none' },
    backgroundColor: { control: 'color' },
    borderColor: { control: 'color' },
  },
} as Meta;

const Template = (args) => <LinkButton text="Base" url="https://google.com" {...args} />;

export const Base = Template.bind({});

export const Small = Template.bind({});
Small.args = { size: 'small' };

export const Medium = Template.bind({});
Medium.args = { size: 'medium' };

export const Large = Template.bind({});
Large.args = { size: 'large' };

export const Internal = Template.bind({});
Internal.args = { url: '/home' };

export const ColouredButton = Template.bind({});
ColouredButton.args = { backgroundColor: 'lightgrey', borderColor: 'pink' };
