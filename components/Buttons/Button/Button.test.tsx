import React from 'react';

import { render, screen, fireEvent } from '@testing-library/react';
import { Button } from './Button';
import { ButtonVariantType, ButtonType, TargetType } from '../../../types';

export type ButtonVariantTestType = [ButtonVariantType, string];
export type ButtonTestType = [ButtonType, string];
export type TargetTestType = [TargetType, string];

describe('Button', () => {
  const text = 'Click me!';

  it('Should display the Button component in the document', () => {
    render(<Button text={text} />);

    const button = screen.getByTestId('qa-button');

    expect(button).toBeInTheDocument();
  });

  it('Should display the Button component with the expected text in the document', () => {
    render(<Button text={text} />);

    const buttonText = screen.getByText(text);

    expect(buttonText).toBeInTheDocument();
  });

  describe('isDisabled prop', () => {
    describe('When the isDisabled prop is set to true', () => {
      it('Should disable the Button', () => {
        render(<Button text={text} isDisabled />);

        const button = screen.getByTestId('qa-button');

        expect(button).toBeDisabled();
      });
    });

    describe('When the isDisabled prop is set to false', () => {
      it('Should not disable the Button', () => {
        render(<Button text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).not.toBeDisabled();
      });
    });
  });

  describe('onClick prop', () => {
    describe('When the onClick prop is set', () => {
      const onClickSpy = jest.fn();

      describe('When Button is clicked', () => {
        it('Should call the onClick function 1 time', () => {
          render(<Button onClick={onClickSpy} text={text} />);

          const button = screen.getByTestId('qa-button');

          fireEvent.click(button);

          expect(onClickSpy).toBeCalledTimes(1);
        });
      });
    });

    describe('When the onClick prop is not set', () => {
      it('Should not have the onClick attribute', () => {
        render(<Button text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).not.toHaveAttribute('onClick');
      });
    });
  });

  describe('type prop', () => {
    const buttonTypes: ButtonTestType[] = [
      ['button', 'button'],
      ['submit', 'submit'],
      ['reset', 'reset'],
    ];

    it.each(buttonTypes)('When the type prop is set to %s, it should have the attribute type %s', (type, expected) => {
      render(<Button text={text} type={type} />);

      const button = screen.getByTestId('qa-button');

      expect(button).toHaveAttribute('type', expected);
    });

    describe('When the type is not set', () => {
      it('Should have the attribute type button', () => {
        render(<Button text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveAttribute('type', 'button');
      });
    });
  });

  describe('variant prop', () => {
    const variants: ButtonVariantTestType[] = [
      ['cta', 'c-button--cta'],
      ['cta--tertiary', 'c-button--cta--tertiary'],
      ['danger', 'c-button--danger'],
      ['danger--tertiary', 'c-button--danger--tertiary'],
      ['dark', 'c-button--dark'],
      ['primary', 'c-button--primary'],
      ['secondary', 'c-button--secondary'],
      ['tertiary', 'c-button--tertiary'],
    ];

    it.each(variants)('When the variant prop is set to %s, it should have the %s css class', (variant, expected) => {
      render(<Button text={text} variant={variant} />);

      const button = screen.getByTestId('qa-button');

      expect(button).toHaveClass(expected);
    });

    describe('When the variant prop is not set', () => {
      it('Should have the c-button--primary css class', () => {
        render(<Button text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveClass('c-button--primary');
      });
    });
  });

  describe('linkButton prop', () => {
    describe('When the linkButton prop is set', () => {
      const linkButton = { id: 'link-button', url: '/url' };
      const { id, url } = linkButton;

      it('Should have the attribute id with the expected id', () => {
        render(<Button linkButton={linkButton} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveAttribute('id', id);
      });

      it('Should have the attribute href with the expected url', () => {
        render(<Button linkButton={linkButton} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveAttribute('href', url);
      });

      describe('target prop', () => {
        const targetTypes: TargetTestType[] = [
          ['_blank', '_blank'],
          ['_self', '_self'],
          ['_parent', '_parent'],
          ['top', 'top'],
        ];

        it.each(targetTypes)(
          'When the target prop is set to %s, it should have target attribute with a value of %s',
          (target, expected) => {
            render(<Button text={text} linkButton={{ id: 'link-button', url: '/url', target }} />);

            const button = screen.getByTestId('qa-button');

            expect(button).toHaveAttribute('target', expected);
          }
        );

        it('Should have the target attribute with the expected target', () => {
          render(<Button linkButton={linkButton} text={text} />);

          const button = screen.getByTestId('qa-button');

          expect(button).toHaveAttribute('id', id);
        });
      });
    });
  });
});
