import React from 'react';

import cx from 'classnames';

import { ButtonProps } from './Button.types';

import './Button.css';

const Button: React.FC<ButtonProps> = ({
  additionalClassNames,
  backgroundColor,
  borderColor,
  isDisabled = false,
  linkButton,
  onClick,
  text,
  type = 'button',
  variant = 'primary',
}) => {
  const ButtonComponent = linkButton ? linkButton.linkComponent || 'a' : 'button';

  const buttonClassNames = cx('c-button', additionalClassNames, {
    [`c-button--${variant}`]: Boolean(variant),
  });

  return (
    <ButtonComponent
      className={buttonClassNames}
      data-testid="qa-button"
      disabled={isDisabled}
      href={linkButton?.url}
      id={linkButton?.id}
      rel={linkButton?.url.indexOf('http') === 0 ? 'noopener noreferrer' : undefined}
      style={{
        backgroundColor /* stylelint-disable-line value-keyword-case */,
        borderColor /* stylelint-disable-line value-keyword-case */,
      }}
      target={linkButton?.target}
      type={type}
      onClick={onClick}
    >
      {text}
    </ButtonComponent>
  );
};

export { Button };
