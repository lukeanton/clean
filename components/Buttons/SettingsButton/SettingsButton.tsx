import React from 'react';

import cx from 'classnames';

import { SettingsButtonProps } from './SettingsButton.types';

import { Icon } from '../../Icon';

import './SettingsButton.css';

const SettingsButton: React.FC<SettingsButtonProps> = ({ additionalClassNames, onClick, supportiveText }) => {
  const settingsButtonClassNames = cx('c-settings-button', additionalClassNames);

  return (
    <button className={settingsButtonClassNames} data-testid="qa-settings-button" type="button" onClick={onClick}>
      <span aria-hidden="true">
        <Icon className="c-settings-button__icon" id="id_three_dots_icon" />
      </span>
      <span className="h-hide-visually">{supportiveText}</span>
    </button>
  );
};

export { SettingsButton };
