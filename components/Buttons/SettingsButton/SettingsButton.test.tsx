import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { SettingsButton } from './SettingsButton';

describe('SettingsButton', () => {
  const supportiveText = 'Settings button';
  let onClickSpy: () => void;

  it('Should display the SettingsButton component in the document', () => {
    render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

    const settingsButton = screen.getByTestId('qa-settings-button');

    expect(settingsButton).toBeInTheDocument();
  });

  it('Should display the SettingsButton with the expected supported text in the document', () => {
    render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

    const settingsButton = screen.getByText(supportiveText);

    expect(settingsButton).toBeInTheDocument();
  });

  describe('SettingsButton icon', () => {
    it('Should display the SettingsButton icon component in the document', () => {
      render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

      const icon = screen.getByTestId('qa-icon');

      expect(icon).toBeInTheDocument();
    });

    it('Should display the Icon with the id_three_dots_icon iconId in the document', () => {
      const settingsButtonIconId = 'id_three_dots_icon';

      render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

      const settingsButtonIcon = screen.getByTestId('qa-icon-use');

      expect(settingsButtonIcon).toHaveAttribute('xlink:href', '#' + settingsButtonIconId);
    });
  });

  describe('additionalClassNames prop', () => {
    const additionalClassNames = 'c-project-settings-button';

    describe('When the additionalClassNames prop is set', () => {
      it('Should have the additionalClassNames css class', () => {
        render(<SettingsButton onClick={onClickSpy} additionalClassNames={additionalClassNames} supportiveText={supportiveText} />);

        const settingsButton = screen.getByTestId('qa-settings-button');

        expect(settingsButton).toHaveClass(additionalClassNames);
      });
    });

    describe('When the additionalClassNames prop is not set', () => {
      it('Should not have the additionalClassNames css class', () => {
        render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

        const settingsButton = screen.queryByTestId('qa-settings-button');

        expect(settingsButton).not.toHaveClass(additionalClassNames);
      });
    });
  });

  describe('onClick prop', () => {
    describe('When the settingsButton is clicked', () => {
      it('Should call the onClick function 1 time', () => {
        onClickSpy = jest.fn();

        render(<SettingsButton onClick={onClickSpy} supportiveText={supportiveText} />);

        const settingsButton = screen.getByTestId('qa-settings-button');

        fireEvent.click(settingsButton);

        expect(onClickSpy).toBeCalledTimes(1);
      });
    });
  });
});
