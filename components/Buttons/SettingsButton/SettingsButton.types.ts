import { SyntheticEvent } from 'react';

import { IAdditionalClassNames, ISupportiveText } from '../../../interfaces';

export interface SettingsButtonProps extends IAdditionalClassNames, ISupportiveText {
  /**
   * Provide an onClick function that will be triggered when the settings button is clicked
   */
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => void;
}
