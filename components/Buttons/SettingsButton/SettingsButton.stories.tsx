import React from 'react';
import { action } from '@storybook/addon-actions';
import { Meta, Story } from '@storybook/react';
import { SettingsButton } from './SettingsButton';
import { SettingsButtonProps } from './SettingsButton.types';
import { Icons } from '../../Icons';

export default {
  title: 'Components/SettingsButton',
  component: SettingsButton,
} as Meta;

const Template: Story<SettingsButtonProps> = (args) => {
  return (
    <React.Fragment>
      <Icons />
      <SettingsButton {...args} />
    </React.Fragment>
  );
};

export const Base = Template.bind({});
Base.args = {
  additionalClassName: '',
  onClick: action('Settings button clicked'),
  supportiveText: 'Settings button',
};
