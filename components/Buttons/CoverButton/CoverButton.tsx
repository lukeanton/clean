import React from 'react';

import cx from 'classnames';

import { CoverButtonProps } from './CoverButton.types';

import './CoverButton.css';

const CoverButton: React.FC<CoverButtonProps> = ({ additionalClassNames, onClick, supportiveText }) => {
  const coverButtonClassNames = cx('c-cover-button', additionalClassNames);

  return (
    <button className={coverButtonClassNames} data-testid="qa-cover-button" type="button" onClick={onClick}>
      <span className="h-hide-visually">{supportiveText}</span>
    </button>
  );
};

export { CoverButton };
