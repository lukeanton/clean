import { IAdditionalClassNames, IOnClickButton, ISupportiveText } from '../../../interfaces';

export interface CoverButtonProps extends IAdditionalClassNames, IOnClickButton, ISupportiveText {}
