import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { CoverButton } from './CoverButton';

describe('CoverButton', () => {
  const handleClick = () => {};

  it('should display the CoverButton with default contents', () => {
    render(<CoverButton supportiveText="Test Supportive Text" onClick={handleClick} />);
    const coverButton = screen.queryByTestId('qa-cover-button');

    expect(coverButton).toBeInTheDocument();
    expect(coverButton).toHaveClass('c-cover-button');
    expect(coverButton).toHaveTextContent('Test Supportive Text');
  });

  describe('When class assigned to CoverButton component', () => {
    it('should have the class assigned to asset', () => {
      const additionalClassName = 'project-cover-button';

      render(<CoverButton additionalClassNames={additionalClassName} supportiveText="Test Supportive Text" onClick={handleClick} />);
      const coverButton = screen.queryByTestId('qa-cover-button');
      expect(coverButton).toHaveClass(additionalClassName);
    });
  });

  describe('When CoverButton has a supportive text', () => {
    it('should include the supportive text', () => {
      const supportiveText = 'Project Link';

      render(<CoverButton supportiveText={supportiveText} onClick={handleClick} />);

      const coverButton = screen.getByText(supportiveText);
      expect(coverButton).toBeInTheDocument();
    });
  });

  describe('When cover button clicked', () => {
    it('should assign text to a variable', () => {
      let text = '';

      const onClick = () => {
        text = 'Test text';
      };

      render(<CoverButton supportiveText="Test Supportive Text" onClick={onClick} />);

      const coverButton = screen.getByRole('button');

      expect(text).toBe('');
      fireEvent.click(coverButton);
      expect(text).toBe('Test text');
    });
  });
});
