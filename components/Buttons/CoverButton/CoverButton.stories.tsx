import React from 'react';
import { Meta, Story } from '@storybook/react';
import { CoverButton } from './CoverButton';
import { action } from '@storybook/addon-actions';

import { CoverButtonProps } from './CoverButton.types';

export default {
  title: 'Components/CoverButton',

  component: CoverButton,
} as Meta;

const Template: Story<CoverButtonProps> = (args) => {
  return <CoverButton {...args} />;
};

export const Base = Template.bind({});
Base.args = {
  onClick: action('Cover button clicked'),
  supportiveText: 'Open the sidebar',
};
