import React from 'react';
import { render, screen } from '@testing-library/react';

import { Quote } from './Quote';

describe('Quote', () => {
  test('it displays the quote element', () => {
    render(<Quote author="Test author" children="Test quote" />);

    const quoteElement = screen.queryByTestId('qa-quote');
    expect(quoteElement).toBeInTheDocument();
  });

  describe('When there is a text of the quote', () => {
    test('it shows quote text', () => {
      const text = 'Test quote text';

      render(<Quote author="Test author" children={text} />);

      const quoteElement = screen.getByTestId('qa-quote');
      expect(quoteElement).toHaveTextContent(text);
    });
  });

  describe('When there is an author of the quote', () => {
    test('it shows quote author', () => {
      const author = 'Test author';

      render(<Quote author={author} children="Test quote" />);

      const quoteElement = screen.getByTestId('qa-quote-author');
      expect(quoteElement).toHaveTextContent(author);
    });
  });

  describe('When there is an source of the quote', () => {
    test('it should display the source', () => {
      const source = 'Test source';

      render(<Quote author="Test author" children="Test quote" source={source} />);

      const quoteElement = screen.getByTestId('qa-quote-source');
      expect(quoteElement).toHaveTextContent(source);
    });
  });
});
