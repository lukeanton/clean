export interface QuoteProps {
  /**
   * The quote author
   */
  author?: string;
  /**
   * The creative works referenced
   */
  source?: string;
}
