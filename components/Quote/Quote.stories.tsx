import React from 'react';

import { Meta } from '@storybook/react/types-6-0';

import { Quote } from '../Quote';

export default {
  title: 'Components/Quote',
  component: Quote,
} as Meta;

export const Base = () => (
  <Quote author="Dave">
    <p>Hello</p>
  </Quote>
);

export const WithSource = () => (
  <Quote author="Dave" source="ABC News">
    World news is the best news
  </Quote>
);
