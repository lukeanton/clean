import React from 'react';
import { Meta } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { ButtonIconOnly } from './ButtonIconOnly';
import { Icons } from '../Icons';

export default {
  title: 'Components/ButtonIconOnly',
  component: ButtonIconOnly,
  argTypes: {
    onClick: { control: 'none' },
    text: { control: 'none' },
    type: { control: 'none' },
    iconId: {
      control: {
        type: 'select',
        options: ['id_caret_up_icon', 'id_caret_down_icon', 'id_close_icon', 'id_minus_icon', 'id_plus_icon'],
      },
    },
  },
} as Meta;

const Template = (args) => (
  <React.Fragment>
    <Icons />
    <ButtonIconOnly onClick={action('clicked')} {...args}>
      Hello World
    </ButtonIconOnly>
  </React.Fragment>
);

export const BookmarkButtonDefault = Template.bind({});
BookmarkButtonDefault.args = {
  iconId: 'id_bookmark_icon',
  text: 'Bookmark',
};

export const BookmarkButtonIconSelected = Template.bind({});
BookmarkButtonIconSelected.args = {
  iconId: 'id_bookmark_icon',
  isIconSelected: true,
  text: 'Bookmark',
};

export const CloseButton = Template.bind({});
CloseButton.args = {
  iconId: 'id_close_icon',
  text: 'Close',
  variant: 'danger--tertiary',
};

export const PlusButton = Template.bind({});
PlusButton.args = {
  iconId: 'id_plus_icon',
  text: 'Add',
};

export const MinusButton = Template.bind({});
MinusButton.args = {
  iconId: 'id_minus_icon',
  text: 'Minus',
};
