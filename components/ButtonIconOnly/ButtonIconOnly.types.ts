import { IAdditionalClassNames, IIconId, IIsDisabled, IOnClickButton, IButtonVariant } from '../../interfaces';

export interface ButtonIconOnlyProps extends IAdditionalClassNames, IIconId, IIsDisabled, IOnClickButton, IButtonVariant {
  /**
   * Specify if the svg icon has a border
   */
  isIconBorderVisible?: boolean;
  /**
   * Specify if the svg icon is selected so its styles can be updated
   */
  isIconSelected?: boolean;
  /**
   * The hidden text for the icon which will be read for screen readers
   */
  text: string;
  /**
   * The URL that the hyperlink points to.
   */
  type?: 'button' | 'submit';
}
