import React from 'react';
import { Meta } from '@storybook/react';
import { Toolbar } from './Toolbar';

import { LinkButton } from '../Buttons';
import { Select } from '../Select';
import { DateRangePicker, SingleDatePicker } from '../DatePickers';

export default {
  title: 'Components/Toolbar',
  component: Toolbar,
} as Meta;

const Template = (args) => {
  return <Toolbar {...args} />;
};

export const Base = Template.bind({});
Base.args = {
  children: (
    <React.Fragment>
      <div style={{ display: 'flex', gap: '1rem', justifyContent: 'space-between' }}>
        <span>Item 1</span>
        <span>Item 2</span>
        <span>Item 3</span>
      </div>
      <div style={{ display: 'flex', gap: '1rem', justifyContent: 'space-between' }}>
        <span>Item 1</span>
        <span>Item 2</span>
        <span>Item 3</span>
      </div>
      <div style={{ display: 'flex', gap: '1rem', justifyContent: 'space-between' }}>
        <span>Item 1</span>
        <span>Item 2</span>
        <span>Item 3</span>
      </div>
    </React.Fragment>
  ),
};

export const singleLeftRight = Template.bind({});
singleLeftRight.args = {
  childrenStart: <input type="text" placeholder="Search" />,
  childrenEnd: <LinkButton size="small" url="/" text="Apply" />,
};

export const multipleLeftSingleRight = Template.bind({});
multipleLeftSingleRight.args = {
  childrenStart: (
    <React.Fragment>
      <input type="text" placeholder="Search" />
      <select title="avengers" name="avengers">
        <option value="captain">Captain America</option>
        <option value="thor">Thor</option>
        <option value="iron">Iron Man</option>
        <option value="ant">Ant Man</option>
      </select>
    </React.Fragment>
  ),
  childrenEnd: <LinkButton size="small" url="/" text="Apply" />,
};

export const multipleRightSingleLeft = Template.bind({});
multipleRightSingleLeft.args = {
  childrenStart: <input type="text" placeholder="Search" />,
  childrenEnd: (
    <React.Fragment>
      <select title="avengers" name="avengers">
        <option value="captain">Captain America</option>
        <option value="thor">Thor</option>
        <option value="iron">Iron Man</option>
        <option value="ant">Ant Man</option>
      </select>
      <LinkButton size="small" url="/" text="Apply filters" />
    </React.Fragment>
  ),
};

export const singleLeftRightMiddle = Template.bind({});
singleLeftRightMiddle.args = {
  childrenStart: <input type="text" placeholder="Search" />,
  childrenEnd: <LinkButton size="small" url="/" text="Apply filters" />,
  childrenMiddle: <SingleDatePicker selectedDate={new Date()} />,
};

export const multipleLeftSingleMiddleSingleRight = Template.bind({});
multipleLeftSingleMiddleSingleRight.args = {
  childrenStart: (
    <React.Fragment>
      <input type="text" placeholder="Search" />
      <select name="avengers">
        <option value="captain">Captain America</option>
        <option value="thor">Thor</option>
        <option value="iron">Iron Man</option>
        <option value="ant">Ant Man</option>
      </select>
    </React.Fragment>
  ),
  childrenMiddle: <SingleDatePicker selectedDate={new Date()} />,
  childrenEnd: <LinkButton size="small" url="/" text="Apply filters" />,
};

export const singleLeftMultipleMiddleSingleRight = Template.bind({});

const selectedDateRange = {
  start: new Date(),
  end: new Date(),
};

singleLeftMultipleMiddleSingleRight.args = {
  childrenStart: <input type="text" placeholder="Search" />,
  childrenMiddle: (
    <React.Fragment>
      <select title="dc" name="dc">
        <option value="batman">Batman</option>
        <option value="superman">Superman</option>
        <option value="aqua man">Aqua Man</option>
        <option value="wonder woman">Wonder Woman</option>
      </select>
      <DateRangePicker selectedDate={selectedDateRange} />
    </React.Fragment>
  ),
  childrenEnd: <LinkButton size="small" url="/" text="Apply filters" />,
};
