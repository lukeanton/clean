import React from 'react';

import { render, screen } from '@testing-library/react';
import { Toolbar } from './Toolbar';

describe('Toolbar', () => {
  describe('When there is no children', () => {
    test('it should not display the children', () => {
      render(<Toolbar />);
      const toolbar = screen.queryByTestId('qa-toolbar');
      expect(toolbar).toBeEmptyDOMElement();

      const toolbarEnd = screen.queryByTestId('qa-toolbar-end');
      expect(toolbarEnd).toBeNull();

      const toolbarStart = screen.queryByTestId('qa-toolbar-start');
      expect(toolbarStart).toBeNull();

      const toolbarMiddle = screen.queryByTestId('qa-toolbar-middle');
      expect(toolbarMiddle).toBeNull();
    });
  });

  describe('When there are filtered children items', () => {
    describe('When there is children left', () => {
      test('it should display the children left', () => {
        const childrenStart = 'This is children left';

        render(<Toolbar childrenStart={childrenStart} />);

        const toolbarStart = screen.queryByTestId('qa-toolbar-start');
        expect(toolbarStart).toHaveTextContent(childrenStart);
      });
    });

    describe('When there is children right', () => {
      test('it should display the children right', () => {
        const childrenEnd = 'This is children right';

        render(<Toolbar childrenEnd={childrenEnd} />);

        const toolbarEnd = screen.queryByTestId('qa-toolbar-end');
        expect(toolbarEnd).toHaveTextContent(childrenEnd);
      });
    });

    describe('When there is children middle', () => {
      test('it should display the children middle', () => {
        const childrenMiddle = 'This is children date';

        render(<Toolbar childrenMiddle={childrenMiddle} />);

        const toolbarMiddle = screen.queryByTestId('qa-toolbar-middle');
        expect(toolbarMiddle).toBeInTheDocument();
        expect(toolbarMiddle).toHaveTextContent(childrenMiddle);
      });
    });
  });

  describe('When there is a main children', () => {
    test('it should display the children', () => {
      const children = 'This is children';

      render(<Toolbar children={children} />);

      const toolbar = screen.queryByTestId('qa-toolbar');
      expect(toolbar).toHaveTextContent(children);
    });
  });
});
