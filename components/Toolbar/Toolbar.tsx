import React from 'react';

import cx from 'classnames';

import { ToolbarProps } from './Toolbar.types';

import './Toolbar.css';

const Toolbar: React.FC<ToolbarProps> = ({
  additionalClassNames = '',
  children,
  childrenEnd,
  childrenMiddle,
  childrenMiddlePosition = 'right',
  childrenStart,
  label = 'filters',
}) => {
  const toolbarClassNames = cx('c-toolbar', additionalClassNames);

  return (
    <div aria-label={label} className={toolbarClassNames} data-testid="qa-toolbar" role="toolbar">
      {children ?? (
        <React.Fragment>
          {childrenStart && (
            <div className="c-toolbar c-toolbar--start" data-testid="qa-toolbar-start">
              {childrenStart}
            </div>
          )}
          {childrenMiddle && (
            <div
              className="c-toolbar c-toolbar--middle"
              data-testid="qa-toolbar-middle"
              style={{
                textAlign: childrenMiddlePosition,
              }}
            >
              {childrenMiddle}
            </div>
          )}
          {childrenEnd && (
            <div className="c-toolbar c-toolbar--end" data-testid="qa-toolbar-end">
              {childrenEnd}
            </div>
          )}
        </React.Fragment>
      )}
    </div>
  );
};
export { Toolbar };
