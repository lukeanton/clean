import { ReactNode } from 'react';

import { IAdditionalClassNames } from '../../interfaces';

export interface ToolbarProps extends IAdditionalClassNames {
  /**
   * The children of toolbar
   */
  children?: ReactNode;
  /**
   * The children of toolbar end
   */
  childrenEnd?: ReactNode;
  /**
   * The middle children of toolbar
   */
  childrenMiddle?: ReactNode;
  /**
   * The alignment of the middle children
   */
  childrenMiddlePosition?: 'center' | 'left' | 'right';
  /**
   * The children of toolbar start
   */
  childrenStart?: ReactNode;
  /**
   * The aria-label type
   */
  label?: string;
}
