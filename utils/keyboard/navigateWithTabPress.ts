const navigateWithTabPress = (event: KeyboardEvent, index: number, tabsLength: number) =>
  String(event.code) === 'Tab' && index + 1 >= tabsLength ? 0 : index + 1;

export { navigateWithTabPress };
