const path = require('path');

module.exports = ({ config }) => {
  const projectRoot = path.resolve(__dirname, '../');

  config.resolve.alias = {
    components: `${projectRoot}/components`,
  };

  return config;
};
