import 'backpack.css';
import 'animate.css';
import '../css/fonts.css';
import '../css/typography.css';
import '../css/global.css';
import '../css/helpers.css';

export const parameters = {
  controls: { expanded: true },
  actions: { argTypesRegex: '^on.*' },
  options: {
    storySort: (a, b) => (a[1].kind === b[1].kind ? 0 : a[1].id.localeCompare(b[1].id, undefined, { numeric: true })),
  },
};
