module.exports = {
  extends: ['stylelint-config-prettier', 'stylelint-config-recommended', 'stylelint-config-standard'],
  ignore: ['**/*.tsx'],
  plugins: ['stylelint-a11y', 'stylelint-high-performance-animation', 'stylelint-order', 'stylelint-performance-animation'],
  rules: {
    // TODO: Reinstate this later
    // 'a11y/media-prefers-reduced-motion': true,
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'extend',
          'at-root',
          'debug',
          'warn',
          'extend',
          'if',
          'else',
          'for',
          'each',
          'while',
          'mixin',
          'include',
          'content',
          'return',
          'function',
        ],
      },
    ],
    /**
     * Limit language features.
     * https://stylelint.io/user-guide/rules/#limit-language-features
     */

    // Declaration
    'declaration-property-unit-whitelist': { 'line-height': ['em', 'px'] },
    'declaration-block-trailing-semicolon': null,

    // Function
    'function-url-scheme-blacklist': ['data', 'ftp', '/^http/'],

    // General / Sheet
    'max-nesting-depth': [5, { ignore: ['blockless-at-rules'] }],
    'no-unknown-animations': true,

    // Number
    'number-max-precision': 3,

    // Property
    'property-blacklist': ['grid'],

    // Selector
    'selector-max-id': 0,
    'selector-nested-pattern': '',
    'selector-no-vendor-prefix': true,

    // Unit
    'unit-blacklist': ['cm', 'ex', 'in', 'mm', 'pc', 'pt'],

    // Value
    'value-no-vendor-prefix': true,

    /**
     * Stylistic issues.
     * https://stylelint.io/user-guide/rules/#stylistic-issues
     */

    // Declaration
    'declaration-empty-line-before': null,

    // Comment
    'comment-empty-line-before': [
      'always',
      {
        except: ['first-nested'],
        ignore: ['after-comment', 'stylelint-commands'],
      },
    ],

    // Font family
    'font-family-name-quotes': 'always-where-recommended',

    // Value list
    'value-list-comma-newline-after': null,

    /**
     * Turn off rules that may conflict with Prettier.
     */
    indentation: null,
    'max-line-length': null,
    'string-quotes': null,

    /**
     * Plugin rules.
     */

    // stylelint-a11y
    'a11y/content-property-no-static-value': true,
    'a11y/no-obsolete-attribute': true,
    'a11y/no-obsolete-element': true,

    // stylelint-order
    'order/order': [
      'custom-properties',
      'dollar-variables',
      'declarations',
      'at-rules',
      'rules',
      {
        type: 'rule',
      },
      {
        type: 'rule',
        // Matches pseudo-elements
        selector: /^&::[\w-]+$/,
      },
      {
        type: 'rule',
        // Matches simple pseudo-classes
        selector: /^&:[\w-]+$/,
      },
    ],
    'order/properties-alphabetical-order': true,
  },
};
