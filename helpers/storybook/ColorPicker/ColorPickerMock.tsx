import React, { useState } from 'react';

import cx from 'classnames';
import { ColorResult } from 'react-color';

import { ColorPicker } from '../../../components/ColorPicker';
import { DEFAULT_COLOR } from '../../../components/ColorPicker/ColorPicker.constants';
import { ColorPickerPreview } from '../../../components/ColorPickerPreview';
import './ColorPickerMock.css';

const ColorPickerMock: React.FC = () => {
  const [selectedColor, setSelectedColor] = useState(DEFAULT_COLOR);
  const [isColorPicker, setIsColorPicker] = useState(false);

  const handleColorChange = (updatedColor: ColorResult) => {
    const { r, g, b, a } = updatedColor.rgb;
    const updatedColorRgba = `rgba(${r}, ${g}, ${b}, ${a})`;

    setSelectedColor(updatedColorRgba);
  };

  const colorPickerClasses = cx('c-color-picker__select', {
    'c-color-picker-show': isColorPicker,
    'c-color-picker-hide': !isColorPicker,
  });

  return (
    <React.Fragment>
      <ColorPickerPreview selectedColor={selectedColor} onClick={() => setIsColorPicker(!isColorPicker)} />
      <ColorPicker additionalClassNames={colorPickerClasses} color={selectedColor} onChange={handleColorChange} />
    </React.Fragment>
  );
};

export { ColorPickerMock };
