import React from 'react';

import googleIcon from '../images/google_logo.png';

import { ProjectHeaderMockProps } from './ProjectHeaderMock.types';

import { Button } from '../../../components/Buttons';
import { ProjectHeader } from '../../../components/Headers/ProjectHeader';

import './ProjectHeaderMock.css';

const ProjectHeaderMock: React.FC<ProjectHeaderMockProps> = ({ avatarText = '' }) => {
  return (
    <ProjectHeader
      avatarText={avatarText}
      logo={{
        iconId: 'id_error_icon',
        imageSrc: googleIcon,
      }}
    >
      <Button text="Logout" type="button" variant="dark" />
    </ProjectHeader>
  );
};

export { ProjectHeaderMock };
