import React from 'react';

import { voicesData } from './voices';

import { Accordion } from '../../../components/Accordion';

const AccordionWithAudioMock: React.FC = () => {
  return (
    <React.Fragment>
      <Accordion
        audioAssetBaseUrl="https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com"
        data={voicesData}
        isDisplayPlayAudioButton
        onToggleSwitchClick={() => {}}
      />
    </React.Fragment>
  );
};

export { AccordionWithAudioMock };
