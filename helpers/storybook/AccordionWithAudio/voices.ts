export const voicesData = [
  {
    key: 'Arabic',
    label: 'Arabic',
    nodes: [
      {
        key: '31',
        label: 'Zeina',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/arabic-zeina.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Chinese',
    label: 'Chinese',
    nodes: [
      {
        key: '30',
        label: 'Zhiyu',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/chinese-zhiyu.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Danish',
    label: 'Danish',
    nodes: [
      {
        key: '29',
        label: 'Mads',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/danish-mads.mp3',
        nodes: [],
      },
      {
        key: '37',
        label: 'Naja',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/danish-naja.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Dutch',
    label: 'Dutch',
    nodes: [
      {
        key: '26',
        label: 'Ruben',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/dutch-ruben.mp3',
        nodes: [],
      },
      {
        key: '27',
        label: 'Lotte',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/dutch-lotte.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'English',
    label: 'English',
    nodes: [
      {
        key: '23',
        label: 'Brian',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/english-brian.mp3',
        nodes: [],
      },
      {
        key: '24',
        label: 'Emma',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/english-emma.mp3',
        nodes: [],
      },
      {
        key: '25',
        label: 'Amy',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/english-amy.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'French',
    label: 'French',
    nodes: [
      {
        key: '1',
        label: 'Mathieu',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/french-mathieu.mp3',
        nodes: [],
      },
      {
        key: '21',
        label: 'Léa',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/french-léa.mp3',
        nodes: [],
      },
      {
        key: '22',
        label: 'Céline',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/french-céline.mp3',
        nodes: [],
      },
      {
        key: '32',
        label: 'Mathieu',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/french-mathieu.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'German',
    label: 'German',
    nodes: [
      {
        key: '33',
        label: 'Marlene',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/german-marlene.mp3',
        nodes: [],
      },
      {
        key: '34',
        label: 'Vicki',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/german-vicki.mp3',
        nodes: [],
      },
      {
        key: '35',
        label: 'Hans',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/german-hans.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Hindi',
    label: 'Hindi',
    nodes: [
      {
        key: '36',
        label: 'Aditi',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/hindi-aditi.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Italian',
    label: 'Italian',
    nodes: [
      {
        key: '18',
        label: 'Giorgo',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/italian-giorgo.mp3',
        nodes: [],
      },
      {
        key: '19',
        label: 'Bianca',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/italian-bianca.mp3',
        nodes: [],
      },
      {
        key: '20',
        label: 'Carla',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/italian-carla.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Japanese',
    label: 'Japanese',
    nodes: [
      {
        key: '2',
        label: 'Takumi',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/japanese-takumi.mp3',
        nodes: [],
      },
      {
        key: '17',
        label: 'Mizuki',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/japanese-mizuki.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Korean',
    label: 'Korean',
    nodes: [
      {
        key: '3',
        label: 'Seoyeon',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/korean-seoyeon.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Norwegian',
    label: 'Norwegian',
    nodes: [
      {
        key: '4',
        label: 'Liv',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/norwegian-liv.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Polish',
    label: 'Polish',
    nodes: [
      {
        key: '5',
        label: 'Ewa',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/polish-ewa.mp3',
        nodes: [],
      },
      {
        key: '6',
        label: 'Maja',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/polish-maja.mp3',
        nodes: [],
      },
      {
        key: '7',
        label: 'Jacek',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/polish-jacek.mp3',
        nodes: [],
      },
      {
        key: '8',
        label: 'Jan',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/polish-jan.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Portugese',
    label: 'Portugese',
    nodes: [
      {
        key: '9',
        label: 'Ines',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/portugese-ines.mp3',
        nodes: [],
      },
      {
        key: '10',
        label: 'Cristiano',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/portugese-cristiano.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Romanian',
    label: 'Romanian',
    nodes: [
      {
        key: '11',
        label: 'Carmen',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/romanian-carmen.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Russian',
    label: 'Russian',
    nodes: [
      {
        key: '12',
        label: 'Tatyana',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/russian-tatyana.mp3',
        nodes: [],
      },
      {
        key: '13',
        label: 'Maxim',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/russian-maxim.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Spanish',
    label: 'Spanish',
    nodes: [
      {
        key: '14',
        label: 'Conchita',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/spanish-conchita.mp3',
        nodes: [],
      },
      {
        key: '15',
        label: 'Lucia',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/spanish-lucia.mp3',
        nodes: [],
      },
      {
        key: '16',
        label: 'Enrique',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/spanish-enrique.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Swedish',
    label: 'Swedish',
    nodes: [
      {
        key: '28',
        label: 'Astrid',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/swedish-astrid.mp3',
        nodes: [],
      },
    ],
  },
  {
    key: 'Turkish',
    label: 'Turkish',
    nodes: [
      {
        key: '38',
        label: 'Filiz',
        url: 'https://kanzi-assets-production.s3.ap-southeast-2.amazonaws.com/turkish-filiz.mp3',
        nodes: [],
      },
    ],
  },
];
