import React from 'react';

import { AvatarBreadcrumbCtaSectionTemplateMockProps } from './AvatarBreadcrumbCtaSectionTemplateMock.interfaces';

import { AvatarBreadcrumbCtaSectionTemplate } from '../../../components/Templates/AvatarBreadcrumbCtaSectionTemplate';

import './AvatarBreadcrumbCtaSectionTemplateMock.css';

const AvatarBreadcrumbCtaSectionTemplateMock: React.FC<AvatarBreadcrumbCtaSectionTemplateMockProps> = ({ isFreePlan }) => {
  return (
    <AvatarBreadcrumbCtaSectionTemplate
      ctaBreadcrumbItems={[
        {
          key: '1',
          content: <a href="/?path=/docs/templates-avatarbreadcrumbctasectiontemplate--base#overview">Overview</a>,
        },
        {
          key: '2',
          content: (
            <a href="/?path=/docs/templates-avatarbreadcrumbbtasectiontemplate--base#AvatarBreadcrumbCtaSectionTemplate">Component</a>
          ),
        },
      ]}
      ctaButtonText={isFreePlan ? 'Free Plan' : 'Paid Plan'}
      sectionTitle="Project Name"
    >
      <div className="c-subscription">
        <span>
          Current subscription - <strong className="h-transform-uppercase">pro</strong> (1.000.000 characters)
        </span>
        <span>
          <strong className="h-transform-uppercase">Remaining</strong> (16.893 characters)
        </span>
      </div>
    </AvatarBreadcrumbCtaSectionTemplate>
  );
};

export { AvatarBreadcrumbCtaSectionTemplateMock };
