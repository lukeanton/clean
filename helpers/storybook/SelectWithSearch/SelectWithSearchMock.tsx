import React, { useState } from 'react';

import { Button, Input, SelectWithSearch, ToggleSwitch, Toolbar } from 'components';

const SEARCH_LIST = [
  {
    id: 0,
    label: 'All schools',
  },
  {
    id: 1,
    label: 'The Matilda Centre',
  },
  {
    id: 2,
    label: 'University of Newcastle',
  },
  {
    id: 3,
    label: 'University of New South Wales',
  },
  {
    id: 4,
    label: 'Monash University',
  },
  {
    id: 5,
    label: 'Melbourne University',
  },
  {
    id: 6,
    label: 'Sydney University',
  },
];

const SelectWithSearchMock = () => {
  const [isSearchContentVisible, setSearchIsContentVisible] = useState(false);

  const handleSearchItemClick = (selectedId: number | string) => {
    console.log(`${selectedId} is the  of the selected item`);
  };

  return (
    <>
      <Toolbar
        childrenEnd={
          <>
            <ToggleSwitch
              additionalClassNames="c-user-status-toggle"
              id="user-status-filter"
              isChecked={false}
              labelText=""
              onChange={() => setSearchIsContentVisible(false)}
            />
            <Button text="Add new +" type="button" variant="primary-inverse" onClick={() => setSearchIsContentVisible(false)} />
          </>
        }
        childrenMiddle={
          <SelectWithSearch
            isDisplaySearchContent={isSearchContentVisible}
            searchList={SEARCH_LIST}
            isAvatarVisible
            onDisplaySearchContent={() => setSearchIsContentVisible(!isSearchContentVisible)}
            onSearchItemClick={handleSearchItemClick}
          />
        }
        childrenStart={
          <>
            <Input
              id="test"
              labelText=""
              name="test"
              placeholder="Search..."
              type="text"
              onChange={() => {}}
              onFocus={() => setSearchIsContentVisible(false)}
            />
            <Button text="Search" variant="primary-inverse" onClick={() => setSearchIsContentVisible(false)} />
          </>
        }
      />
    </>
  );
};

export { SelectWithSearchMock };
