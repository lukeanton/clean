import React, { useState } from 'react';

import { General, Navigation, Related, Seo } from '../TabSet/presentationTabs';

import { ITab, SideBar, SideBarTriggerButton, TabSet } from '../../../components';

const tabs: ITab[] = [
  {
    iconId: 'id_general_tab_icon',
    id: 'general',
    label: 'General',
    view: General,
  },
  {
    iconId: 'id_seo_tab_icon',
    id: 'seo',
    label: 'Seo',
    view: Seo,
  },
  {
    iconId: 'id_related_tab_icon',
    id: 'related',
    label: 'Related',
    view: Related,
  },
  {
    iconId: 'id_navigation_tab_icon',
    id: 'navigation',
    label: 'Navigation',
    view: Navigation,
  },
];

const SideBarTriggerExample: React.FC = () => {
  const [isSideBarOpen, setIsSideBarOpen] = useState<boolean>(false);

  const handleOpenCloseSideBar = () => {
    setIsSideBarOpen((currentValue) => !currentValue);
  };

  return (
    <React.Fragment>
      <SideBarTriggerButton
        buttonContent={{
          text: 'Side bar trigger',
        }}
        isSideBarOpen={isSideBarOpen}
        onClick={handleOpenCloseSideBar}
      />
      <SideBar
        additionalClassNames="c-custom-sidebar-class"
        isSideBarOpen={isSideBarOpen}
        isDisplayCloseButton
        onClose={handleOpenCloseSideBar}
      >
        <TabSet defaultActiveTabId="general" tabs={tabs} />
      </SideBar>
    </React.Fragment>
  );
};

const SideBarTriggerWithIconExample: React.FC = () => {
  const [isSideBarOpen, setIsSideBarOpen] = useState<boolean>(false);

  const handleOpenCloseSideBar = () => {
    setIsSideBarOpen((currentValue) => !currentValue);
  };

  return (
    <React.Fragment>
      <SideBarTriggerButton
        buttonContent={{
          iconId: 'id_plus_icon',
        }}
        isSideBarOpen={isSideBarOpen}
        onClick={handleOpenCloseSideBar}
      />
      <SideBar additionalClassNames="c-custom-sidebar-class" isSideBarOpen={isSideBarOpen} onClose={handleOpenCloseSideBar}>
        <TabSet defaultActiveTabId="general" tabs={tabs} />
      </SideBar>
    </React.Fragment>
  );
};

export { SideBarTriggerExample, SideBarTriggerWithIconExample };
