export interface NotificationsDropdownMockProps {
  /**
   * The max number of displayed list items
   */
  numberOfDisplayedNotificationListItems?: number;
}
