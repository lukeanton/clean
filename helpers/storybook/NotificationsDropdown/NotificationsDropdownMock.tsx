import React from 'react';

import { NotificationsDropdownMockProps } from './NotificationsDropdownMock.types';

import { NotificationsDropdown } from '../../../components/NotificationsDropdown';

const NotificationsDropdownMock: React.FC<NotificationsDropdownMockProps> = ({ numberOfDisplayedNotificationListItems }) => {
  const notificationsListItems = [
    {
      id: '1',
      text: 'Jane Namely has accepted their invitation to the FFSP project',
      isRead: true,
    },
    {
      id: '2',
      text: 'Your app Bubble pop activity has been updated',
      isRead: true,
    },
    {
      id: '3',
      text: 'Your app Bubble pop activity has been seen',
      isRead: false,
    },
    {
      id: '4',
      text: 'Lorem ipsum sit amet dolar valar morghulis',
      isRead: false,
    },
  ];

  return (
    <NotificationsDropdown
      notificationListItems={notificationsListItems}
      numberOfDisplayedNotificationListItems={numberOfDisplayedNotificationListItems}
      onClick={() => {}}
    />
  );
};

export { NotificationsDropdownMock };
