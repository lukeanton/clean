interface IApiDataItem {
  defaultProp: string;
  description: string;
  propName: string;
  type: string;
}

export interface ApiTableProps {
  apiTableData: IApiDataItem[];
}
