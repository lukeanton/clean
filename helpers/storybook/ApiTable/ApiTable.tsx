import React from 'react';

import { ApiTableProps } from './ApiTable.types';
import './ApiTable.css';

const ApiTable: React.FC<ApiTableProps> = ({ apiTableData }) => {
  return (
    <div className="c-table-container">
      <table className="c-api-table">
        <thead className="c-api-table__header">
          <tr>
            <th className="h-no-wrap">Name</th>
            <th>Description</th>
            <th className="h-no-wrap">Default</th>
          </tr>
        </thead>
        <tbody>
          {apiTableData.map(({ defaultProp, description, propName, type }) => (
            <tr key={propName}>
              <td className="h-bold h-no-wrap">{propName}</td>
              <td>
                {description}
                <br />
                <span className="c-code-tag">{type}</span>
              </td>
              <td className="h-no-wrap">
                <span className="c-code-tag">{defaultProp}</span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export { ApiTable };
