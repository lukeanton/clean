/* eslint-disable no-console */

import React, { useRef } from 'react';

import { CopyButton } from '../../../components/Buttons/CopyButton';

import './CopyButtonMock.css';

const CopyButtonMock: React.FC = () => {
  const copyRefDiv = useRef<HTMLDivElement>(null);
  const copyRefInput = useRef<HTMLInputElement>(null);
  const copyRefTextArea = useRef<HTMLTextAreaElement>(null);
  const copyRefParagraph = useRef<HTMLParagraphElement>(null);
  const copyRefSpan = useRef<HTMLSpanElement>(null);
  const copyRefTableCell1 = useRef<HTMLTableCellElement>(null);
  const copyRefTableCell2 = useRef<HTMLTableCellElement>(null);

  return (
    <React.Fragment>
      <div className="c-copy-button-mock">
        <div className="c-copy-button-mock-item">
          <input ref={copyRefInput} title="Copy from input" type="text" value="Input value" onChange={() => {}} />
          <CopyButton copyElementRef={copyRefInput} supportiveText="Copy from input" onCopyCompleted={() => console.log('Completed')} />
        </div>
        <div className="c-copy-button-mock-item">
          <textarea ref={copyRefTextArea} title="Copy from textarea" value="Textarea value" onChange={() => {}} />
          <CopyButton
            copyElementRef={copyRefTextArea}
            supportiveText="Copy from textarea"
            onCopyCompleted={() => console.log('Completed')}
          />
        </div>
        <div className="c-copy-button-mock-item">
          <p ref={copyRefParagraph}>Paragraph text</p>
          <CopyButton
            copyElementRef={copyRefParagraph}
            supportiveText="Copy from paragraph"
            onCopyCompleted={() => console.log('Completed')}
          />
        </div>
        <div className="c-copy-button-mock-item">
          <span ref={copyRefSpan}>Span text</span>
          <CopyButton copyElementRef={copyRefSpan} supportiveText="Copy from span" onCopyCompleted={() => console.log('Completed')} />
        </div>
        <div className="c-copy-button-mock-item">
          <div ref={copyRefDiv}>Container text</div>
          <CopyButton copyElementRef={copyRefDiv} supportiveText="Copy from div" onCopyCompleted={() => console.log('Completed')} />
        </div>
        <table className="c-copy-button-mock-table">
          <thead>
            <tr className="c-copy-button-mock-table__row">
              <th className="c-copy-button-mock-table__row__head">Title</th>
              <th className="c-copy-button-mock-table__row__head">Created</th>
              <th className="c-copy-button-mock-table__row__head">Key</th>
              <th className="c-copy-button-mock-table__row__head">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <tr className="c-copy-button-mock-table__row">
              <td className="c-copy-button-mock-table__row__cell">API-key name 1</td>
              <td className="c-copy-button-mock-table__row__cell">15/02/2021</td>
              <td ref={copyRefTableCell1} className="c-copy-button-mock-table__row__cell">
                njnsfWEnjhweien19hkjhnksweurh
              </td>
              <td className="c-copy-button-mock-table__row__cell">
                <CopyButton
                  copyElementRef={copyRefTableCell1}
                  supportiveText="Copy from table cell"
                  onCopyCompleted={() => console.log('Completed')}
                />
              </td>
            </tr>
            <tr className="c-copy-button-mock-table__row">
              <td className="c-copy-button-mock-table__row__cell">API-key name 2</td>
              <td className="c-copy-button-mock-table__row__cell">11/02/2021</td>
              <td ref={copyRefTableCell2} className="c-copy-button-mock-table__row__cell">
                akdjasSADA10asadasdasdlpovcx
              </td>
              <td className="c-copy-button-mock-table__row__cell">
                <CopyButton
                  copyElementRef={copyRefTableCell2}
                  supportiveText="Copy from table cell"
                  onCopyCompleted={() => console.log('Completed')}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

export { CopyButtonMock };
