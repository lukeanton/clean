import React, { useState } from 'react';

import { ToggleSwitch } from '../../../components/ToggleSwitch';
import { IIdentifier, IIsLabelHidden, ILabelText } from '../../../interfaces';

export interface ToggleSwitchDemoProps extends IIdentifier, IIsLabelHidden, ILabelText {}

const ToggleSwitchDemo: React.FC<ToggleSwitchDemoProps> = ({ id, isLabelHidden = false, labelText }) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleOnChange = () => setIsChecked(!isChecked);

  return <ToggleSwitch id={id} isChecked={isChecked} isLabelHidden={isLabelHidden} labelText={labelText} onChange={handleOnChange} />;
};

export { ToggleSwitchDemo };
