import React, { useState } from 'react';

import { DropdownMockProps } from './DropdownMock.types';

import { Dropdown } from '../../../components/Dropdown';

const DropdownMock: React.FC<DropdownMockProps> = ({ dropdownTrigger, children, dropdownId }) => {
  const [isSearchContentVisible, setSearchIsContentVisible] = useState(false);

  return (
    <Dropdown
      dropdownId={dropdownId}
      isDisplayContent={isSearchContentVisible}
      trigger={dropdownTrigger}
      onDisplayContent={setSearchIsContentVisible}
    >
      {children}
    </Dropdown>
  );
};

export { DropdownMock };
