import { ElementType } from 'react';

export interface DropdownMockProps {
  dropdownId: string;
  dropdownTrigger?: ElementType;
}
