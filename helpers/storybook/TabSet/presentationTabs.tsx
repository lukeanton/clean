/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-console */
import React, { CSSProperties } from 'react';

import { FormButtons, Input, Textarea, Spacing, ToggleSwitch, ITab, Button } from '../../../components';

import './presentationTabs.css';

const viewStyle: CSSProperties = {
  padding: '1.4rem',
};

const buttonWrapper: CSSProperties = {
  position: 'absolute',
  backgroundColor: '#F6F6F7',
  padding: '2rem',
  bottom: '0',
  left: '0',
  right: '0',
};

const handleDelete = () => {
  console.log('delete data');
};

const Countries = () => (
  <table className="c-table-example">
    <tr>
      <th>User Name</th>
      <th>Contact</th>
      <th>Country</th>
    </tr>
    <tr>
      <td>Alfreds Futterkiste</td>
      <td>Maria Anders</td>
      <td>Germany</td>
    </tr>
    <tr>
      <td>Centro comercial Moctezuma</td>
      <td>Francisco Chang</td>
      <td>Mexico</td>
    </tr>
    <tr>
      <td>Ernst Handel</td>
      <td>Roland Mendel</td>
      <td>Austria</td>
    </tr>
    <tr>
      <td>Island Trading</td>
      <td>Helen Bennett</td>
      <td>UK</td>
    </tr>
    <tr>
      <td>Laughing Bacchus Winecellars</td>
      <td>Yoshi Tannamuri</td>
      <td>Canada</td>
    </tr>
    <tr>
      <td>Magazzini Alimentari Riuniti</td>
      <td>Giovanni Rovelli</td>
      <td>Italy</td>
    </tr>
  </table>
);

const Seinfeld = () => (
  <table className="c-table-example">
    <tr>
      <th>Name</th>
      <th>Best Episode</th>
      <th>Description</th>
    </tr>
    <tr>
      <td className="h-no-wrap">Elaine Benes</td>
      <td>The Sponge</td>
      <td>
        (Somewhat) lovable loser. Persnickety smart aleck. Erratic goofball. Seinfeld‘s three other leads can be boiled down to types. But
        no box could contain the full essence of Elaine Benes, the show’s most complex, quirky and ultimately hilarious character.
      </td>
    </tr>
    <tr>
      <td className="h-no-wrap">George Costanza</td>
      <td>The Comeback</td>
      <td>
        He was, in no particular order, a petty human being, a pathological liar, a spendthrift, an egomaniac, a hypochondriac and a
        self-loathing schlemiel. Whether he's making up an implausible excuse that involves Art Vandelay, using his "dead fiancee" to gain
        entrance to an all-models club, brushing off his actual dead fiancee's passing with an indifferent "Huh" or knocking over children
        to escape a fire, George was the wizard of id. ("I feel like my old self again. Neurotic, paranoid, totally inadequate, completely
        insecure. It's a pleasure."){' '}
      </td>
    </tr>
    <tr>
      <td className="h-no-wrap">Jerry Seinfeld</td>
      <td>The Voice</td>
      <td>
        The popularity of Larry David's Curb Your Enthusiasm, and the resulting awareness that the George character was modeled after
        David's neurotic rudeness, has caused a lot of second and third generation Seinfeld fans to undervalue Jerry's role. It never
        mattered that the "IRL" Jerry Seinfeld was kind of a terrible actor, or that he sometimes couldn't read his lines without cracking a
        smile. Even when he was having trouble keeping a straight face, Jerry was still the perfect straight man. Elaine and George and
        Kramer may be the more enduring, iconic characters, but Jerry was the firmament of the show — the axis around which all the other
        characters orbited. In "The Dog," Elaine and George try to hang out sans Jerry and find that he is the only thing they can find to
        talk about with each other. ("Wait, have you ever seen him throw up?")
      </td>
    </tr>
    <tr>
      <td className="h-no-wrap">Cosmo Kramer</td>
      <td>The Merv Griffin Show</td>
      <td>
        The history of TV comedy is full of wacky neighbors, but none so crazed as Kramer — this guy lives in a world all his own, bristling
        with so much manic energy, his hair seems to give off Bride of Frankenstein-style electric sparks. A wholly original comic creation,
        he's like the Jewish Falstaff that Shakespeare must have always wanted to create.
      </td>
    </tr>
    <tr>
      <td>Newman</td>
      <td className="h-no-wrap">The Label Maker</td>
      <td>
        Hello, Newman. A postal worker who lives down the hall from Jerry and Kramer, Newman is the show’s primary antagonist: “Mark my
        words, Seinfeld! Your day of reckoning is coming, when an evil wind will blow through your little playworld and wipe that smug smile
        off your face!” There’s no obvious reason for him and Jerry to be sworn enemies — he’s no more self-absorbed or scheming than any of
        the lead characters. But he did work the same postal route as David “Son of Sam” Berkowitz, which can’t be a coincidence. GAVIN
        EDWARDS
      </td>
    </tr>
  </table>
);

export const Basic = () => (
  <div>
    <Spacing>
      <Input id="first-name" labelText="First Name" name="firstName" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Input id="last-name" labelText="Last Name" name="lastName" type="text" onChange={() => null} />
    </Spacing>
    <div>
      <Button text="Submit" variant="cta" />
    </div>
  </div>
);

export const Advanced = () => (
  <div>
    <Spacing>
      <Input id="first-name" labelText="First Name" name="firstName" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Input id="last-name" labelText="Last Name" name="lastName" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Textarea id="description" labelText="Description" name="description" onChange={() => null} />
    </Spacing>
    <div>
      <Button text="Submit" variant="cta" />
    </div>
  </div>
);

export const General = ({ onClose }: { onClose: () => void }) => (
  <div style={viewStyle}>
    <Spacing>
      <Input id="title" labelText="Title" name="title" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Textarea id="description" labelText="Description" name="description" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Input id="url" labelText="Url" name="url" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <ToggleSwitch id="publish" labelText="Publish Page" isChecked onChange={() => null} />
    </Spacing>
    <div style={buttonWrapper}>
      <FormButtons onClose={onClose} onDelete={handleDelete} />
    </div>
  </div>
);

export const Seo = ({ onClose }: { onClose: () => void }) => (
  <div style={viewStyle}>
    <Spacing>
      <Input id="seo-title" labelText="Seo Title" name="seoTitle" type="text" onChange={() => null} />
    </Spacing>
    <Spacing>
      <Textarea id="seo-description" labelText="Seo Description" name="seoDescription" onChange={() => null} />
    </Spacing>
    <div style={buttonWrapper}>
      <FormButtons onClose={onClose} onDelete={handleDelete} />
    </div>
  </div>
);

export const Related = ({ onClose }: { onClose: () => void }) => (
  <div style={viewStyle}>
    <Spacing>
      <Input id="resources" labelText="Resources" name="resources" type="text" onChange={() => null} />
    </Spacing>
    <div style={buttonWrapper}>
      <FormButtons onClose={onClose} onDelete={handleDelete} />
    </div>
  </div>
);

export const Navigation = ({ onClose }: { onClose: () => void }) => (
  <div style={viewStyle}>
    <Spacing>
      <Input id="navigation" labelText="Navigation" name="navigation" type="text" onChange={() => null} />
    </Spacing>
    <div style={buttonWrapper}>
      <FormButtons onClose={onClose} onDelete={handleDelete} />
    </div>
  </div>
);

export const Organisation = () => <Countries />;
export const ProjectAdmin = () => <h1>Project admin table view</h1>;
export const PublicUsers = () => <h1>Public users table view</h1>;
export const Invitations = () => <h1>Invitations table view</h1>;
export const Groups = () => <h1>Groups table view</h1>;
export const UserTypes = () => <h1>User types table view</h1>;

const Tab = () => <div style={viewStyle} />;

const isolatedTabs: ITab[] = [
  { id: 'tab 1', label: 'Tab 1', view: Tab },
  { id: 'tab 2', label: 'Tab 2', view: Tab },
  { id: 'tab 3', label: 'Tab 3', view: Tab },
  { id: 'tab 4', label: 'Tab 4', view: Tab },
];

const leftAlignTabs: ITab[] = [
  { id: 'organisation', label: 'Organisation', view: Countries },
  { id: 'project admin', label: 'Project admin', view: ProjectAdmin },
  { id: 'public users', label: 'Public users', view: PublicUsers },
  { id: 'invitations', label: 'Invitations', view: Invitations },
  { id: 'groups', label: 'Groups', view: Groups },
  { id: 'user types', label: 'User types', view: UserTypes },
];

const switchTabs: ITab[] = [
  { id: 'basic', label: 'Basic Credentials', view: Basic },
  { id: 'advanced', label: 'Advanced', view: Advanced },
];

const iconTabs: ITab[] = [
  { iconId: 'id_general_tab_icon', id: 'tab 1', label: 'Tab 1', view: Tab },
  { iconId: 'id_seo_tab_icon', id: 'tab 2', label: 'Tab 2', view: Tab },
  { iconId: 'id_related_tab_icon', id: 'tab 3', label: 'Tab 3', view: Tab },
  { iconId: 'id_navigation_tab_icon', id: 'tab 4', label: 'Tab 4', view: Tab },
];

const tabs: ITab[] = [
  { iconId: 'id_general_tab_icon', id: 'general', label: 'General', view: General },
  { iconId: 'id_seo_tab_icon', id: 'seo', label: 'Seo', view: Seo },
  { iconId: 'id_related_tab_icon', id: 'related', label: 'Related', view: Related },
  { iconId: 'id_navigation_tab_icon', id: 'navigation', label: 'Navigation', view: Navigation },
];

const tableTabs: ITab[] = [
  { id: 'table 1', label: 'Seinfeld', view: Seinfeld },
  { id: 'table 2', label: 'Countries', view: Countries },
];

const environment = {
  sidebar: 'SIDEBAR',
  table: 'TABLE',
};

export { environment, iconTabs, isolatedTabs, leftAlignTabs, switchTabs, tabs, tableTabs };
