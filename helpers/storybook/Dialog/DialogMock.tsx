import React, { useState } from 'react';

import { DialogMockProps } from './DialogMock.types';

import { Button } from '../../../components/Buttons';
import { Dialog } from '../../../components/Dialog';

const DialogMock: React.FC<DialogMockProps> = ({ buttonText, children, confirmText, isOpen, title, type, onCancel, onConfirm }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleToggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleOnRequestClose = () => {
    setIsModalOpen(false);
  };

  return (
    <React.Fragment>
      <Button text={buttonText} onClick={handleToggleModal} />
      <Dialog
        confirmText={confirmText}
        isOpen={isOpen || isModalOpen}
        title={title}
        type={type}
        onCancel={onCancel}
        onClose={handleOnRequestClose}
        onConfirm={onConfirm}
      >
        {children}
      </Dialog>
    </React.Fragment>
  );
};

export { DialogMock };
