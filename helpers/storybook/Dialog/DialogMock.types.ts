import { DialogProps } from '../../../components/Dialog';

export interface DialogMockProps extends DialogProps {
  /**
   * Specify text for the mock button
   */
  buttonText: string;
}
