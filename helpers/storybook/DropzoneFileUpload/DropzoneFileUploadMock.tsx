import React, { useState } from 'react';

import { ButtonIconOnly } from '../../../components/ButtonIconOnly';
import { DropzoneFileUpload } from '../../../components/DropzoneFileUpload';

import './DropzoneFileUploadMock.css';

const DropzoneFileUploadMock = () => {
  const [file, setFile] = useState<File[] | undefined>();

  const handleDrop = (files: File[]) => {
    setFile(files);
  };

  return (
    <React.Fragment>
      <div>
        <DropzoneFileUpload file={file} labelText="Upload files" onDrop={handleDrop}>
          <div className="c-dropzone-file-upload-mock">
            <span className="c-dropzone-file-upload-mock_text">File uploaded</span>
            <ButtonIconOnly iconId="id_close_icon" text="" onClick={() => setFile(undefined)} />
          </div>
        </DropzoneFileUpload>
      </div>
    </React.Fragment>
  );
};

export { DropzoneFileUploadMock };
