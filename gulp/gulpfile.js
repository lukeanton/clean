const cssnano = require('cssnano');
const del = require('del');
const gulp = require('gulp');
const concat = require('gulp-concat-css');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const prefixwrap = require('postcss-prefixwrap');

// TODO: Find a better way to set this cssPrefix since it's used here
// as well as in components
const cssPrefix = 'netfront-ui-external';
const nodeModulesPath = '../node_modules';
const targetFolder = '../css';
const targetFileExtension = '.min.css';

gulp.task('build-namespaced-antd-css-file', () => {
  const rulePrefix = `${cssPrefix}-antd`;

  return gulp
    .src(`${nodeModulesPath}/antd/dist/antd.css`)
    .pipe(
      postcss([
        prefixwrap(`.${rulePrefix}`, {
          ignoredSelectors: [':root', /^\.ant(.+)$/, /^\.slide-(.+)$/, /^\.zoom-big-fast(.+)$/],
        }),
        // Minify after prefixwrap
        cssnano({ preset: 'default' }),
      ])
    )
    .pipe(rename(`${rulePrefix}-antd${targetFileExtension}`))
    .pipe(gulp.dest(targetFolder));
});

gulp.task('clean', () => {
  return del([`${targetFolder}/*min.css`], {
    force: true,
  });
});

gulp.task('combine-namespaced-css-files', () => {
  return gulp
    .src(`${targetFolder}/*${targetFileExtension}`)
    .pipe(concat(`${cssPrefix}${targetFileExtension}`))
    .pipe(gulp.dest(targetFolder));
});

gulp.task('remove-temporary-css-files', () => {
  return del([`${targetFolder}/${cssPrefix}-*${targetFileExtension}`], {
    force: true,
  });
});

gulp.task(
  'build-namespaced-css-file',
  gulp.series('clean', 'build-namespaced-antd-css-file', 'combine-namespaced-css-files', 'remove-temporary-css-files')
);
